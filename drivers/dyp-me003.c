#include "dyp-me003.h"
#include "initcalls.h"
#include "interrupt.h"
#include "gpio.h"
#include "console.h"

#define DYP_ME003_GPIO		18

static struct gpio_api *gpiodev = NULL;

static int DETECT_CNT = 0;

void dyp_me003_irq_handler(uint32_t irq){
	printk("%s: Movement Detected! DETECT_CNT = %d\n",__func__,DETECT_CNT);
	DETECT_CNT++;
}

int dyp_me003_init(void){

	int ret = 0;

	// request gpio device driver api
	gpiodev = (struct gpio_api*)request_device(GPIO_DEV);

	if(gpiodev==NULL)
		printk("%s: Could not get GPIO driver api!\n",__func__);

	ret = gpiocore_request_gpio(DYP_ME003_GPIO);
	if(ret){
		// Error occured
		printk("%s: Error obtaining GPIO pin %d\n",DYP_ME003_GPIO);
		return -1;
	}

	// request interrupt for the pin DYP_ME003_GPIO
	ret = request_irq(dyp_me003_irq_handler,gpiocore_gpio_to_irq(DYP_ME003_GPIO));

	// Pull the gpio pin low
	gpiodev->gpio_clear(DYP_ME003_GPIO);

	// Set the pin direction as input
	gpiodev->gpio_function_set(DYP_ME003_GPIO,GPIO_INPUT);

	// Enable irq generation for the gpio pin
	gpiodev->gpio_disable_irq(gpiocore_gpio_to_irq(DYP_ME003_GPIO),DETECT_ALL);
	gpiodev->gpio_enable_irq(gpiocore_gpio_to_irq(DYP_ME003_GPIO),GPIO_HIGH_DETECT);

        printk("%s: dyp_me003 driver initialized! \n",__func__);
        return 0;
}

int dyp_me003_exit(void){
	return 0;
}

/*
module_init(dyp_me003_init);
module_init(dyp_me003_exit);
*/
