#include "initcalls.h"
#include "gpio.h"
#include "console.h"
#include "interrupt.h"
#include "algo.h"
#include "../arch/arm/bcm2708/bcm2708_systemtimer.h"

#define ds18b20_gpio_pin1 	17

extern void wait(uint32_t time);

static struct gpio_api *gpiodev = NULL;

/* ROM COMMANDS */

#define SEARCH_ROM			0xF0
#define READ_ROM			0x33
#define MATCH_ROM			0x55
#define SKIP_ROM			0xCC
#define ALARM_SEARCH_ROM		0xEC


/* FUNCTION COMMANDS */

#define CONVERT_T_FUNC			0x44
#define WRITE_SCRATCHPAD_FUNC		0x4E
#define READ_SCRATCHPAD_FUNC		0xBE
#define COPY_SCRATCHPAD_FUNC		0x48
#define RECALL_E2_FUNC			0xB8
#define READ_POWER_SUPPLY_FUNC		0xB4

// Temperature digits as integers. These correspond to values 1000*2^-4 --> 1000*2^6
static uint32_t digits[11] = {625,1250,2500,5000,10000,20000,40000,80000,160000,320000,640000};

void ds18b20_irq_handler1(uint32_t irq){

	printk("%s: Interrupt Handler 1!\n",__func__);
}

int ds18b20_init(){

	uint32_t pinlevel;

	// request gpio device driver api
	gpiodev = (struct gpio_api*)request_device(GPIO_DEV);
	
	if(gpiodev==NULL)
		printk("%s: Could not get GPIO driver api!\n",__func__);

	// request gpio pin
	int ret = 0;

	ret = gpiocore_request_gpio(ds18b20_gpio_pin1);
	if(ret){
		printk("%s: Could not allocate gpio pin %d! \n",__func__,ds18b20_gpio_pin1);
		return -1;
	}

	// request interrupt for the pin 17
//	ret = request_irq(ds18b20_irq_handler1,gpiocore_gpio_to_irq(ds18b20_gpio_pin1));

	// Set the pin direction outwards
	gpiodev->gpio_function_set(ds18b20_gpio_pin1,GPIO_OUTPUT);

	// Check first that the level is low
	gpiodev->gpio_clear(ds18b20_gpio_pin1);
	pinlevel = gpiodev->gpio_pinlevel(ds18b20_gpio_pin1);
	printk("%s: initial PINLEVEL = %d\n",__func__,pinlevel);

	// Pull the pin 17 high
	gpiodev->gpio_set(ds18b20_gpio_pin1);

	// Check that the level is high
	pinlevel = gpiodev->gpio_pinlevel(ds18b20_gpio_pin1);
	printk("%s: new PINLEVEL = %d\n",__func__,pinlevel);

	// Disable irq generation for the 1w-gpio pin
	gpiodev->gpio_disable_irq(gpiocore_gpio_to_irq(ds18b20_gpio_pin1),DETECT_ALL);
	
	printk("%s: ds18b20 driver initialized! \n",__func__);
	return 0;
}


int ds18b20_initialize_connection(){
	
	uint32_t pinlevel=0;

	// Set the pin direction outwards
	gpiodev->gpio_function_set(ds18b20_gpio_pin1,GPIO_OUTPUT);
	// pull down the pin for at least 500µs
	gpiodev->gpio_clear(ds18b20_gpio_pin1);
	wait(500);
	// release the gpio pulldown and switch to input mode and wait 75µs
	gpiodev->gpio_function_set(ds18b20_gpio_pin1,GPIO_INPUT);
	wait(75);
	// read the pinlevel. If device is connected, the line should be pulled down
	pinlevel = gpiodev->gpio_pinlevel(ds18b20_gpio_pin1);
	// wait for initialization time to end
	wait(425);

	// If the measured pinlevel was zero and now it is 1, then everything is ok!
	if(!pinlevel && gpiodev->gpio_pinlevel(ds18b20_gpio_pin1))
		return 0;
	else
		return -1;
}

uint8_t ds18b20_readbit(){
	// make sure the direction is output
	gpiodev->gpio_function_set(ds18b20_gpio_pin1,GPIO_OUTPUT);
	// pull down the pin for 5 µs
	gpiodev->gpio_clear(ds18b20_gpio_pin1);
	wait(3);
	// then release the pin and switch to input mode for additional 6 µs
	gpiodev->gpio_function_set(ds18b20_gpio_pin1,GPIO_INPUT);
	wait(10);
	// Then read the pin level
	uint8_t pinlevel = gpiodev->gpio_pinlevel(ds18b20_gpio_pin1);
	// wait additional 50µs
	wait(20);

	return pinlevel;
}

void ds18b20_writebit0(){
	// Make sure the pin is in output configuration
	gpiodev->gpio_function_set(ds18b20_gpio_pin1,GPIO_OUTPUT);	

	// pull down the pin for at least 60µs
	gpiodev->gpio_clear(ds18b20_gpio_pin1);
	wait(60);

	// then release the pin and pullup takes it back to +3.3V 
	gpiodev->gpio_function_set(ds18b20_gpio_pin1,GPIO_INPUT);
	wait(7);
}

void ds18b20_writebit1(){
	gpiodev->gpio_function_set(ds18b20_gpio_pin1,GPIO_OUTPUT);	

	// pull down the pin for about 10µs
	gpiodev->gpio_clear(ds18b20_gpio_pin1);
	wait(13);

	// then release the pin and pullup takes it back to +3.3V 
	gpiodev->gpio_function_set(ds18b20_gpio_pin1,GPIO_INPUT);
	wait(54);
}

void ds18b20_writebyte(uint8_t cmd){
	for(int i=0;i<=7;i++){
		if(cmd%2)ds18b20_writebit1();
		else ds18b20_writebit0();
		cmd>>=1;
	}
}

uint8_t ds18b20_readbyte(){
	uint8_t val = 0;
	for(int i=0;i<=7;i++){
		val>>=1;	
		if(ds18b20_readbit())
			val+=0b10000000;
	}
	return val;
}

uint8_t ds18b20_readbytes(uint8_t buf[], uint8_t bytes){
	for(int k=0;k<=bytes-1;k++) buf[k]=ds18b20_readbyte();
	// for(k=0;k<=bytes-1;k++) printk("buf[%d] = %x\n",k,buf[k]);
	return 0;
}


int ds18b20_issue_command(uint8_t cmd){

	switch(cmd){
		case SEARCH_ROM:
		case READ_ROM:
		case MATCH_ROM:
		case SKIP_ROM:
		case ALARM_SEARCH_ROM:
		case CONVERT_T_FUNC:
		case WRITE_SCRATCHPAD_FUNC:
		case READ_SCRATCHPAD_FUNC:
		case COPY_SCRATCHPAD_FUNC:
		case RECALL_E2_FUNC:
		case READ_POWER_SUPPLY_FUNC:{
			ds18b20_writebyte(cmd);
			break;
		}
		default:{
			printk("%s: Undefined command for ds18b20!\n",__func__);
			return -1;
			break;
		}
	}

	return 0;
}

long int ds18b20_calculate_temp(uint8_t buff[]){
	uint8_t lsb = buff[0];
	uint8_t msb = buff[1];
	uint8_t msbit = 0;
	long int temp = 0;

	// read first 8 bits from lsb (2^-4 - 2^3)
	for(int i=0;i<=7;i++){
		temp+=(lsb%2)*digits[i];
		lsb>>=1;
	}
	// read 2 bits from msb (2^4 - 2^5)
	for(int i=0;i<=1;i++){
		temp+=(msb%2)*digits[8+i];
		msb>>=1;
	}
	// read the final (most significant bit) into own variable
	msbit = (msb%2)*digits[10];
	msb>>=1;

	// if the sign bit holds "1" then the temperature is negative
	// and the accumulated values up to 2^5 should be subtracted from
	// the last (msbit) digit value.
	if(msb%2) temp = -(msbit-temp);
	else temp = msbit+temp;
 
	return temp;
}

long int ds18b20_get_temperature(){
	uint8_t buff[9];

	// Initialize the connection first
	ds18b20_initialize_connection();

	// Skip the rom detection for now
	ds18b20_issue_command(SKIP_ROM);

	// Ask the sensor to store the new 
	// temperature to the scratchpad memory
	ds18b20_issue_command(CONVERT_T_FUNC);

	// Wait a little bit for the storing to finish
	wait(1000);

	// Re-initialize connection
	ds18b20_initialize_connection();

	// Skip the rom detection for now
	ds18b20_issue_command(SKIP_ROM);

	// Issue a read scratchpad memory -command
	ds18b20_issue_command(READ_SCRATCHPAD_FUNC);

	// Read 9 bytes from the sensor
	ds18b20_readbytes(buff,9);

	// Calculate the temperature from the read values
	return ds18b20_calculate_temp(buff);
}


int ds18b20_exit(){
	printk("%s: ds18b20 driver exit! \n",__func__);
	return 0;
}


module_init(ds18b20_init);
module_exit(ds18b20_exit);

