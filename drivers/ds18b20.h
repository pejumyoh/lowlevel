#ifndef INCLUDE_DS18B20_H
#define INCLUDE_DS18B20_H

#include <stdint.h>

int ds18b20_initialize_connection();
int ds18b20_issue_command(uint8_t cmd);
uint8_t ds18b20_readbytes(uint8_t buf[], uint8_t bytes);
long int ds18b20_get_temperature();

#endif
