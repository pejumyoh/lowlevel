#ifndef INCLUDE_MAIL_H
#define INCLUDE_MAIL_H

// Mailbox Register layout
#define BCM2708_MAILBOX_BASE		0x2000B880			/* Base address */
#define BCM2708_MAILBOX_READ_R		BCM2708_MAILBOX_BASE + 0x00	/* Mailbox read register */
#define BCM2708_MAILBOX_POLL_R		BCM2708_MAILBOX_BASE + 0x10	/* Mailbox polling register */
#define BCM2708_MAILBOX_SENDER_R	BCM2708_MAILBOX_BASE + 0x14	/* Mailbox Sender information register */
#define BCM2708_MAILBOX_STATUS_R	BCM2708_MAILBOX_BASE + 0x18	/* Mailbox status register */
#define BCM2708_MAILBOX_CONFIG_RW	BCM2708_MAILBOX_BASE + 0x1C	/* Mailbox configuration register */
#define BCM2708_MAILBOX_WRITE_W		BCM2708_MAILBOX_BASE + 0x20	/* Mailbox write register */

// Mailbox Channel layout
#define BCM2708_MAILBOX_CHANNEL_PM		0	/* Power management interface */
#define BCM2708_MAILBOX_CHANNEL_FB		1	/* Framebuffer */
#define BCM2708_MAILBOX_CHANNEL_VUART		2	/* Virtual UART */
#define BCM2708_MAILBOX_CHANNEL_VCHIQ		3	/* VCHIQ interface */
#define BCM2708_MAILBOX_CHANNEL_LEDS		4	/* LEDs interface */
#define BCM2708_MAILBOX_CHANNEL_BUTTONS		5	/* Buttons interface */
#define BCM2708_MAILBOX_CHANNEL_TS		6	/* Touchscreen interface */


uint32_t writemailbox(uint32_t data, uint8_t channel);
uint32_t readmailbox(uint8_t channel);

#endif
