#ifndef INCLUDE_BCM2708_UART
#define INCLUDE_BCM2708_UART

#include "../pl011_uart.h"

#define BCM2708_UART_BASE	0x20201000

int bcm2708_uart_init();
int bcm2708_uart_tx_byte(unsigned char c);
unsigned char bcm2708_uart_rx_byte();

#endif
