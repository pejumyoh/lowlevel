#include "console.h"
#include "interrupt.h"
#include "bcm2708_interrupts.h"
#include "bcm2708_gpio.h"
#include "io.h"

volatile struct irqcontroller *irqbase = (volatile struct irqcontroller*)(BCM2708_IRQCONTROLLER_BASE);

void bcm2708_irq_pending(uint32_t* irqs, uint16_t* count){
	int i;
	uint32_t regval;
	int index = 0;
	/* Check which interrupts are pending */

	*count=0;
	/* Basic Pending Interrupts */
	regval = irqbase->IRQ_basic_pending;
	for(i=0;i<=9;i++){
		if(regval&(1<<i)){
			irqs[index]=BASIC_IRQS_BASE+i;
		//	printk("%s: INTERRUPT PENDING... irq = %d\n",__func__,irqs[index]);
			index+=1;
		}
	}

	/* Shared Interrupts 2 */
	regval = irqbase->IRQ_pending_2;
	for(i=0;i<=31;i++){
		if(regval&(1<<i)){
			irqs[index]=SHARED_IRQS_2_BASE+i;
		//	printk("%s: INTERRUPT PENDING... irq = %d\n",__func__,irqs[index]);
			index+=1;
		}
	}

	/* Shared Interrupts 1 */
	/* Leave scheduler (system timer 1) to last one to serve */
	regval = irqbase->IRQ_pending_1;
	for(i=31;i>=1;i--){
		if(regval&(1<<i)){
			irqs[index]=SHARED_IRQS_1_BASE+i;
		//	printk("%s: INTERRUPT PENDING... irq = %d\n",__func__,irqs[index]);
			index+=1;
		}
	}

	*count=index;
}

int bcm2708_irq_init(void){

	struct irqcontroller *irqbase = (struct irqcontroller*)(BCM2708_IRQCONTROLLER_BASE);

	// enable all GPIO interrupts in the interrupt controller
	uint16_t gpio_irqbits = SHARED_GPIO_B3_IRQ%32;	
	irqbase->Enable_IRQs_2 = (1<<gpio_irqbits);

	// enable UART interrupt in the interrupt controller
	uint16_t uart_irqbits = SHARED_UART_IRQ%32;	
	irqbase->Enable_IRQs_2 = (1<<uart_irqbits);

	// enable system timer 1 interrupt in the interrupt controller
	uint16_t stimer1_irqbits = SHARED_SYSTEMTIMER_COMPARE_C1_IRQ%32;
	irqbase->Enable_IRQs_1 = (1<<stimer1_irqbits);

	// Register the platform -specific probe function for irq subsystem
	irq_register_probe_pending(bcm2708_irq_pending);

	return 0;
}
