#ifndef INCLUDE_BCM2708_SYSTEMTIMER_H
#define INCLUDE_BCM2708_SYSTEMTIMER_H

#include <stdint.h>

/* -------------------------------- 1Mhz Clock ----------------------------------- */

#define BCM2708_SYSTEMTIMER_BASE	0x20003000

struct systemtimer
{
	volatile uint32_t	timer_cs;			// System Timer Control and Status
	volatile uint32_t	timer_clo;			// System Timer Counter Lower 32 bits
	volatile uint32_t	timer_chi;			// System Timer Counter Upper 32 bits
	volatile uint32_t	timer_c0;			// System Timer Compare 0; corresponds to IRQ line 0 (ALREADY USED BY THE GPU)
	volatile uint32_t	timer_c1;			// System Timer Compare 1; corresponds to IRQ line 1
	volatile uint32_t	timer_c2;			// System Timer Compare 2; corresponds to IRQ line 2
	volatile uint32_t	timer_c3;			// System Timer Compare 3; corresponds to IRQ line 3
};

#define BCM2708_SYSTEMTIMER_CS		SYSTEMTIMER_BASE + 0x0		// System Timer Control and Status register
#define BCM2708_SYSTEMTIMER_CLO		SYSTEMTIMER_BASE + 0x4		// System Timer Counter Lower 32 bits
#define BCM2708_SYSTEMTIMER_CHI		SYSTEMTIMER_BASE + 0x8		// System Timer Counter Upper 32 bits
#define BCM2708_SYSTEMTIMER_C0		SYSTEMTIMER_BASE + 0xc		// System Timer Compare 0; corresponds to IRQ line 0
#define BCM2708_SYSTEMTIMER_C1		SYSTEMTIMER_BASE + 0x10		// System Timer Compare 1; corresponds to IRQ line 1
#define BCM2708_SYSTEMTIMER_C2		SYSTEMTIMER_BASE + 0x14		// System Timer Compare 2; corresponds to IRQ line 2
#define BCM2708_SYSTEMTIMER_C3		SYSTEMTIMER_BASE + 0x18		// System Timer Compare 3; corresponds to IRQ line 3

// Bit definition for System Timer Control and Status
#define BCM2708_SYSTEMTIMER_CS_MATCH0_CLEAR	(1 << 0)
#define BCM2708_SYSTEMTIMER_CS_MATCH1_CLEAR	(1 << 1)
#define BCM2708_SYSTEMTIMER_CS_MATCH2_CLEAR	(1 << 2)
#define BCM2708_SYSTEMTIMER_CS_MATCH3_CLEAR	(1 << 3)

void bcm2708_systemtimer_init();
uint32_t bcm2708_systemtimer_getdelay();
void bcm2708_stimer_enable_irq(uint32_t irq, uint32_t mode);
uint32_t bcm2708_systemtimer_getcurrenttime();

#endif
