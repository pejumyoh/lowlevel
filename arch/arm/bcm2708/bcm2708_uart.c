#include "bcm2708_uart.h"
#include "io.h"
#include "txtdev.h"
#include "device.h"
#include "gpio.h"
#include "console.h"
#include "initcalls.h"
#include "uart.h"
#include "lowlevel.h"
#include "bcm2708_interrupts.h"
#include "interrupt.h"

#define BCM2708_UART_GPIO1	14
#define BCM2708_UART_GPIO2	15

volatile struct pl011_uart* bcm2708_uart0 = (volatile struct pl011_uart*)BCM2708_UART_BASE;

struct device bcm2708_uart_dev;
struct device_driver bcm2708_uart_driver;

static uint32_t bcm2708_uart_irq_lines[NR_UART_INTERRUPTS] = {0};

volatile struct gpio_api* gpiodev = NULL;

int bcm2708_uart_tx_byte(unsigned char c){
	// loop until the transfer buffer has space.
	while(bcm2708_uart0->UARTFR & PL011_UARTFR_TXFF){;}
	// then send the character to uart
	bcm2708_uart0->UARTDR = c;
	return 0;
}

unsigned char bcm2708_uart_rx_byte(){
	// loop until the receive buffer becomes non-empty
	while(bcm2708_uart0->UARTFR & PL011_UARTFR_RXFE){;}
	// read a character from uart data register
	return (unsigned char)(bcm2708_uart0->UARTDR);
}

int bcm2708_uart_set_baudrate(unsigned int baudrate)
{	
	// TODO
	return 0;
}

int bcm2708_uart_set_parity(uint8_t parity)
{
	// TODO
	return 0;
}

static struct uart_api bcm2708_uart_api = {
	.uart_tx_byte = bcm2708_uart_tx_byte,
	.uart_rx_byte = bcm2708_uart_rx_byte,
	.uart_set_baudrate = bcm2708_uart_set_baudrate,
	.uart_set_parity = bcm2708_uart_set_parity,
};


static void bcm2708_uart_irq_handler(uint32_t irq){

	// Check which bits are set in the IMSC Register, convert them into interrupt numbers
	// and handle the corresponding interrupts

	unsigned int status = bcm2708_uart0->UARTMIS;
	bcm2708_uart0->UARTICR = 0x7F2;
	flush_cache();		
	dmb();

	if(status & PL011_UARTMIS_CTSMMIS){
		manage_interrupt(UART_IRQ_CTSMIM);
	}
	else if(status & PL011_UARTMIS_RXMIS){
		manage_interrupt(UART_IRQ_RXIM);
	}
	else if(status & PL011_UARTMIS_TXMIS){
		manage_interrupt(UART_IRQ_TXIM);
	}
	else if(status & PL011_UARTMIS_RTMIS){
		// There was data waiting in the UART read FIFO 
		// but timeout occured. Convert this irq into UART_IRQ_RXIM
		// which handles the emptying of the read buffer
		manage_interrupt(UART_IRQ_RXIM);
		bcm2708_uart0->UARTICR = PL011_UARTICR_RTIC;
	}
	else if(status & PL011_UARTMIS_FEMIS){
		printk("PL011_UARTMIS_FEMIS\n");
	//	manage_interrupt(UART_IRQ_FEIM);
	}
	else if(status & PL011_UARTMIS_PEMIS){
		printk("PL011_UARTMIS_PEMIS\n");
	//	manage_interrupt(UART_IRQ_PEIM);
	}
	else if(status & PL011_UARTMIS_BEMIS){
		printk("PL011_UARTMIS_BEMIS\n");
	//	manage_interrupt(UART_IRQ_BEIM);
	}
	else if(status & PL011_UARTMIS_OEMIS){

	/*	http://www.cypress.com/?id=4&rID=39597 states that:
		
		Overrun error occurs when another byte of data arrives even before the previous byte 
		has not been read from the UART's receive buffer. This is mainly due to time taken 
		by CPU to service the UART interrupt in order to remove characters from receive buffer. 
		If the CPU does not service the UART interrupt quick enough and does not read the received 
		byte before the next arrives, then the buffer becomes full and an Overrun error occurs.
		Below are some tips to avoid overrun errors.

		1. Run the CPU at the maximum possible speed. This will speed up the execution of the 
		   UART interrupt (and any other interrupt too).

		2. Keep the UART ISR efficient and as short as possible. For example the ISR could just 
		   read from the UART's RX buffer and transfer it to a RAM buffer and set a flag.  
		   The processing of the received data could be done in the foreground.
		   The time taken to execute the ISR should be less than the time taken to receive the 
		   next data byte.

		3. Writing a C ISR pushes and pops all the virtual registers, paging registers 
		   and A and X registers. This increases the time taken to execute an ISR.  Write the 
		   UART ISR in assembly to make it more efficient.

		4. Other interrupts in the program can also create interrupt latencies which could result 
		   in overrun errors. Under such circumstances, reducing the baud rate of the UART will 
		   also prevent the overrun error.
	*/

		printk("PL011_UARTMIS_OEMIS\n");
	//	manage_interrupt(UART_IRQ_OEIM);
	}
}


int bcm2708_uart_exit(){

	return 0;
}

static void bcm2708_uart_enable_irq(uint32_t irq, uint32_t mode){


}

static void bcm2708_uart_disable_irq(uint32_t irq, uint32_t mode){


}

static void bcm2708_uart_mask_irq(uint32_t irq){

	// Check Masked Interrupt Status (UARTMIS) Register for pending 
	// IRQs and clear them
	if(bcm2708_uart0->UARTMIS & PL011_UARTMIS_CTSMMIS){
		bcm2708_uart0->UARTICR = PL011_UARTICR_CTSMIC;
	}
	else if(bcm2708_uart0->UARTMIS & PL011_UARTMIS_RXMIS){
		bcm2708_uart0->UARTICR = PL011_UARTICR_RXIC;
	}
	else if(bcm2708_uart0->UARTMIS & PL011_UARTMIS_TXMIS){
		bcm2708_uart0->UARTICR = PL011_UARTICR_TXIC;
	}
	else if(bcm2708_uart0->UARTMIS & PL011_UARTMIS_RTMIS){
		bcm2708_uart0->UARTICR = PL011_UARTICR_RTIC;
	}
	else if(bcm2708_uart0->UARTMIS & PL011_UARTMIS_FEMIS){
		bcm2708_uart0->UARTICR = PL011_UARTICR_FEIC;
	}
	else if(bcm2708_uart0->UARTMIS & PL011_UARTMIS_PEMIS){
		bcm2708_uart0->UARTICR = PL011_UARTICR_PEIC;
	}
	else if(bcm2708_uart0->UARTMIS & PL011_UARTMIS_BEMIS){
		bcm2708_uart0->UARTICR = PL011_UARTICR_BEIC;
	}
	else if(bcm2708_uart0->UARTMIS & PL011_UARTMIS_OEMIS){
		bcm2708_uart0->UARTICR = PL011_UARTICR_OEIC;
	}
}

static void bcm2708_uart_unmask_irq(uint32_t irq){


}

static struct irq_chip bcm2708_uart_irq_chip = {
	.name		= "bcm2708_uart",
	.enable_irq	= bcm2708_uart_enable_irq,		/* use for enabling specific interrupt  */
	.disable_irq	= bcm2708_uart_disable_irq,		/* use for disabling specific interrupt */
	.mask_irq	= bcm2708_uart_mask_irq,		/* use for masking specific interrupt   */
	.unmask_irq	= bcm2708_uart_unmask_irq,		/* use for unmasking specific interrupt */
	.nr_irqs	= NR_UART_INTERRUPTS,			/* Number of interrupts that this chip handles */
	.irqvec		= bcm2708_uart_irq_lines,		/* Vector containing the interrupt numbers */
};

int bcm2708_uart_init(){
	
	// Request gpio device api
	gpiodev = (struct gpio_api*) request_device(GPIO_DEV);

	if(gpiodev==NULL){
		printk("%s: Failed to obtain GPIO device!\n",__func__);
		return -1;
	}

	// Reserve the GPIO pins for UART use
	if(gpiocore_request_gpio(BCM2708_UART_GPIO1)){
		printk("%s: Failed to reserve GPIO pin %d\n",__func__, BCM2708_UART_GPIO1);
		return -1;
	}
	if(gpiocore_request_gpio(BCM2708_UART_GPIO2)){
		printk("%s: Failed to reserve GPIO pin %d\n",__func__, BCM2708_UART_GPIO2);
		return -1;
	}

	// Set the GPIO function for the reserved pins to ALT0 (UART mode)
	gpiodev->gpio_function_set(BCM2708_UART_GPIO1,GPIO_ALT0);
	gpiodev->gpio_function_set(BCM2708_UART_GPIO2,GPIO_ALT0);

	// Disable the uart first
	bcm2708_uart0->UARTCR = 0x00000000;

	// Disable pullup/down on UART gpio pins
	gpiodev->gpio_pullupdown_set(BCM2708_UART_GPIO1,GPIO_PUD_UP_DOWN_DISABLE);
	gpiodev->gpio_pullupdown_set(BCM2708_UART_GPIO2,GPIO_PUD_UP_DOWN_DISABLE);

	// Clear pending interrupts.
	bcm2708_uart0->UARTICR = 0x7FF;

	// Set integer & fractional part of baud rate.
	// Divider = UART_CLOCK/(16 * Baud)
	// Fraction part register = (Fractional part * 64) + 0.5
	// UART_CLOCK = 3000000; Baud = 115200.
 
	// Divider = 3000000 / (16 * 115200) = 1.627 = ~1.
	// Fractional part register = (.627 * 64) + 0.5 = 40.6 = ~40.
	bcm2708_uart0->UARTIBRD = 1;
	bcm2708_uart0->UARTFBRD = 40;
 
	// Enable 8 bit data transmission (1 stop bit, no parity).
	bcm2708_uart0->UARTLCR_H = (PL011_UARTLCR_H_WLEN_8B);

	// Mask all interrupts.
	bcm2708_uart0->UARTIMSC = (PL011_UARTIMSC_CTSMIM|PL011_UARTIMSC_RXIM|PL011_UARTIMSC_TXIM|
				   PL011_UARTIMSC_RTIM|PL011_UARTIMSC_FEIM|PL011_UARTIMSC_PEIM|
				   PL011_UARTIMSC_BEIM|PL011_UARTIMSC_OEIM);

	// Enable UART0, receive & transfer part of UART.
	bcm2708_uart0->UARTCR = (PL011_UARTCR_UARTEN|PL011_UARTCR_TXE|PL011_UARTCR_RXE);

	// Fill-in the Interrupt line data:
	// Generic masked UART interrupt
	bcm2708_uart_irq_lines[0] = SHARED_UART_IRQ;

	// Separate UART interrupts included in the 
	// Generic UART interrupt mask
	bcm2708_uart_irq_lines[1] = UART_IRQ_CTSMIM;
	bcm2708_uart_irq_lines[2] = UART_IRQ_RXIM;
	bcm2708_uart_irq_lines[3] = UART_IRQ_TXIM;
	bcm2708_uart_irq_lines[4] = UART_IRQ_RTIM;
	bcm2708_uart_irq_lines[5] = UART_IRQ_FEIM;
	bcm2708_uart_irq_lines[6] = UART_IRQ_PEIM;
	bcm2708_uart_irq_lines[7] = UART_IRQ_BEIM;
	bcm2708_uart_irq_lines[8] = UART_IRQ_OEIM;

	// Register UART IRQ chip to interrupt manager
	irq_register_chip(&bcm2708_uart_irq_chip);      

	// Request an interrupt line for UART interrupts
	request_irq(bcm2708_uart_irq_handler,SHARED_UART_IRQ);

	// Fill in the device info
	bcm2708_uart_dev.name = "bcm2708_uart";
	bcm2708_uart_dev.type = UART_DEV,
	bcm2708_uart_dev.driver = &bcm2708_uart_driver,
	bcm2708_uart_dev.irq_info.start = -1,
	bcm2708_uart_dev.irq_info.end = -1,
	bcm2708_uart_dev.data = NULL,

	// Fill in the device driver info
	bcm2708_uart_driver.name = "bcm2708_uart";
	bcm2708_uart_driver.dev = &bcm2708_uart_dev;
	bcm2708_uart_driver.driver_init = bcm2708_uart_init;
	bcm2708_uart_driver.driver_exit = bcm2708_uart_exit;
	bcm2708_uart_driver.api = &bcm2708_uart_api;

	device_register(&bcm2708_uart_dev);
	device_driver_register(&bcm2708_uart_driver);

	printk("%s: UART device initialized!\n",__func__);

	return 0;
}

//core_init(bcm2708_uart_init);
//core_exit(bcm2708_uart_exit);
