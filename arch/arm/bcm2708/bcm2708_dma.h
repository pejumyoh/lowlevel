#ifndef INCLUDE_BCM2708_DMA_H
#define INCLUDE_BCM2708_DMA_H

#include <stddef.h>
#include <stdint.h>


#define BCM2708_DMA_BASE		0x20007000

#define BCM2708_DMA0_BASE		BCM2708_DMA_BASE
#define BCM2708_DMA1_BASE		BCM2708_DMA_BASE + 0x100
#define BCM2708_DMA2_BASE		BCM2708_DMA_BASE + 0x200
#define BCM2708_DMA3_BASE		BCM2708_DMA_BASE + 0x300
#define BCM2708_DMA4_BASE		BCM2708_DMA_BASE + 0x400
#define BCM2708_DMA5_BASE		BCM2708_DMA_BASE + 0x500
#define BCM2708_DMA6_BASE		BCM2708_DMA_BASE + 0x600
#define BCM2708_DMA7_BASE		BCM2708_DMA_BASE + 0x700
#define BCM2708_DMA8_BASE		BCM2708_DMA_BASE + 0x800
#define BCM2708_DMA9_BASE		BCM2708_DMA_BASE + 0x900
#define BCM2708_DMA10_BASE		BCM2708_DMA_BASE + 0xa00
#define BCM2708_DMA11_BASE		BCM2708_DMA_BASE + 0xb00
#define BCM2708_DMA12_BASE		BCM2708_DMA_BASE + 0xc00
#define BCM2708_DMA13_BASE		BCM2708_DMA_BASE + 0xd00
#define BCM2708_DMA14_BASE		BCM2708_DMA_BASE + 0xe00
#define BCM2708_DMA15_BASE		BCM2708_DMA_BASE + 0xdfe000

#define BCM2708_DMA0_CONBLK_AD		BCM2708_DMA0_BASE + 0x4
#define BCM2708_DMA1_CONBLK_AD		BCM2708_DMA1_BASE + 0x4
#define BCM2708_DMA2_CONBLK_AD		BCM2708_DMA2_BASE + 0x4
#define BCM2708_DMA3_CONBLK_AD		BCM2708_DMA3_BASE + 0x4
#define BCM2708_DMA4_CONBLK_AD		BCM2708_DMA4_BASE + 0x4
#define BCM2708_DMA5_CONBLK_AD		BCM2708_DMA5_BASE + 0x4
#define BCM2708_DMA6_CONBLK_AD		BCM2708_DMA6_BASE + 0x4
#define BCM2708_DMA7_CONBLK_AD		BCM2708_DMA7_BASE + 0x4
#define BCM2708_DMA8_CONBLK_AD		BCM2708_DMA8_BASE + 0x4
#define BCM2708_DMA9_CONBLK_AD		BCM2708_DMA9_BASE + 0x4
#define BCM2708_DMA10_CONBLK_AD		BCM2708_DMA10_BASE + 0x4
#define BCM2708_DMA11_CONBLK_AD		BCM2708_DMA11_BASE + 0x4
#define BCM2708_DMA12_CONBLK_AD		BCM2708_DMA12_BASE + 0x4
#define BCM2708_DMA13_CONBLK_AD		BCM2708_DMA13_BASE + 0x4
#define BCM2708_DMA14_CONBLK_AD		BCM2708_DMA14_BASE + 0x4
#define BCM2708_DMA15_CONBLK_AD		BCM2708_DMA15_BASE + 0x4


#define BCM2708_DMA_INT_STATUS_ENABLE_BASE	BCM2708_DMA_BASE + 0xfe0

struct bcm2708_dmareg
{
	uint32_t cs;		/* Control and Status */
	uint32_t conblk_ad;	/* Control Block address */
	uint32_t ti;		/* Transfer Information	*/
	uint32_t source_ad;	/* Source Address */
	uint32_t dest_ad;	/* Destination address */
	uint32_t txfr_len;	/* Transfer length */
	uint32_t stride;	/* 2D Stride */
	uint32_t nextconblk;	/* Next CB Address */
	uint32_t debug1;	/* Debug1 */
	uint32_t debug2;	/* Debug2 */
};


struct bcm2708_dmacb
{
	uint32_t ti;		/* Transfer Information	*/
	uint32_t source_ad;	/* Source Address */
	uint32_t dest_ad;	/* Destination address */
	uint32_t txfr_len;	/* Transfer length */
	uint32_t stride;	/* 2D Stride */
	uint32_t nextconblk;	/* Next CB Address */
	uint32_t debug1;	/* Debug1 */
	uint32_t debug2;	/* Debug2 */
};

/* The DMA is started by writing the address of a CB structure into the CONBLK_AD register 
and then setting the ACTIVE bit. The DMA will fetch the CB from the address set in the SCB_ADDR 
field of this reg and it will load it into the read-only registers described below. It will 
then begin a DMA transfer according to the information in the CB. When it has completed the 
current DMA transfer (length => 0) the DMA will update the CONBLK_AD register with the contents 
of the NEXTCONBK register, fetch a new CB from that address, and start the whole procedure 
once again. The DMA will stop (and clear the ACTIVE bit) when it has completed a DMA transfer 
and the NEXTCONBK register is set to 0x0000_0000. It will load this value into the CONBLK_AD 
reg and then stop. */


struct bcm2708_dma_int_status_enable
{
	uint32_t int_status;	/* Interrupt status of each DMA channel		*/
	uint32_t enable;	/* Global enable bits for each DMA channel	*/
};


/* Control bits for DMA CS Register */

#define		BCM2708_DMA_CS_ACTIVE					(1<<0)		/* This bit enables the DMA. */
#define		BCM2708_DMA_CS_END					(1<<1)		/* DMA End Flag	*/
#define		BCM2708_DMA_CS_INT					(1<<2)		/* Interrupt Status */
#define		BCM2708_DMA_CS_DREQ					(1<<3)		/* DREQ State */
#define		BCM2708_DMA_CS_PAUSED					(1<<4)		/* DMA Paused State */
#define		BCM2708_DMA_CS_DREQ_STOPS_DMA				(1<<5)		/* DMA Paused by DREQ State */
#define		BCM2708_DMA_CS_WAITING_FOR_OUTSTANDING_WRITES		(1<<6)		/* DMA is Waiting for the Last Write to be Received */
#define		BCM2708_DMA_CS_ERROR					(1<<8)		/* DMA Error */
#define		BCM2708_DMA_CS_PRIORITY					(0b1111<<16)	/* AXI Priority Level */
#define		BCM2708_DMA_CS_PANIC_PRIORITY				(0b1111<<20)	/* AXI Panic Priority Level */
#define		BCM2708_DMA_CS_WAIT_FOR_OUTSTANDING_WRITES		(1<<28)		/* Wait for outstanding writes */
#define		BCM2708_DMA_CS_DISDEBUG					(1<<29)		/* Disable debug pause signal */
#define		BCM2708_DMA_CS_ABORT					(1<<30)		/* Abort DMA */
#define		BCM2708_DMA_CS_RESET					(1<<31)		/* DMA Channel Reset */

/* Control bits for DMA TI Register */
#define		BCM2708_DMA_TI_INTEN					(1<<0)		/* Interrupt Enable */
#define		BCM2708_DMA_TI_TDMODE					(1<<1)		/* 2D Mode */
#define		BCM2708_DMA_TI_WAIT_RESP				(1<<3)		/* Wait for a Write Response */
#define		BCM2708_DMA_TI_DEST_INC					(1<<4)		/* Destination Address Increment */
#define		BCM2708_DMA_TI_DEST_WIDTH				(1<<5)		/* Destination Transfer Width */
#define		BCM2708_DMA_TI_DEST_DREQ				(1<<6)		/* Control Destination Writes with DREQ */
#define		BCM2708_DMA_TI_DEST_IGNORE				(1<<7)		/* Ignore Writes */
#define		BCM2708_DMA_TI_SRC_INC					(1<<8)		/* Source Address Increment */
#define		BCM2708_DMA_TI_SRC_WIDTH				(1<<9)		/* Source Transfer Width */
#define		BCM2708_DMA_TI_SRC_DREQ					(1<<10)		/* Control Source Reads with DREQ */
#define		BCM2708_DMA_TI_SRC_IGNORE				(1<<11)		/* Ignore Reads */
#define		BCM2708_DMA_TI_BURST_LENGTH				(0b1111 <<12)	/* Burst Transfer Length, 4 bits */
#define		BCM2708_DMA_TI_PERMAP					(0x11111<<16)	/* Peripheral Mapping */
#define		BCM2708_DMA_TI_WAITS					(0x11111<<21)	/* Add Wait Cycles */
#define		BCM2708_DMA_TI_NO_WIDE_BURSTS				(1<<26)		/* Don't Do wide writes as a 2 beat burst */


void bcm2708_dma_init(void);
void dma_start_and_wait(struct bcm2708_dmacb* dmacb);

#endif
