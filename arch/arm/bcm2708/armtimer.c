#include "armtimer.h"

void init_armtimer_irq(uint32_t time)
{
	struct armtimer* timer = (struct armtimer*)ARMTIMER_BASE;

	timer->Load	= time;
	timer->Control	= ARMTIMER_CTRL_32BIT | ARMTIMER_CTRL_ENABLE | ARMTIMER_CTRL_INT_ENABLE | ARMTIMER_CTRL_PRESCALE_1;
}
