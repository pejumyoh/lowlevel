#include "bcm2708_dma.h"
#include "lowlevel.h"

volatile struct bcm2708_dmareg* dmareg0 = NULL;
volatile struct bcm2708_dma_int_status_enable* dma_sten = NULL;

void bcm2708_dma_init()
{
	dmareg0 = (volatile struct bcm2708_dmareg*)(BCM2708_DMA0_BASE);
	dma_sten = (volatile struct bcm2708_dma_int_status_enable*)(BCM2708_DMA_INT_STATUS_ENABLE_BASE);
}
void dma_start_and_wait(struct bcm2708_dmacb* dmacb)
{
	dmareg0->cs = BCM2708_DMA_CS_RESET;
	dma_sten->enable = 0x1;
	dmareg0->conblk_ad = (uint32_t)(dmacb);
	dmareg0->cs = BCM2708_DMA_CS_ACTIVE;
	
	while(dmareg0->cs & BCM2708_DMA_CS_ACTIVE){
		// If DMA transfer has ended, exit this loop
		;
	}
}
