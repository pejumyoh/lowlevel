#ifndef INCLUDE_BCM2708_GPIO_H
#define INCLUDE_BCM2708_GPIO_H

#include <stdint.h>
#include "gpio.h"

#define BCM2708_GPIO_BASE	0x20200000
#define BCM2708_GPIO_PINS	54


/* GPIO Controller register layout */

struct bcm2708_gpio
{
	uint32_t gpfsel[6];	/* GPIO Function Select Registers */
	uint32_t reserved1;

	uint32_t gpset[2];	/* GPIO Pin Output Set Registers */
	uint32_t reserved2;

	uint32_t gpclr[2];	/* GPIO Pin Output Clear Registers */
	uint32_t reserved3;

	uint32_t gplev[2];	/* GPIO Pin Level Registers */
	uint32_t reserved4;

	uint32_t gpeds[2];	/* GPIO Pin Event Detect Status Registers */
	uint32_t reserved5;

	uint32_t gpren[2];	/* GPIO Pin Rising Edge Detect Enable Registers */
	uint32_t reserved6;

	uint32_t gpfen[2];	/* GPIO Pin Falling Edge Detect Enable Registers */
	uint32_t reserved7;

	uint32_t gphen[2];	/* GPIO Pin High Detect Enable Registers */
	uint32_t reserved8;

	uint32_t gplen[2];	/* GPIO Pin Low Detect Enable Registers */
	uint32_t reserved9;

	uint32_t gparen[2];	/* GPIO Pin Async. Rising Edge Detect Registers */
	uint32_t reserved10;

	uint32_t gpafen[2];	/* GPIO Pin Async. Falling Edge Detect Registers */
	uint32_t reserved11;

	uint32_t gppud;		/* GPIO Pin Pull-up/down Enable Registers */
	uint32_t gppudclk[2];	/* GPIO Pin Pull-up/down Enable Clock Registers */
};


/* GPIO Function Select Registers */

#define BCM2708_GPFSEL0 	BCM2708_GPIO_BASE + 0x0		/* GPIO Function Select 0 */
#define BCM2708_GPFSEL1 	BCM2708_GPIO_BASE + 0x4		/* GPIO Function Select 1 */
#define BCM2708_GPFSEL2 	BCM2708_GPIO_BASE + 0x8		/* GPIO Function Select 2 */
#define BCM2708_GPFSEL3 	BCM2708_GPIO_BASE + 0xC		/* GPIO Function Select 3 */
#define BCM2708_GPFSEL4 	BCM2708_GPIO_BASE + 0x10	/* GPIO Function Select 4 */
#define BCM2708_GPFSEL5 	BCM2708_GPIO_BASE + 0x14	/* GPIO Function Select 5 */

/* GPIO Pin Output Set Registers */

#define BCM2708_GPSET0		BCM2708_GPIO_BASE + 0x1C	/* GPIO Pin Output Set 0 */
#define BCM2708_GPSET1		BCM2708_GPIO_BASE + 0x20	/* GPIO Pin Output Set 1 */ 

/* GPIO Pin Output Clear Registers */

#define BCM2708_GPCLR0		BCM2708_GPIO_BASE + 0x28	/* GPIO Pin Output Clear 0 */ 
#define BCM2708_GPCLR1		BCM2708_GPIO_BASE + 0x2C	/* GPIO Pin Output Clear 1 */ 

/* GPIO Pin Level Registers */

#define BCM2708_GPLEV0		BCM2708_GPIO_BASE + 0x34	/* GPIO Pin Level 0 */ 
#define BCM2708_GPLEV1		BCM2708_GPIO_BASE + 0x38	/* GPIO Pin Level 1 */ 

/* GPIO Pin Event Detect Status Registers */

#define BCM2708_GPEDS0		BCM2708_GPIO_BASE + 0x40	/* GPIO Pin Event Detect Status 0 */ 
#define BCM2708_GPEDS1		BCM2708_GPIO_BASE + 0x44	/* GPIO Pin Event Detect Status 1 */ 

/* GPIO Pin Rising Edge Detect Enable Registers */

#define BCM2708_GPREN0		BCM2708_GPIO_BASE + 0x4C	/* GPIO Pin Rising Edge Detect Enable 0 */ 
#define BCM2708_GPREN1		BCM2708_GPIO_BASE + 0x50	/* GPIO Pin Rising Edge Detect Enable 1 */ 

/* GPIO Pin Falling Edge Detect Enable Registers */

#define BCM2708_GPFEN0		BCM2708_GPIO_BASE + 0x58	/* GPIO Pin Falling Edge Detect Enable 0 */ 
#define BCM2708_GPFEN1		BCM2708_GPIO_BASE + 0x5C	/* GPIO Pin Falling Edge Detect Enable 1 */ 

/* GPIO Pin High Detect Enable Registers */

#define BCM2708_GPHEN0		BCM2708_GPIO_BASE + 0x64	/* GPIO Pin High Detect Enable 0 */ 
#define BCM2708_GPHEN1		BCM2708_GPIO_BASE + 0x68	/* GPIO Pin High Detect Enable 1 */ 

/* GPIO Pin Low Detect Enable Registers */

#define BCM2708_GPLEN0		BCM2708_GPIO_BASE + 0x70	/* GPIO Pin Low Detect Enable 0 */ 
#define BCM2708_GPLEN1		BCM2708_GPIO_BASE + 0x74	/* GPIO Pin Low Detect Enable 1 */ 

/* GPIO Pin Async. Rising Edge Detect Registers */

#define BCM2708_GPAREN0		BCM2708_GPIO_BASE + 0x7C	/* GPIO Pin Async. Rising Edge Detect 0 */ 
#define BCM2708_GPAREN1		BCM2708_GPIO_BASE + 0x80	/* GPIO Pin Async. Rising Edge Detect 1 */ 

/* GPIO Pin Async. Falling Edge Detect Registers */

#define BCM2708_GPAFEN0		BCM2708_GPIO_BASE + 0x88	/* GPIO Pin Async. Falling Edge Detect 0 */ 
#define BCM2708_GPAFEN1		BCM2708_GPIO_BASE + 0x8C	/* GPIO Pin Async. Falling Edge Detect 1 */ 

/* GPIO Pin Pull-up/down Enable Registers */

#define BCM2708_GPPUD		BCM2708_GPIO_BASE + 0x94	/* GPIO Pin Pull-up/down Enable */ 

/* GPIO Pin Pull-up/down Enable Clock Registers */

#define BCM2708_GPPUDCLK0	BCM2708_GPIO_BASE + 0x98	/* GPIO Pin Pull-up/down Enable Clock 0 */ 
#define BCM2708_GPPUDCLK1	BCM2708_GPIO_BASE + 0x9C	/* GPIO Pin Pull-up/down Enable Clock 1 */ 

void bcm2708_gpio_enable_irq(uint32_t irq, uint32_t mode);
int bcm2708_gpio_init();

#endif
