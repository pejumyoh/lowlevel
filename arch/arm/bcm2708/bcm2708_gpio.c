#include "bcm2708_gpio.h"
#include "console.h"
#include "lowlevel.h"
#include "bcm2708_interrupts.h"
#include "interrupt.h"
#include "debug.h"
#include "gpio.h"
#include "initcalls.h"
#include "device.h"
#include "io.h"

volatile struct bcm2708_gpio* gpio = NULL;

uint32_t bcm2708_irq_to_gpio(uint32_t irq);
uint32_t bcm2708_gpio_to_irq(uint32_t pin);
int bcm2708_gpio_init();
int bcm2708_gpio_exit();

extern void wait(uint32_t time);

static uint32_t bcm2708_irq_lines[NR_GPIO_INTERRUPTS] = {0};


#define GPIO_RISING_DETECT              (1<<1)
#define GPIO_FALLING_DETECT             (1<<2)
#define GPIO_ASYNCH_RISING_DETECT       (1<<3)
#define GPIO_ASYNCH_FALLING_DETECT      (1<<4)
#define GPIO_HIGH_DETECT                (1<<5)
#define GPIO_LOW_DETECT                 (1<<6)


/* Loop <delay> times in a way that the compiler won't optimize away. */
static void delay(int32_t count){
        int32_t i = 0;
        for(i=count;i>0;){ i--; }
}

// ----------------------------------------- IRQ CHIP ROUTINES -----------------------------------------------
void bcm2708_gpio_enable_irq(uint32_t irq, uint32_t mode){

	NULLTST(gpio);

	// Enable the interrupt for pin corresponding to 'irq'
	uint32_t pin = bcm2708_irq_to_gpio(irq);
	uint32_t gpio_bank = pin/32;
	uint32_t gpio_pin  = pin%32;

	if(mode&GPIO_RISING_DETECT){
		// Enable rising edge detect
		gpio->gpren[gpio_bank] = (gpio->gpren[gpio_bank]|(1<<gpio_pin));
	}
	if(mode&GPIO_FALLING_DETECT){
		// Enable falling edge detect
		gpio->gpfen[gpio_bank] = (gpio->gpfen[gpio_bank]|(1<<gpio_pin));
	}
	if(mode&GPIO_ASYNCH_RISING_DETECT){
		// Enable Asynchronous rising edge detect
		gpio->gparen[gpio_bank] = (gpio->gparen[gpio_bank]|(1<<gpio_pin));
	}
	if(mode&GPIO_ASYNCH_FALLING_DETECT){
		// Enable Asynchronous falling edge detect
		gpio->gpafen[gpio_bank] = (gpio->gpafen[gpio_bank]|(1<<gpio_pin));
	}
	if(mode&GPIO_HIGH_DETECT){
		// Enable level high detect
		gpio->gphen[gpio_bank] = (gpio->gphen[gpio_bank]|(1<<gpio_pin));

	}
	if(mode&GPIO_LOW_DETECT){
		// Enable level low detect
		gpio->gplen[gpio_bank] = (gpio->gplen[gpio_bank]|(1<<gpio_pin));
	}
}

void bcm2708_gpio_disable_irq(uint32_t irq, uint32_t mode){

	NULLTST(gpio);

	// Disable the interrupt for pin corresponding to 'irq'
	uint16_t pin = bcm2708_irq_to_gpio(irq);
	uint32_t gpio_bank = pin/32;
	uint32_t gpio_pin  = pin%32;
	uint32_t regval;

	if(mode&GPIO_RISING_DETECT){
		// Disable rising edge detect
		regval = gpio->gpren[gpio_bank];
		regval&=~(1<<gpio_pin);
		gpio->gpren[gpio_bank] = regval;
	}
	if(mode&GPIO_FALLING_DETECT){
		// Disable falling edge detect
		regval = gpio->gpfen[gpio_bank];
		regval&=~(1<<gpio_pin);
		gpio->gpfen[gpio_bank] = regval;
	}
	if(mode&GPIO_ASYNCH_RISING_DETECT){
		// Disable Asynchronous rising edge detect
		regval = gpio->gparen[gpio_bank];
		regval&=~(1<<gpio_pin);
		gpio->gparen[gpio_bank] = regval;
	}
	if(mode&GPIO_ASYNCH_FALLING_DETECT){
		// Disable Asynchronous falling edge detect
		regval = gpio->gpafen[gpio_bank];
		regval&=~(1<<gpio_pin);
		gpio->gpafen[gpio_bank] = regval;
	}
	if(mode&GPIO_HIGH_DETECT){
		// Disable level high detect
		regval = gpio->gphen[gpio_bank];
		regval&=~(1<<gpio_pin);
		gpio->gphen[gpio_bank] = regval;
	}
	if(mode&GPIO_LOW_DETECT){
		// Disable level low detect
		regval = gpio->gplen[gpio_bank];
		regval&=~(1<<gpio_pin);
		gpio->gplen[gpio_bank] = regval;
	}
}

void bcm2708_gpio_mask_irq(uint32_t irq){

	// Check the pending irq's and clear them
	// first bank
	int i;
	for(i=0;i<=31;i++){
		if(gpio->gpeds[0] & (1<<i)){
			gpio->gpeds[0] = (1<<i);
			break;
		}
	}
	// Second bank
	for(i=0;i<=21;i++){
		if(gpio->gpeds[1] & (1<<i)){
			gpio->gpeds[1] = (1<<i);
			break;
		}
	}
	// printk("%s: Masking irqs!\n",__func__);
}


void bcm2708_gpio_unmask_irq(uint32_t irq){
	bcm2708_gpio_enable_irq(irq,GPIO_RISING_DETECT|GPIO_FALLING_DETECT);
}

static struct irq_chip bcm2708_gpio_irq_chip = {
	.name		= "bcm2708_gpio",
	.enable_irq	= bcm2708_gpio_enable_irq,		/* use for enabling specific interrupt	*/
	.disable_irq	= bcm2708_gpio_disable_irq,		/* use for disabling specific interrupt */
	.mask_irq	= bcm2708_gpio_mask_irq,		/* use for masking specific interrupt	*/
	.unmask_irq	= bcm2708_gpio_unmask_irq,		/* use for unmasking specific interrupt */
	.nr_irqs	= NR_GPIO_INTERRUPTS,			/* Number of interrupts that this chip handles */
        .irqvec		= bcm2708_irq_lines,
};

__attribute__((used)) static void bcm2708_irq_handler(uint32_t irq){
	int i;
	uint32_t mapped_irq = 0;

	// Check the Event Detect Status Register (GPEDS) for pending events
	// And convert them to interrupt signals

	// first bank
	for(i=0;i<=31;i++){
		if(gpio->gpeds[0] & (1<<i)){
			mapped_irq = bcm2708_gpio_to_irq(i);
			manage_interrupt(mapped_irq);
		}
	}

	// Second bank
	for(i=0;i<=21;i++){
		if(gpio->gpeds[1] & (1<<i)){
			mapped_irq = bcm2708_gpio_to_irq(32+i);
			manage_interrupt(mapped_irq);
		}
	}

	// printk("%s: RECEIVED IRQ %d\n",__func__,irq);
	// printk("%s: IRQ HANDLER IN GPIO DRIVER!\n",__func__);
}
// ----------------------------------------- IRQ CHIP ROUTINES END --------------------------------------------







// --------------------------------------- GPIO Pin handler functions -----------------------------------------

void bcm2708_gpio_function_set(uint32_t pin, gpio_function func){

	NULLTST(gpio);
	uint32_t gpio_bank = pin/10;
	uint32_t gpio_pin  = pin%10;
	uint32_t function = 0;
	uint32_t regval = 0;

	switch(func){
		case GPIO_INPUT:
		//	printk("%s: GPIO_INPUT\n",__func__);
			function = 0b000;
		break; 
		case GPIO_OUTPUT:
		//	printk("%s: GPIO_OUTPUT\n",__func__);
			function = 0b001;
		break;
		case GPIO_ALT0:
		//	printk("%s: GPIO_ALT0\n",__func__);
			function = 0b100;
		break;
		case GPIO_ALT1:
		//	printk("%s: GPIO_ALT1\n",__func__);
			function = 0b101; 
		break;
		case GPIO_ALT2:
		//	printk("%s: GPIO_ALT2\n",__func__);
			function = 0b110;
		break;
		case GPIO_ALT3:
		//	printk("%s: GPIO_ALT3\n",__func__);
			function = 0b111;
		break;
		case GPIO_ALT4:
		//	printk("%s: GPIO_ALT4\n",__func__);
			function = 0b011;
		break;
		case GPIO_ALT5:
		//	printk("%s: GPIO_ALT5\n",__func__);
			function = 0b010;
		break;
	}

	regval = gpio->gpfsel[gpio_bank];
	regval&=~(0b111<<gpio_pin*3);
	gpio->gpfsel[gpio_bank]=(regval|(function<<gpio_pin*3));
}

void bcm2708_gpio_set(uint32_t pin){
	NULLTST(gpio);
	uint32_t gpio_bank = pin/32;
	uint32_t gpio_pin  = pin%32;
	gpio->gpset[gpio_bank] = (1 << gpio_pin);
}

void bcm2708_gpio_clear(uint32_t pin){
	NULLTST(gpio);
	uint8_t gpio_bank = pin/32;
	uint8_t gpio_pin  = pin%32;
	gpio->gpclr[gpio_bank] = (1 << gpio_pin);
}

uint32_t bcm2708_gpio_pinlevel(uint32_t pin){
	NULLTST(gpio);
	uint32_t gpio_bank = pin/32;
	uint32_t gpio_pin  = pin%32;
	uint32_t plevel = ((gpio->gplev[gpio_bank]&(1<<gpio_pin)) >> gpio_pin);
	return plevel;
}

uint32_t bcm2708_irq_to_gpio(uint32_t irq){
	uint8_t pin = irq - GPIO_IRQ_BASE;
	return pin;
}

uint32_t bcm2708_gpio_to_irq(uint32_t pin){
	uint32_t irq = GPIO_IRQ_BASE + pin; 
	return irq;
}

int bcm2708_gpio_pullupdown_set(uint32_t pin, gpio_pudctrl mode){

	NULLTST(gpio);
	if(pin>BCM2708_GPIO_PINS)
	{
		printk("%s: GPIO pin out of bounds! [0-->%d]\n",__func__,BCM2708_GPIO_PINS);
		return -1;
	}

	uint32_t gpio_bank = pin/32;
	uint32_t gpio_pin  = pin%32;

	printk("%s: gpio_bank = %d and gpio_pin = %d and mode = %x\n",__func__,gpio_bank,gpio_pin,(uint32_t)mode);

	gpio->gppud = (uint32_t)mode;
	delay(150);

	gpio->gppudclk[gpio_bank] = (1 << gpio_pin);
	delay(150);

	gpio->gppudclk[gpio_bank] = 0x00000000;
	delay(150);

	return 0;	
}

// ------------------------------------ GPIO Pin handler functions end -----------------------------------------





struct gpio_data bcm2708_gpio_data = {
	.NR_GPIO_PINS = BCM2708_GPIO_PINS,
};

struct gpio_api bcm2708_gpio_api = {
	.irq_to_gpio = bcm2708_irq_to_gpio,
	.gpio_to_irq = bcm2708_gpio_to_irq,
	.gpio_function_set = bcm2708_gpio_function_set,
	.gpio_set = bcm2708_gpio_set,
	.gpio_clear = bcm2708_gpio_clear,
	.gpio_pinlevel = bcm2708_gpio_pinlevel,
	.gpio_enable_irq = bcm2708_gpio_enable_irq,
	.gpio_disable_irq = bcm2708_gpio_disable_irq,
	.gpio_pullupdown_set = bcm2708_gpio_pullupdown_set,
	.data = &bcm2708_gpio_data,
};

struct device_driver bcm2708_gpio_driver = {
	.name = "bcm2708_gpio",
	.driver_init = bcm2708_gpio_init,
	.driver_exit = bcm2708_gpio_exit,
	.api = &bcm2708_gpio_api,
};

int bcm2708_gpio_init(){

	int i;

	// Set gpio base address
	gpio = (volatile struct bcm2708_gpio*)BCM2708_GPIO_BASE;

	// Fill-in the Interrupt line data:
	// Event-generated interrupts
	bcm2708_irq_lines[0] = SHARED_GPIO_B3_IRQ;

	// GPIO-pin interrupts
	for(i=0;i<=BCM2708_GPIO_PINS-1;i++){
		bcm2708_irq_lines[1+i] = bcm2708_gpio_to_irq(i);
	}

	// Register GPIO IRQ chip to interrupt manager
	irq_register_chip(&bcm2708_gpio_irq_chip);	

	// Set all external pins output and clear them
	for(i=2;i<=27;i++){
		bcm2708_gpio_function_set(i,GPIO_OUTPUT);
		bcm2708_gpio_clear(i);
	}

	for(uint32_t i=2;i<=27;i++){
		bcm2708_gpio_enable_irq(bcm2708_gpio_to_irq(i),GPIO_RISING_DETECT|GPIO_FALLING_DETECT);
	}

	// Request an interrupt line for all the GPIO events (SHARED_GPIO_B3_IRQ)
	request_irq(bcm2708_irq_handler,SHARED_GPIO_B3_IRQ);

	// Register the device driver (api) to the device manager
	device_driver_register(&bcm2708_gpio_driver);

	gpiocore_setdrvdata(&bcm2708_gpio_data);

	return 0;
}

int bcm2708_gpio_exit(){
	int i;
	gpio = (volatile struct bcm2708_gpio*)BCM2708_GPIO_BASE;
	// Clear all pins
	for(i=0;i<=BCM2708_GPIO_PINS-1;i++){
		bcm2708_gpio_clear(i);
	}

	printk("%s: Calling irq_unregister_chip...\n",__func__);
	irq_unregister_chip(&bcm2708_gpio_irq_chip);

	release_irq("bcm2708_gpio",SHARED_GPIO_B3_IRQ);
	return 0;
}

//platform_init(bcm2708_gpio_init);
//platform_exit(bcm2708_gpio_exit);
