#ifndef INCLUDE_BCM2708_INTERRUPTS_H
#define INCLUDE_BCM2708_INTERRUPTS_H

#include <stdint.h>

// Interrupt controller base address
#define BCM2708_IRQCONTROLLER_BASE		0x2000B200

struct irqcontroller
{
	volatile uint32_t IRQ_basic_pending;	// Bitmask of pending ARM-specific IRQs
	volatile uint32_t IRQ_pending_1;	// Bitmask of pending shared IRQs 0-31
	volatile uint32_t IRQ_pending_2;	// Bitmask of pending shared IRQs 32-63
	volatile uint32_t FIQ_control;		// The FIQ register control which interrupt source can generate a FIQ to the ARM. Only a single interrupt can be selected.
	volatile uint32_t Enable_IRQs_1;	// Write 1 to the corresponding bit(s) to enable one or more shared IRQs in the range 0-31
	volatile uint32_t Enable_IRQs_2;	// Write 1 to the corresponding bit(s) to enable one or more shared IRQs in the range 32-63
	volatile uint32_t Enable_Basic_IRQs;	// Write 1 to the corresponding bit(s) to enable one or more ARM-specific (Basic) IRQs
	volatile uint32_t Disable_IRQs_1;	// Write 1 to the corresponding bit(s) to disable one or more shared IRQs in the range 0-31
	volatile uint32_t Disable_IRQs_2;	// Write 1 to the corresponding bit(s) to disable one or more shared IRQs in the range 32-63
	volatile uint32_t Disable_Basic_IRQs;	// Write 1 to the corresponding bit(s) to disable one or more ARM-specific (Basic) IRQs
};

// Bit Definitions for the ARM-specific (Basic) interrupts
#define BASIC_IRQS_BASE				(0)
#define BASIC_ARM_TIMER_IRQ			(BASIC_IRQS_BASE + 0)		/* Set to enable ARM Timer IRQ */
#define BASIC_ARM_MAILBOX_IRQ			(BASIC_IRQS_BASE + 1)		/* Set to enable ARM Mailbox IRQ */
#define BASIC_ARM_DOORBELL_0_IRQ		(BASIC_IRQS_BASE + 2)		/* Set to enable ARM Doorbell 0 IRQ */
#define BASIC_ARM_DOORBELL_1_IRQ		(BASIC_IRQS_BASE + 3)		/* Set to enable ARM Doorbell 1 IRQ */
#define BASIC_GPU_0_HALTED_IRQ			(BASIC_IRQS_BASE + 4)		/* Set to enable GPU 0 Halted IRQ */
#define BASIC_GPU_1_HALTED_IRQ			(BASIC_IRQS_BASE + 5)		/* Set to enable GPU 1 Halted IRQ */
#define BASIC_ACCESS_ERROR_1_IRQ		(BASIC_IRQS_BASE + 6)		/* Set to enable Access error type -1 IRQ */
#define BASIC_ACCESS_ERROR_0_IRQ		(BASIC_IRQS_BASE + 7)		/* Set to enable Access error type -0 IRQ */

#define NR_BASIC_INTERRUPTS			8

// Bit Definitions for shared interrupts 1 (Interrupt enable register 1)
#define SHARED_IRQS_1_BASE			(BASIC_IRQS_BASE + 32)
#define SHARED_SYSTEMTIMER_COMPARE_C0_IRQ	(SHARED_IRQS_1_BASE + 0)	/* Do not enable this IRQ; it’s already used by the GPU */
#define SHARED_SYSTEMTIMER_COMPARE_C1_IRQ	(SHARED_IRQS_1_BASE + 1)	/* BCM2835 System Timer Compare C1 Interrupt		*/
#define SHARED_SYSTEMTIMER_COMPARE_C2_IRQ	(SHARED_IRQS_1_BASE + 2)	/* Do not enable this IRQ; it’s already used by the GPU */
#define SHARED_SYSTEMTIMER_COMPARE_C3_IRQ	(SHARED_IRQS_1_BASE + 3)	/* BCM2835 System Timer Compare C3 Interrupt		*/
#define SHARED_USBCONTROLLER_IRQ		(SHARED_IRQS_1_BASE + 9)
#define SHARED_AUX_IRQ				(SHARED_IRQS_1_BASE + 29)

// Bit Definitions for shared interrupts 2 (Interrupt enable register 2)
#define SHARED_IRQS_2_BASE			(SHARED_IRQS_1_BASE + 32)
#define SHARED_I2C_SPI_SLV_IRQ			(SHARED_IRQS_2_BASE + 11)
#define SHARED_PWA0_IRQ				(SHARED_IRQS_2_BASE + 13)
#define SHARED_PWA1_IRQ				(SHARED_IRQS_2_BASE + 14)
#define SHARED_SMI_IRQ				(SHARED_IRQS_2_BASE + 16)
#define SHARED_GPIO_B0_IRQ			(SHARED_IRQS_2_BASE + 17)
#define SHARED_GPIO_B1_IRQ			(SHARED_IRQS_2_BASE + 18)
#define SHARED_GPIO_B2_IRQ			(SHARED_IRQS_2_BASE + 19)
#define SHARED_GPIO_B3_IRQ			(SHARED_IRQS_2_BASE + 20)
#define SHARED_I2C_IRQ				(SHARED_IRQS_2_BASE + 21)
#define SHARED_SPI_IRQ				(SHARED_IRQS_2_BASE + 22)
#define SHARED_PCM_IRQ				(SHARED_IRQS_2_BASE + 23)
#define SHARED_UART_IRQ				(SHARED_IRQS_2_BASE + 25)
#define SHARED_SD_HOST_CONTROLLER_IRQ		(SHARED_IRQS_2_BASE + 30)

#define GPIO_IRQ_BASE				(100)
#define NR_GPIO_INTERRUPTS			54+1				/* 54 GPIO pins + 1 interrupt enable (B3) registers */

#define UART_IRQ_BASE				GPIO_IRQ_BASE + NR_GPIO_INTERRUPTS
#define NR_UART_INTERRUPTS			8+1				/* 8 Separate IRQ sources + 1 ORred main interrupt */ 

#define UART_IRQ_CTSMIM				UART_IRQ_BASE + 0		/* UART CTS Modem Interrupt Mask 	*/
#define UART_IRQ_RXIM				UART_IRQ_BASE + 1		/* UART Receive Interrupt Mask		*/
#define UART_IRQ_TXIM				UART_IRQ_BASE + 2		/* UART Transfer Interrupt Mask 	*/
#define UART_IRQ_RTIM				UART_IRQ_BASE + 3		/* UART Receive Timeout Interrupt Mask	*/
#define UART_IRQ_FEIM				UART_IRQ_BASE + 4 		/* UART Framing Error Interrupt Mask 	*/
#define UART_IRQ_PEIM				UART_IRQ_BASE + 5		/* UART Parity Error Interrupt Mask 	*/
#define UART_IRQ_BEIM				UART_IRQ_BASE + 6		/* UART Break Error Interrupt Mask 	*/
#define UART_IRQ_OEIM				UART_IRQ_BASE + 7		/* UART Overrun Error Interrupt Mask 	*/


/*
// Bit Definitions for the ARM-specific (Basic) interrupts
#define BASIC_IRQ_BASE				(0 << 0)			// Base for Basic IRQ's
#define BASIC_ARM_TIMER_IRQ			(1 << 0)			// Set to enable ARM Timer IRQ 
#define BASIC_ARM_MAILBOX_IRQ			(1 << 1)			// Set to enable ARM Mailbox IRQ
#define BASIC_ARM_DOORBELL_0_IRQ		(1 << 2)			// Set to enable ARM Doorbell 0 IRQ 
#define BASIC_ARM_DOORBELL_1_IRQ		(1 << 3)			// Set to enable ARM Doorbell 1 IRQ 
#define BASIC_GPU_0_HALTED_IRQ			(1 << 4)			// Set to enable GPU 0 Halted IRQ 
#define BASIC_GPU_1_HALTED_IRQ			(1 << 5)			// Set to enable GPU 1 Halted IRQ 
#define BASIC_ACCESS_ERROR_1_IRQ		(1 << 6)			// Set to enable Access error type -1 IRQ 
#define BASIC_ACCESS_ERROR_0_IRQ		(1 << 7)			// Set to enable Access error type -0 IRQ 

// Bit Definitions for shared interrupts 1 (Interrupt enable register 1)
#define SHARED_SYSTEMTIMER_COMPARE_C0_IRQ	(1 << 0)			// Do not enable this IRQ; it’s already used by the GPU
#define SHARED_SYSTEMTIMER_COMPARE_C1_IRQ	(1 << 1)			// BCM2835 System Timer Compare C1 Interrupt	
#define SHARED_SYSTEMTIMER_COMPARE_C2_IRQ	(1 << 2)			// Do not enable this IRQ; it’s already used by the GPU 
#define SHARED_SYSTEMTIMER_COMPARE_C3_IRQ	(1 << 3)			// BCM2835 System Timer Compare C3 Interrupt		
#define SHARED_USBCONTROLLER_IRQ		(1 << 9)
#define SHARED_AUX_IRQ				(1 << 29)

// Bit Definitions for shared interrupts 2 (Interrupt enable register 2)
#define SHARED_I2C_SPI_SLV_IRQ			(1 << 11)
#define SHARED_PWA0_IRQ				(1 << 13)
#define SHARED_PWA1_IRQ				(1 << 14)
#define SHARED_SMI_IRQ				(1 << 16)
#define SHARED_GPIO_B0_IRQ			(1 << 17)
#define SHARED_GPIO_B1_IRQ			(1 << 18)
#define SHARED_GPIO_B2_IRQ			(1 << 19)
#define SHARED_GPIO_B3_IRQ			(1 << 20)
#define SHARED_I2C_IRQ				(1 << 21)
#define SHARED_SPI_IRQ				(1 << 22)
#define SHARED_PCM_IRQ				(1 << 23)
#define SHARED_UART_IRQ				(1 << 25)
#define SHARED_SD_HOST_CONTROLLER_IRQ		(1 << 30)
*/

int bcm2708_irq_init(void);

#endif
