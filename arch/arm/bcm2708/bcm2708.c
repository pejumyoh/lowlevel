#include "platform.h"
#include "bcm2708_fb.h"
#include "bcm2708_gpio.h"
#include "bcm2708_interrupts.h"
#include "bcm2708_systemtimer.h"
#include "bcm2708_dma.h"
#include "bcm2708_uart.h"
#include "bcm2708_gpio.h"
#include "device.h"
#include "interrupt.h"
#include "console.h"
#include "gpio.h"
#include "fbcon.h"
#include "uartcon.h"

struct device bcm2708_fb={
	.name			= "bcm2708_fb",
	.type			= FB_DEV,
	.driver			= NULL,
	.irq_info		= {
		.start	= -1,
		.end	= -1,
	},
};

struct device bcm2708_gpio={
	.name			= "bcm2708_gpio",
	.type			= GPIO_DEV,
	.driver			= NULL,
	.irq_info		= {
		.start	= SHARED_GPIO_B0_IRQ,
		.end	= SHARED_GPIO_B3_IRQ,
	},
};

struct device bcm2708_systemtimer ={
	.name			= "bcm2708_stimer",
	.type			= TIMER_DEV,
	.driver			= NULL,
	.irq_info		= {
		.start	= SHARED_SYSTEMTIMER_COMPARE_C0_IRQ,
		.end	= SHARED_SYSTEMTIMER_COMPARE_C3_IRQ,
	},
};

void platform_early_initialize()
{
	// Enable essential platform pheripherals before continuing 
	// the boot process
	device_register(&bcm2708_fb);
	device_register(&bcm2708_gpio);
	device_register(&bcm2708_systemtimer);

	// Init dma-controller
	bcm2708_dma_init();
	// initialize framebuffer device
	bcm2708_fb_init();
		
	fbcon_init();

	// register and load gpio driver
	bcm2708_gpio_init();


}

void platform_initialize()
{
	bcm2708_uart_init();

	uartcon_init();

	// initialize console driver
	console_init();

	// Enable platform-specific irq-handler
	bcm2708_irq_init();

	// enable systemtimer
	bcm2708_systemtimer_init();

//	bcm2708_stimer_enable_irq(33,0);
}
