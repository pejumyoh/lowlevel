#include "bcm2708_systemtimer.h"
#include "bcm2708_interrupts.h"
#include "lowlevel.h"
#include "interrupt.h"
#include "io.h"
#include "console.h"

uint32_t systemtimer_delay = 100000;

struct systemtimer *timer;

uint32_t bcm2708_systemtimer_getdelay();

void bcm2708_stimer_enable_irq(uint32_t irq, uint32_t mode)
{
	printk("%s: enabling timer interrupt.. \n",__func__);
	uint32_t timer_current = timer->timer_clo;
	timer_current+=systemtimer_delay;
	timer->timer_cs = BCM2708_SYSTEMTIMER_CS_MATCH1_CLEAR;
	timer->timer_c1 = timer_current;
}

void bcm2708_stimer_disable_irq(uint32_t irq, uint32_t mode)
{
	printk("%s: disabling timer interrupt.. \n",__func__);
}

void bcm2708_stimer_mask_irq(uint32_t irq)
{
	// masking (clearing) interrupt means we will re-arm the timer
	uint32_t timer_delay = bcm2708_systemtimer_getdelay();
	volatile uint32_t timer_current = timer->timer_clo;
	timer->timer_cs = BCM2708_SYSTEMTIMER_CS_MATCH1_CLEAR;
	timer_current+=timer_delay;
	timer->timer_c1 = timer_current;
}

void bcm2708_stimer_unmask_irq(uint32_t irq)
{
	printk("%s: unmasking timer interrupt.. \n",__func__);
}

static struct irq_chip bcm2708_stimer_irq_chip = {
        .name           = "bcm2708_stimer",
        .enable_irq     = bcm2708_stimer_enable_irq,              /* use for enabling specific interrupt  */
        .disable_irq    = bcm2708_stimer_disable_irq,             /* use for disabling specific interrupt */
        .mask_irq       = bcm2708_stimer_mask_irq,                /* use for masking specific interrupt   */
        .unmask_irq     = bcm2708_stimer_unmask_irq,              /* use for unmasking specific interrupt */
};

static void bcm2708_stimer_irq_handler(uint32_t irq)
{
	printk("%s: Timer tick!\n",__func__);
}

void bcm2708_systemtimer_init()
{
	timer = (struct systemtimer*)BCM2708_SYSTEMTIMER_BASE;

//	printk("%s: Calling irq_register_chip...\n",__func__);  
	irq_register_chip(&bcm2708_stimer_irq_chip);

	request_irq(bcm2708_stimer_irq_handler,SHARED_SYSTEMTIMER_COMPARE_C1_IRQ);
}


void systemtimer_init(uint32_t delay)
{
	timer = (struct systemtimer*)BCM2708_SYSTEMTIMER_BASE;

	struct irqcontroller *irqbase = (struct irqcontroller*)(BCM2708_IRQCONTROLLER_BASE);

	uint32_t timer_current = timer->timer_clo;
	timer_current+=delay;

	systemtimer_delay = delay;

	timer->timer_c3 = timer_current;
	timer->timer_cs = BCM2708_SYSTEMTIMER_CS_MATCH3_CLEAR;

	// enable system timer interrupt in the interrupt controller
	irqbase->Enable_IRQs_1 = SHARED_SYSTEMTIMER_COMPARE_C3_IRQ;

	// enable the interrupts now
//	enable_interrupts();
//	printk("\nInterrupts enabled!\n");
}


uint32_t bcm2708_systemtimer_getdelay()
{
	return systemtimer_delay;
}

uint32_t bcm2708_systemtimer_getcurrenttime()
{
	return timer->timer_clo;
}
