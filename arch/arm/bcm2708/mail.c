#include "bcm2708_platform.h"
#include <stdint.h>
#include "lowlevel.h"
#include "io.h"

uint32_t writemailbox(uint32_t data, uint8_t channel)
{
	uint32_t ready = 0;
	uint32_t status = 0;

	// Check that the data address has 4 lowest bits zero
	if(data & 0xF)
		return -1;

	if(channel > 15)
		return -1;

	// read the write status bit from the mailbox status register
	// and loop until mailbox is empty
	while(!ready) {
		status = read32(BCM2708_MAILBOX_STATUS_R);
		if(!(status & 0x80000000))
			ready = 1;
	}

	// add channel number to address' 4 lowest bit container
	data+=channel;
	write32(BCM2708_MAILBOX_WRITE_W,data);

	return 0;
}

uint32_t readmailbox(uint8_t channel)
{
	uint32_t ready = 0;
	uint32_t status = 0;
	uint32_t resp = 0;
	uint32_t chid = -1;
	uint32_t ret = 0;

	if(channel > 15)
		return -1;

	while(chid!=channel){
		// read the read status bit from the mailbox status register
		// and loop until mailbox is full
		while(!ready){
			status = read32(BCM2708_MAILBOX_STATUS_R);
			if( !(status & (1 << 30)) )
				ready = 1;
		}

		resp = read32(BCM2708_MAILBOX_READ_R);
		chid = (resp & 0xF);
	}

	ret = resp & 0xfffffff0;
	return ret;
}

