#ifndef INCLUDE_BCM2708_FB_H
#define INCLUDE_BCM2708_FB_H

#include "framebuffer.h"

struct bcm2708_fb_info
{
	uint32_t width;         /* #0 Physical Width                                                    */
	uint32_t height;        /* #4 Physical Height                                                   */
	uint32_t vwidth;        /* #8 Virtual Width                                                     */
	uint32_t vheight;       /* #12 Virtual Height                                                   */
	uint32_t pitch;         /* #16 Pitch (Number of bytes between each row of the frame buffer)     */
	uint32_t bitdepth;      /* #20 Bit Depth (bits per pixel)                                       */
	uint32_t xoff;          /* #24 X-offset (Offset in the x direction)                             */
	uint32_t yoff;          /* #28 Y-offset (Offset in the y direction)                             */
	uint32_t *fbptr;        /* #32 Pointer (The pointer to the actual frame buffer memory)          */
	uint32_t size;          /* #36 Framebuffer Size.                                                */
};

int bcm2708_fb_init(void);
int bcm2708_fb_exit(void);
uint32_t get_dmacb();
uint32_t get_dmacb2();
uint32_t get_fbptr();
void bcm2708_fb_drawpixel(uint32_t x, uint32_t y, uint16_t colour);

#endif
