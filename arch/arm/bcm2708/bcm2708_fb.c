/*
 *	BCM2708 Framebuffer driver
 *	(c) 2015 Petri Myöhänen
 */

#include <stddef.h>
#include "bcm2708_fb.h"
#include "framebuffer.h"
#include "buddy.h"
#include "io.h"
#include "mail.h"
#include "colordefs.h"
#include "lowlevel.h"
#include "device.h"
#include "framebuffer.h"
#include "bcm2708_dma.h"
#include "colordefs.h"
#include "console.h"
#include "initcalls.h"

extern void wait(uint32_t time);

#define BCM2708_FB_DRIVER_NAME	"bcm2708_fb"

uint32_t* zeroptr = NULL;
static struct fb_data			bcm2708_fbdata;
static struct bcm2708_fb_info 		*fb_info;
static struct device_driver		bcm2708_fb_driver;

struct bcm2708_dmacb*	dmacb = NULL;
struct bcm2708_dmacb*	dmacb2 = NULL;

void bcm2708_fb_drawpixel(uint32_t x, uint32_t y, uint16_t colour){
	uint32_t x_res = fb_info->width;

	// 16 bit Framebuffer at the moment
	write16((uint32_t)fb_info->fbptr + 2*((y*x_res)+x),colour);
}

void bcm2708_fb_scrollscreen_y(uint32_t ypixels)
{
	uint32_t xres   = fb_info->width;
	uint32_t yres   = fb_info->height;

	uint8_t bpp     = (fb_info->bitdepth/8);    /* bytes per pixel */
	
	char* fb_pointer = (char*)(fb_info->fbptr);

	// Set-up the DMA control block
	dmacb->ti       	= (BCM2708_DMA_TI_SRC_INC | 
				   BCM2708_DMA_TI_DEST_INC | 
				   BCM2708_DMA_TI_TDMODE | 
				   BCM2708_DMA_TI_DEST_WIDTH | 
				   BCM2708_DMA_TI_SRC_WIDTH);

	dmacb->source_ad	= (uint32_t)(fb_pointer+xres*bpp*ypixels); 
	dmacb->dest_ad		= (uint32_t)(fb_pointer);
	dmacb->txfr_len		= ((73<<16)|(xres*bpp*ypixels));	// TODO: fix hardcoded value 
	dmacb->stride		= 0;
	dmacb->nextconblk	= 0;
	dmacb->debug1		= 0;
	dmacb->debug2		= 0;
	dma_start_and_wait(dmacb);

	fb_pointer = (char*)(fb_info->fbptr);

	// Set-up the DMA control block
	dmacb2->ti       	= (BCM2708_DMA_TI_SRC_INC | 
				   BCM2708_DMA_TI_DEST_INC | 
				   BCM2708_DMA_TI_DEST_WIDTH | 
				   BCM2708_DMA_TI_SRC_WIDTH);
	dmacb2->source_ad	= ((uint32_t)(zeroptr)|0x40000000);
	dmacb2->dest_ad		= (uint32_t)(fb_pointer + xres*bpp*(yres-ypixels));
	dmacb2->txfr_len	= (1*xres*bpp*ypixels);
	dmacb2->stride		= 0;
	dmacb2->nextconblk	= 0;
	dmacb2->debug1		= 0;
	dmacb2->debug2		= 0;
	dma_start_and_wait(dmacb2);

//	fb_pointer = (char*)(fb_info->fbptr);
//	memset32((uint32_t*)(fb_pointer+1920*2*(1200-16)),(1*1920*2*16)/4,(Black|Black));
}

void bcm2708_fb_clear(uint32_t color)
{
	// clear/reset the whole screen with a given color
	memset32(fb_info->fbptr,(fb_info->size)>>2,(color<<16 | color));
}

void bcm2708_fb_drawline(uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint16_t color)
{
	// TODO
}

void bcm2708_fb_changebuffer(uint8_t buf_if)
{
	// TODO
}


/* This driver offers its services through the following API */

struct framebuffer_api bcm2708fb_api = {
	.drawpixel	= bcm2708_fb_drawpixel,
	.drawline	= bcm2708_fb_drawline,
	.scrollscreen_y	= bcm2708_fb_scrollscreen_y,
	.clear		= bcm2708_fb_clear,
	.changebuffer	= bcm2708_fb_changebuffer,
	.data		= &bcm2708_fbdata,
};

int bcm2708_fb_init(void){
	uint32_t status;
	uint32_t xres 	= 1920;
	uint32_t yres 	= 1200;
	uint32_t bitdepth = 16;

	fb_info = (struct bcm2708_fb_info*)kmalloc(sizeof(struct bcm2708_fb_info));

	fb_info->width		= xres;
	fb_info->height		= yres;
	fb_info->vwidth		= xres;
	fb_info->vheight	= yres;
	fb_info->pitch		= 0;
	fb_info->bitdepth	= bitdepth;
	fb_info->xoff		= 0;
	fb_info->yoff		= 0;
	fb_info->fbptr		= NULL;
	fb_info->size		= 0;

	/* Use memory mapped address to communicate with the videocore */
	char* memptr = (char*)(fb_info);
	if((status = writemailbox((uint32_t)(memptr+0x40000000),BCM2708_MAILBOX_CHANNEL_FB))!=0){
		// Failed to write to mailbox!
		return -1;
	}

	if(readmailbox(1)){
		// failed to read from mailbox!
		return -1;
	}

	if(fb_info->fbptr==NULL){
		return -1;
	}

	bcm2708_fbdata.width		= fb_info->width;
	bcm2708_fbdata.height		= fb_info->height;
	bcm2708_fbdata.vwidth		= fb_info->vwidth;
	bcm2708_fbdata.vheight		= fb_info->vheight;
	bcm2708_fbdata.pitch		= fb_info->pitch;
	bcm2708_fbdata.bitdepth		= fb_info->bitdepth;
	bcm2708_fbdata.xoff		= fb_info->xoff;
	bcm2708_fbdata.yoff		= fb_info->yoff;
	bcm2708_fbdata.size		= fb_info->size;
	bcm2708_fbdata.fb_id		= 0;
	bcm2708_fbdata.fb_devs		= 1;

	// Allocate memory DMA control block
	dmacb = (struct bcm2708_dmacb*) kmalloc(sizeof(struct bcm2708_dmacb));	
	dmacb2 = (struct bcm2708_dmacb*) kmalloc(sizeof(struct bcm2708_dmacb));	

	zeroptr = (uint32_t*) kmalloc(64*BLOCK0_SIZE);
	memset32(zeroptr,64*BLOCK0_SIZE,0);

	// Fill in the device information and operations (API)
	bcm2708_fb_driver.name		= BCM2708_FB_DRIVER_NAME;
	bcm2708_fb_driver.driver_init	= bcm2708_fb_init;
	bcm2708_fb_driver.driver_exit	= bcm2708_fb_exit;
	bcm2708_fb_driver.api		= (void*)&bcm2708fb_api;

	// Register the Framebuffer driver to the device manager
	device_driver_register(&bcm2708_fb_driver);

	return 0;
}

int bcm2708_fb_exit(void)
{
	if(fb_info){kfree(fb_info);}
	return 0;
}

/*
platform_init(bcm2708_fb_init);
platform_exit(bcm2708_fb_exit);
*/

uint32_t get_dmacb()
{
	return (uint32_t)(dmacb);
}

uint32_t get_dmacb2()
{
	return (uint32_t)(dmacb2);
}

uint32_t get_fbptr()
{
	return (uint32_t)(fb_info->fbptr);
}
