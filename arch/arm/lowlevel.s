.section .text

.globl  enable_interrupts
enable_interrupts:
        mrs     r0, cpsr
        bic     r0, r0, #0x80
        msr     cpsr_c, r0
        mov     pc, lr

.globl  disable_interrupts
disable_interrupts:
        mrs     r0, cpsr
        orr     r0, r0, #0x80
        msr     cpsr_c, r0
        mov     pc, lr

// Data memory barrier
.globl dmb
dmb:	
	.func	dmb
	mov	r0, #0
	mcr	p15, 0, r0, c7, c10, 5
	mov	pc, lr
	.endfunc

.globl	flush_cache
flush_cache:
	.func flush_cache

//	mov	r0, #0
//	mcr p15, 0, r0, c7, c5, 0	/* Invalidate instruction cache */
//	mcr p15, 0, r0, c7, c5, 6	/* Invalidate BTB */
//	mcr p15, 0, r0, c7, c10, 4	/* Drain write buffer */
//	mcr p15, 0, r0, c7, c5, 4	/* Prefetch flush */
//	mov pc, lr			/* Return */
	MCR   p15, 0, ip, c7, c5, 0      @ invalidate I cache
	MCR   p15, 0, ip, c7, c5, 6      @ invalidate BTB
	MCR   p15, 0, ip, c7, c10, 4     @ drain write buffer
	MCR   p15, 0, ip, c7, c5, 4      @ prefetch flush
	MOV   pc, lr
	.endfunc

.globl	wmb
wmb:
	mov r0, #0
	mcr p15, 0, r0, c7, c10, 4


.globl	clinv_dcache
clinv_dcache:
	.func clinv_dcache
	mov	r0, #0
	mcr	p15, 0, r0, c7, c14, 0
	.endfunc
