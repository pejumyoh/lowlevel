#ifndef INCLUDE_PL011_UART_H
#define INCLUDE_PL011_UART_H

#include <stdint.h>
/*
 *	For full description, see PrimeCell UART (PL011) Documentation:
 *	http://infocenter.arm.com/help/topic/com.arm.doc.ddi0183f/DDI0183.pdf 
 *	page 46 for register layout
 */

struct pl011_uart
{
	uint32_t        UARTDR;         /* 0x0-->0x3: Data register */
	uint32_t        UARTRSRECR;     /* 0x4-->0x7: Receive status register/error clear register */
	uint32_t        Reserved1[4];   /* 0x8-->0x17: Reserved */
	uint32_t        UARTFR;         /* 0x18-->0x1b: Flag register */
	uint32_t        Reserved3;      /* 0x1c-->0x1f: Reserved */
	uint32_t        UARTILPR;       /* 0x20-->0x23: IrDA low-power counter register */
	uint32_t        UARTIBRD;       /* 0x24-->0x27: Integer baud rate register */
	uint32_t        UARTFBRD;       /* 0x28-->0x2b: Fractional baud rate register */
	uint32_t        UARTLCR_H;      /* 0x2c-->0x2f: Line control register */
	uint32_t        UARTCR;         /* 0x30-->0x33: Control register */
	uint32_t        UARTIFLS;       /* 0x34-->0x37: Interrupt FIFO level select register */
	uint32_t        UARTIMSC;       /* 0x38-->0x3b: Interrupt mask set/clear register */
	uint32_t        UARTRIS;        /* 0x3c-->0x3f: Raw interrupt status register */
	uint32_t        UARTMIS;        /* 0x40-->0x43: Masked interrupt status register */
	uint32_t        UARTICR;        /* 0x44-->0x47: Interrupt clear register */
	uint32_t        UARTDMACR;      /* 0x48-->0x4b: DMA control register */
//      uint32_t        Reserved4[13];  /* Test Data Registers */       
};



/* Bit definitions for UARTDR */

#define PL011_UARTDR_DATA	0b11111111	/* Receive (read) data character. Transmit (write) data character */
#define PL011_UARTDR_FE		(1<<8)		/* Framing Error */
#define PL011_UARTDR_PE		(1<<9)		/* Parity Error */
#define PL011_UARTDR_BE		(1<<10)		/* Break Error */
#define PL011_UARTDR_OE		(1<<11)		/* Overrun Error */



/* Bit definitions for UARTRSRECR */

#define PL011_UARTRSRECR_FE	(1<<0)		/* Framing Error */
#define PL011_UARTRSRECR_PE	(1<<1)		/* Parity Error */
#define PL011_UARTRSRECR_BE	(1<<2)		/* Break Error */
#define PL011_UARTRSRECR_OE	(1<<3)		/* Overrun Error */
#define PL011_UARTRSRECR_CLEAR	0b11111111	/* Clear above errors */



/* Bit definitions for UARTFR */

#define PL011_UARTFR_CTS	(1<<0)		/* Clear To Send */
#define PL011_UARTFR_DSR	(1<<1)		/* Data Set Ready */
#define PL011_UARTFR_DCD	(1<<2)		/* Data Carrier Detect */
#define PL011_UARTFR_BUSY	(1<<3)		/* UART Busy */
#define PL011_UARTFR_RXFE	(1<<4)		/* Receive FIFO empty */
#define PL011_UARTFR_TXFF	(1<<5)		/* Transmit FIFO full */
#define PL011_UARTFR_RXFF	(1<<6)		/* Receive FIFO full */
#define PL011_UARTFR_TXFE	(1<<7)		/* Transmit FIFO empty */
#define PL011_UARTFR_RI		(1<<8)		/* RING Indicator */



/* Bit definitions for UARTLCR_H */

#define PL011_UARTLCR_H_BRK		(1<<0)		/* Send Break */
#define PL011_UARTLCR_H_PEN		(1<<1)		/* Parity Enable */
#define PL011_UARTLCR_H_EPS		(1<<2)		/* Even Parity Select */
#define PL011_UARTLCR_H_STP2		(1<<3)		/* Two stop bits select */
#define PL011_UARTLCR_H_FEN		(1<<4)		/* Enable FIFOs */
#define PL011_UARTLCR_H_WLEN_5B		(0b00<<5)	/* 5 Bit Word length. These bits indicate the number of data bits transmitted or received in a frame */
#define PL011_UARTLCR_H_WLEN_6B		(0b01<<5) 	/* 6 Bit Word length. These bits indicate the number of data bits transmitted or received in a frame */
#define PL011_UARTLCR_H_WLEN_7B		(0b10<<5)	/* 7 Bit Word length. These bits indicate the number of data bits transmitted or received in a frame */
#define PL011_UARTLCR_H_WLEN_8B		(0b11<<5)	/* 8 Bit Word length. These bits indicate the number of data bits transmitted or received in a frame */
#define PL011_UARTLCR_H_SPS		(1<<7)		/* Stick parity select. See Table below */

/*	-----------------------------------------------------------
	PEN 	EPS 	SPS 	Parity bit (transmitted or checked)
	-----------------------------------------------------------
	0 	x 	x 	Not transmitted or checked
	1	1	0	Even parity
	1	0	0	Odd parity
	1	0	1	1
	1	1	1	0
	----------------------------------------------------------- */




/* Bit definitions for UARTCR */

/*
	To enable transmission, both TXE, bit 8, and UARTEN, bit 0, must be set. 
	Similarly, to enable reception, RXE, bit 9, and UARTEN, bit 0, must be set.


	Program the control registers as follows:
		1. Disable the UART.
		2. Wait for the end of transmission or reception of the current character.
		3. Flush the transmit FIFO by disabling bit 4 (FEN) in the line control register (UARTCLR_H).
		4. Reprogram the control register (UARTCR).
		5. Enable the UART.
*/

#define PL011_UARTCR_UARTEN		(1<<0)		/* UART enable */
#define PL011_UARTCR_SIREN		(1<<1)		/* SIR enable. If this bit is set to 1, the IrDA SIR ENDEC is enabled */
#define PL011_UARTCR_SIRLP		(1<<2)		/* IrDA SIR low power mode. This bit selects the IrDA encoding mode. */
#define PL011_UARTCR_LBE		(1<<7)		/* Loopback enable */
#define PL011_UARTCR_TXE		(1<<8)		/* Transmit enable */
#define PL011_UARTCR_RXE		(1<<9)		/* Receive enable */
#define PL011_UARTCR_DTR		(1<<10)		/* Data transmit ready */
#define PL011_UARTCR_RTS		(1<<11)		/* Request to send */
#define PL011_UARTCR_OUT1		(1<<12)		/* This bit is the complement of the UART Out1 (nUARTOut1) modem status output */
#define PL011_UARTCR_OUT2		(1<<13)		/* This bit is the complement of the UART Out2 (nUARTOut2) modem status output */
#define PL011_UARTCR_RTSEN		(1<<14)		/* RTS hardware flow control enable */
#define PL011_UARTCR_CTSEN		(1<<15)		/* CTS hardware flow control enable */



/* Bit definitions for UARTIFLS */

#define PL011_UARTIFLS_TXIFLSEL18	(0b000<<0)		/* Transmit FIFO becomes 1/8 full */
#define PL011_UARTIFLS_TXIFLSEL14	(0b001<<0)		/* Transmit FIFO becomes 1/4 full */
#define PL011_UARTIFLS_TXIFLSEL12	(0b010<<0)		/* Transmit FIFO becomes 1/2 full */
#define PL011_UARTIFLS_TXIFLSEL34	(0b011<<0)		/* Transmit FIFO becomes 3/4 full */
#define PL011_UARTIFLS_TXIFLSEL78	(0b100<<0)		/* Transmit FIFO becomes 7/8 full */
#define PL011_UARTIFLS_RXIFLSEL18	(0b000<<3)		/* Receive FIFO becomes 1/8 full */
#define PL011_UARTIFLS_RXIFLSEL14	(0b001<<3)		/* Receive FIFO becomes 1/4 full */
#define PL011_UARTIFLS_RXIFLSEL12	(0b010<<3)		/* Receive FIFO becomes 1/2 full */
#define PL011_UARTIFLS_RXIFLSEL34	(0b011<<3)		/* Receive FIFO becomes 3/4 full */
#define PL011_UARTIFLS_RXIFLSEL78	(0b100<<3)		/* Receive FIFO becomes 7/8 full */



/* Bit definitions for UARTIMSC - Writing 0 to corresponding bit clears the mask */

#define PL011_UARTIMSC_RIMIM		(1<<0)			/* nUARTRI modem interrupt mask */
#define PL011_UARTIMSC_CTSMIM		(1<<1)			/* nUARTCTS modem interrupt mask */
#define PL011_UARTIMSC_DCDMIM		(1<<2)			/* nUARTDCD modem interrupt mask */
#define PL011_UARTIMSC_DSRMIM		(1<<3)			/* nUARTDSR modem interrupt mask */
#define PL011_UARTIMSC_RXIM		(1<<4)			/* Receive interrupt mask */
#define PL011_UARTIMSC_TXIM		(1<<5)			/* Transmit interrupt mask */
#define PL011_UARTIMSC_RTIM		(1<<6)			/* Receive timeout interrupt mask */
#define PL011_UARTIMSC_FEIM		(1<<7)			/* Framing error interrupt mask */
#define PL011_UARTIMSC_PEIM		(1<<8)			/* Parity error interrupt mask */
#define PL011_UARTIMSC_BEIM		(1<<9)			/* Break error interrupt mask */
#define PL011_UARTIMSC_OEIM		(1<<10)			/* Overrun error interrupt mask */


/* Bit definitions for UARTRIS */

#define PL011_UARTRIS_RIRMIS		(1<<0)			/* nUARTRI modem interrupt status */
#define PL011_UARTRIS_CTSRMIS		(1<<1)			/* nUARTCTS modem interrupt status */
#define PL011_UARTRIS_DCDRMIS		(1<<2)			/* nUARTDCD modem interrupt status */
#define PL011_UARTRIS_DSRRMIS		(1<<3)			/* nUARTDSR modem interrupt status */
#define PL011_UARTRIS_RXRIS		(1<<4)			/* Receive interrupt status */
#define PL011_UARTRIS_TXRIS		(1<<5)			/* Transmit interrupt status */
#define PL011_UARTRIS_RTRIS		(1<<6)			/* Receive timeout interrupt status */
#define PL011_UARTRIS_FERIS		(1<<7)			/* Framing error interrupt status */
#define PL011_UARTRIS_PERIS		(1<<8)			/* Parity error interrupt status */
#define PL011_UARTRIS_BERIS		(1<<9)			/* Break error interrupt status */
#define PL011_UARTRIS_OERIS		(1<<10)			/* Overrun error interrupt status */


/* Bit definitions for UARTMIS */

#define PL011_UARTMIS_RIMMIS		(1<<0)			/* nUARTRI modem masked interrupt status */
#define PL011_UARTMIS_CTSMMIS		(1<<1)			/* nUARTCTS modem masked interrupt status */
#define PL011_UARTMIS_DCDMMIS		(1<<2)			/* nUARTDCD modem masked interrupt status */
#define PL011_UARTMIS_DSRMMIS		(1<<3)			/* nUARTDSR modem masked interrupt status */
#define PL011_UARTMIS_RXMIS		(1<<4)			/* Receive masked interrupt status */
#define PL011_UARTMIS_TXMIS		(1<<5)			/* Transmit masked interrupt status */
#define PL011_UARTMIS_RTMIS		(1<<6)			/* Receive timeout masked interrupt status */
#define PL011_UARTMIS_FEMIS		(1<<7)			/* Framing error masked interrupt status */
#define PL011_UARTMIS_PEMIS		(1<<8)			/* Parity error masked interrupt status */
#define PL011_UARTMIS_BEMIS		(1<<9)			/* Break error masked interrupt status */
#define PL011_UARTMIS_OEMIS		(1<<10)			/* Overrun error masked interrupt status */


/* Bit definitions for UARTICR */

#define PL011_UARTICR_RIMIC		(1<<0)			/* nUARTRI modem interrupt clear */
#define PL011_UARTICR_CTSMIC		(1<<1)			/* nUARTCTS modem interrupt clear */
#define PL011_UARTICR_DCDMIC		(1<<2)			/* nUARTDCD modem interrupt clear */
#define PL011_UARTICR_DSRMIC		(1<<3)			/* nUARTDSR modem interrupt clear */
#define PL011_UARTICR_RXIC		(1<<4)			/* Receive interrupt clear */
#define PL011_UARTICR_TXIC		(1<<5)			/* Transmit interrupt clear */
#define PL011_UARTICR_RTIC		(1<<6)			/* Receive timeout interrupt clear */
#define PL011_UARTICR_FEIC		(1<<7)			/* Framing error interrupt clear */
#define PL011_UARTICR_PEIC		(1<<8)			/* Parity error interrupt clear */
#define PL011_UARTICR_BEIC		(1<<9)			/* Break error interrupt clear */
#define PL011_UARTICR_OEIC		(1<<10)			/* Overrun error interrupt clear */


/* Bit definitions for UARTDMACR */

#define PL011_UARTDMACR_RXDMAE		(1<<0)			/* Receive DMA enable */
#define PL011_UARTDMACR_TXDMAE		(1<<1)			/* Transmit DMA enable */
#define PL011_UARTDMACR_DMAONERR	(1<<2)			/* DMA on error. If this bit is set to 1, the DMA receive request outputs, 
								   UARTRXDMASREQ or UARTRXDMABREQ, are disabled when the UART error interrupt 
								   is asserted */



#endif
