#include "atags.h"

static struct atags tags;

void inittags(unsigned long *atag_addr)
{
	struct atag_header *header = (struct atag_header*)atag_addr;

	// Calculate header size in words
	// unsigned long hsize = divide(sizeof(struct atag_header),sizeof(unsigned long));
	unsigned long hsize = sizeof(struct atag_header)/sizeof(unsigned long);

	unsigned long *ptr	= atag_addr;
	uint32_t size		= header->size;			// tag size in words including header
	uint32_t tag		= header->tag;			// header value
	uint8_t	 id		= header->tag & 0b1111;		// tag index masked out from tag value

	while(id!=0){
		switch(id){
			case ATAG_CORE:		{ tags.core = (struct atag_core*)		(ptr+hsize); break;}
			case ATAG_MEM:		{ tags.mem = (struct atag_mem*)			(ptr+hsize); break;}
			case ATAG_VIDEOTEXT:	{ tags.videotext = (struct atag_videotext*)	(ptr+hsize); break;}
			case ATAG_RAMDISK:	{ tags.ramdisk = (struct atag_ramdisk*)		(ptr+hsize); break;}
			case ATAG_INITRD2:	{ tags.initrd2 = (struct atag_initrd2*)		(ptr+hsize); break;}
			case ATAG_SERIALNR:	{ tags.serialnr = (struct atag_serialnr*)	(ptr+hsize); break;}
			case ATAG_REVISION:	{ tags.revision = (struct atag_revision*)	(ptr+hsize); break;}
			case ATAG_VIDEOLFB:	{ tags.videolfb = (struct atag_videolfb*)	(ptr+hsize); break;}
			case ATAG_CMDLINE:	{ tags.cmdline = (struct atag_cmdline*)		(ptr+hsize); break;}
		}

		ptr+=size;

		// jump to next header
		header	= (struct atag_header*)ptr;
		size	= header->size;
		tag	= header->tag;
		id	= tag & 0b1111;
	}
}

unsigned long atags_get_avail_mem()
{
	return tags.mem->size;
}

char* atags_get_cmdline()
{
	return (char*)(tags.cmdline);
}
