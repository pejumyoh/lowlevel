#include "versatilepb_uart.h"
#include "io.h"
#include "txtdev.h"
#include "device.h"

volatile struct pl011_uart* versatilepb_uart0 = (volatile struct pl011_uart*)VERSATILEPB_UART_BASE;

struct device versatile_uart_dev;
struct device_driver versatile_uart_driver;


void uart_putstring(char* buf, uint16_t flags){

	while(*buf != '\0'){
		versatilepb_uart0->UARTDR = (unsigned int)(*buf);
		buf++;
	}
}

void uart_reset(void){
	// Nothing to do
}

void uart_rewind(void){
	// Nothong to do
}

static struct txt_api versatile_uart_api = {
        .txtdev_putstring       = uart_putstring,
        .txtdev_reset           = uart_reset,
        .txtdev_rewind          = uart_rewind,
};

int versatilepb_uart_exit(){

	return 0;
}

int versatilepb_uart_init(){
	
	// TODO: Do real initialization

	// Disable the uart first
//	write32(UARTCR,0x00000000);


	// Fill in the device info
	versatile_uart_dev.name = "versatile_uart";
	versatile_uart_dev.type = TXT_DEV,
	versatile_uart_dev.driver = &versatile_uart_driver,
	versatile_uart_dev.irq_info.start = -1,
	versatile_uart_dev.irq_info.end = -1,
	versatile_uart_dev.data = NULL,

	// Fill in the device driver info
	versatile_uart_driver.name = "versatile_uart";
	versatile_uart_driver.dev = &versatile_uart_dev;
	versatile_uart_driver.driver_init = versatilepb_uart_init;
	versatile_uart_driver.driver_exit = versatilepb_uart_exit;
	versatile_uart_driver.api = &versatile_uart_api;

	device_register(&versatile_uart_dev);
	device_driver_register(&versatile_uart_driver);

	return 0;
}
