#include "platform.h"
#include "device.h"
#include "interrupt.h"
#include "console.h"
#include "gpio.h"
#include "versatilepb_uart.h"

struct device versatilepb_uart={
	.name			= "versatilepb_uart",
	.type			= UART_DEV,
	.driver			= NULL,
	.irq_info		= {
		.start	= -1,
		.end	= -1,
	},
};

void platform_early_initialize()
{
	// Enable essential platform pheripherals before continuing 
	// the boot process
//	device_register(&versatilepb_uart);
	versatilepb_uart_init();
}

void platform_initialize()
{
	// Enable platform-specific irq-handler

	// Enable rest of the platform pheripherals 
	//bcm2708_stimer_enable_irq(33);

//	bcm2708_irq_init();
}
