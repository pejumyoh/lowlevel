#ifndef INCLUDE_VERSATILEPB_UART_H
#define INCLUDE_VERSATILEPB_UART_H

#include "../pl011_uart.h"

#define VERSATILEPB_UART_BASE	0x101f1000

int versatilepb_uart_init();
void uart_putstring(char* buf, uint16_t flags);

#endif
