.section ".text.boot"
.global _start
_start:
	ldr	pc, reset
	ldr	pc, undefined_instruction
	ldr	pc, software_interrupt
	ldr	pc, prefetch_abort
	ldr	pc, data_abort
	ldr	pc, not_used
	ldr	pc, irq
	ldr	pc, fiq

reset:			.word reset_entry 
undefined_instruction:	.word undefined_entry
software_interrupt:	.word swi_entry
prefetch_abort:		.word pfabort_entry
data_abort:		.word data_abort_entry
not_used:		.word not_used_entry
irq:			.word irq_entry
fiq:			.word fiq_entry

reset_entry:

	// Initialize the stack pointer in SVC mode
	ldr	r4, =__kernel_begin
	mov	sp, r4

	// Copy interrupt vector table (using relative addressing)
	// to the beginning of the physical memory (0x00000000).
	// Use r4 and r5 to protect r0,r1 and r2.
	
	mov	r5, #0x0000

	ldmia	r4!, {r6-r9}
	stmia	r5!, {r6-r9}
	ldmia	r4!, {r6-r9}
	stmia	r5!, {r6-r9}
	ldmia	r4!, {r6-r9}
	stmia	r5!, {r6-r9}
	ldmia	r4!, {r6-r9}
	stmia	r5!, {r6-r9}	

	// Clear out bss.
	ldr	r4, =__bss_start
	ldr	r9, =__bss_end
	mov	r5, #0
	mov	r6, #0
	mov	r7, #0
	mov	r8, #0
	b       bss_2
bss_1:
	// store multiple at r4.
	stmia	r4!, {r5-r8}
 
	// If we are still below bss_end, loop.
bss_2:
	cmp	r4, r9
	blo	bss_1

	// Initialize stack pointer for IRQ mode
	// ;@ (PSR_IRQ_MODE|PSR_FIQ_DIS|PSR_IRQ_DIS)
	msr	cpsr_c, #0xD2
	mov	sp, #0x4000

	// Go back to supervisor mode
	// ;@ (PSR_SVC_MODE|PSR_FIQ_DIS|PSR_IRQ_DIS)
	msr	cpsr_c, #0xD3

	// Initialize taglist (atags)
	push	{r0-r2}
	mov	r0, r2
	bl	inittags
	pop	{r0-r2}

	// Branch into c-kernel
	b	kern_main

	// In case we escaped from kern_main
	b 	hang

undefined_entry:
	b	hang

swi_entry:
	b	swi_handler 

pfabort_entry:
	b	hang

data_abort_entry:
	b	hang
 
not_used_entry:
	b	hang
 
irq_entry:
	// msr	cpsr_c, #0xD3	// enter Supervisor mode and disable IRQ and FIQ interrupts
	push 	{r0-r12,lr}	// store register contents 
	bl	irq_handler	// handle the irq
	pop 	{r0-r12,lr}	// return register contents
	subs	pc, lr, #4	// continue the work before interrupt happened

fiq_entry:
	b	fiq_handler
hang:
	b	hang

