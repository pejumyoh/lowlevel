#include <stdint.h>
#include "algo.h"

uint32_t divide(uint32_t divident, uint32_t divisor)
{
	uint32_t mask = (1 << 31);
	int i = 31;
	int lz_count=0;
	uint32_t result = 0;
	uint32_t remainder;
	uint32_t current;

	// count leading zeroes in divisors binary representation
	while(i>=0){
		if(divisor&mask) break;
		mask>>=1; lz_count++; i--;
	}
	// shift divisor lz_count bits to left
	current=(divisor<<lz_count);
	remainder = divident;

	while(1){
		if(remainder>=current){
			result+=1;
			remainder-=current;
		}
		if(lz_count==0) break;

		lz_count-=1;
		current>>=1;
		result<<=1;
	}

	return result;
}

int powof(int val, int p) {
        int i;
        int res = val; 
	if(p==0)
	  return 1;

	for(i=1;i<p;i++) {
                res*=val;
	}

	return res;
}
