#include "io.h"

void write32(uint32_t reg, uint32_t val){
	*((volatile uint32_t*)reg) = val;
}

void write16(uint32_t reg, uint16_t val){
	*((volatile uint16_t*)reg) = val;
}
void write8(uint32_t reg, uint8_t val){
	*((volatile uint8_t*)reg) = val;
}
uint32_t read32(uint32_t reg){
	return *((volatile uint32_t*)reg);
}
uint16_t read16(uint32_t reg){
	return *((volatile uint16_t*)reg);
}
uint8_t read8(uint32_t reg){
	return *((volatile uint8_t*)reg);
}

void memcpy32(uint32_t *from, uint32_t words, uint32_t *to){
	size_t cnt = 0;
	while(words!=cnt++) *(volatile uint32_t*)to++ = *(volatile uint32_t*)from++;
}

void memcpy16(uint16_t *from, uint32_t hwords, uint16_t *to){
	size_t cnt = 0;
	while(hwords!=cnt++) *(volatile uint16_t*)to++ = *(volatile uint16_t*)from++;
}

void memcpy8(uint8_t *from, uint32_t bytes, uint8_t *to){
	size_t cnt = 0;
	while(bytes!=cnt++) *(volatile uint8_t*)to++ = *(volatile uint8_t*)from++;
}

void memset32(uint32_t *ptr, uint32_t count, uint32_t val){
	uint32_t i;
	for(i=0;i<count;i++)
	{
		*((volatile uint32_t*)ptr++) = val;
	}
}

void memset16(uint16_t *ptr, uint32_t count, uint16_t val){
	uint32_t i;
	volatile uint16_t *pointer = ptr;
	for(i=0;i<count;i++)
	{
		*((volatile uint16_t*)pointer++) = val;
	}
}

void memset8(uint8_t *ptr, uint32_t count, uint8_t val){
	uint32_t i;
	volatile uint8_t *pointer = ptr;
	for(i=0;i<count;i++)
	{
		*((volatile uint8_t*)pointer++) = val;
	}
}
