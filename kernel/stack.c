#include "console.h"

#define STACK_CHK_GUARD 0xe2dee396

void *__stack_chk_guard = (void*)STACK_CHK_GUARD;

void __stack_chk_fail(void){
	printk("%s: STACK SMASHING DETECTED!!!!",__func__);
	while(1);
}
