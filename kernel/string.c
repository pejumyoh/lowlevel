#include <stdint.h>
#include "string.h"
#include "algo.h"

size_t strlen(char *buf) {
	size_t len = 0;

	for(len=0; *buf!='\0';buf++) len++;
	return len;
}

int strcmp(char *buf1, char* buf2) {

	// ret = 0 means that the strings differ
	// ret = 1 means that the strings are equal
	int ret = 1;
	uint32_t i=0;

	if(strlen(buf1)!=strlen(buf2)){
		ret=0;
	}
	else{
		while(i<=strlen(buf1)){
			if(buf1[i]!=buf2[i]){
				ret=0;
				break;
			}
			i++;
		}
	}

	return ret;
}

void hextostring(char *buf, uint32_t hex) {
	int i;
	uint32_t factor[8];
	uint32_t hexval = hex;

	// Character assignment from numbers 
	// 0...9,10,11,12,13,14,15 to characters 
	// '0'...'9','a','b','c','d','e','f'
	const char *chbuf = "0123456789abcdef";

	buf[0] = '0'; buf[1] = 'x';
        // convert only 32 bit values
	for(i=7;i>=0;i--) {
		factor[i]=divide(hexval,powof(16,i));
		hexval-=(factor[i]*powof(16,i));
		buf[9-i]=chbuf[factor[i]]; 
	}
}
