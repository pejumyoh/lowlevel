#include <stdint.h>
#include "bcm2708_platform.h"
#include "kernel.h"
#include "string.h"
#include "console.h"
#include "algo.h"
#include "buddy.h"
#include "io.h"
#include "atags.h"
#include "lowlevel.h"
#include "armtimer.h"
#include "../arch/arm/bcm2708/bcm2708_gpio.h"
#include "../arch/arm/bcm2708/bcm2708_fb.h"
#include "device.h"
#include "platform.h"
#include "initcalls.h"
#include "fbcon.h"
#include "../drivers/ds18b20.h"
#include "uartcon.h"
#include "gpio.h"
#include "../arch/arm/bcm2708/bcm2708_uart.h"

extern void wait(uint32_t time);

void kern_main(unsigned int zero, unsigned int mach_type, unsigned long* atag_addr)
{
	// Initialize memory allocator
	// PROTECT ALSO KERNEL!!! TAKE SIZE FROM LINKER SCRIPT!!!!
	buddy_init(0,ALLOCATOR_BASE,atags_get_avail_mem());

	// early-initialize the platform.
	platform_early_initialize();
	gpiocore_init();
	platform_initialize();

	// initialize the built-in drivers
	do_initcalls();
	enable_interrupts();

	printk("Register values:\nr0 = %d\nr1 = %d (machine type)\nr2 = %x (atag address)\n",zero,mach_type,(unsigned long)atag_addr);
	unsigned long memo = atags_get_avail_mem();
	printk("available memory = %d bytes (%d kB, %d MB)\n",memo, TOKB(memo), TOMB(memo));

	printk("Terve... %d\n",-10000);

	while(1){
		long int temp = ds18b20_get_temperature();
		printk("temp = %d x 10^(-4) C\r",temp);
		wait(500000);
	}


/*	// enable arm timer interrupt
	struct irqcontroller *irqbase = (struct irqcontroller*)(BCM_IRQCONTROLLER_BASE);
	irqbase->Enable_Basic_IRQs = BASIC_ARM_TIMER_IRQ;

	init_armtimer_irq(10000);
	enable_interrupts();
*/
	
/*	unsigned char ch;
	while(1){

		ch = bcm2708_uart_rx_byte();
		printk("%c\n",ch);
	//	bcm2708_uart_tx_byte(ch);
	}
*/
	while(1){

	};
}
