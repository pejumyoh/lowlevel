#include "device.h"
#include "framebuffer.h"
#include "console.h"

#define MAX_DEVICES	32
#define MAX_DRIVERS	32

static void* device_list[MAX_DEVICES];
static void* driver_list[MAX_DRIVERS] __attribute__((used));

static uint16_t dev_count = 0;

static void* device_api(device_type type){
	int i;
	struct device* dev = NULL;
	void* dev_api = NULL;

	for(i=0;i<=MAX_DEVICES-1;i++){
		if(device_list[i]!=NULL){
			dev = (struct device*)device_list[i];
			if(dev->type == type){
				if(dev->driver !=NULL){
					if(dev->driver->api !=NULL){
						dev_api = dev->driver->api;
					//	printk("%s: Found device %s!\n",__func__, dev->name); 
						break;
					}
					else{
						dev_api = NULL;
					}
				}
				else{
					dev_api = NULL;
				}
			}	
		}
	}

	return (void*)dev_api;
}

int device_register(struct device* dev){

	int i;

	for(i=0;i<=MAX_DEVICES-1;i++){
		if(((void*)dev)==device_list[i]){
			// device already registered
			return -1;
		}
	}

	device_list[dev_count] = (void*)dev;
	dev_count++;

	printk("%s: Registered device %s\n\n",__func__,dev->name);

	return 0;
}

void* request_device(device_type type){
	void *dev = NULL;
	dev = device_api(type);
	return dev;
}

void device_driver_register(struct device_driver* driver){
	int i;
	char* name = driver->name;
	struct device* dev = NULL;

	// Find the corresponding device
	for(i=0;i<=MAX_DEVICES-1;i++){
		if(device_list[i]!=NULL){
			dev = (struct device*)device_list[i];
			if(dev->name == name){
				// device found, bind the driver to this device
				// and give the parent device pointer to the driver
				dev->driver = driver;
				dev->driver->dev = dev;
				break;
			}
		}
	}

	printk("%s: Driver %s bound to device!\n",__func__,name);
}

/*
 *	Request all the devices of type "type". Returns the number of devices found
 */
int request_devices(device_type type, int (*request_callback)(void* dev, char* name)){

	int i;
	unsigned int devcounter = 0;
	struct device* dev = NULL;
	void* dev_api = NULL;

	for(i=0;i<=MAX_DEVICES-1;i++){
		if(device_list[i]!=NULL){
			dev = (struct device*)device_list[i];
			if(dev->type == type){
				if(dev->driver!=NULL){
					if(dev->driver->api!=NULL){
						dev_api = dev->driver->api;
						request_callback((void*)dev_api,dev->driver->name);
						devcounter++;
					}
				}
			}
		}
	}

	return devcounter;
}

struct irq_resource* request_irqresource(char* devname){

	int i;
	struct device* dev = NULL;
	struct irq_resource* res= NULL;

	for(i=0;i<=MAX_DEVICES-1;i++){
		if(device_list[i]!=NULL){
			dev = (struct device*)device_list[i];
			if(dev->name == devname){
				res = &dev->irq_info;
				break;
			}
		}
	}

	return res;
}
