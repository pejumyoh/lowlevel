#include <stdarg.h>
#include "io.h"
#include "console.h"
#include "string.h"
#include "algo.h"
#include "buddy.h"
#include "txtdev.h"
#include "fbcon.h"
#include "colordefs.h"
#include "console.h"
#include "../arch/arm/bcm2708/bcm2708_uart.h"
#include "../arch/arm/bcm2708/bcm2708_interrupts.h"

static struct txt_api* text_output_devices[MAX_TEXT_OUTPUT_DEVS] = {NULL};
static unsigned int nr_text_output_devices = 0;

/*
 *	Handle requests to register multiple txtdev-interfaces
 */
int txtdev_register(void* textdev, char* name){

	int i;
	struct txt_api* tdev = (struct txt_api*)textdev;

	if(tdev!=NULL){

		// Check first if this device is already registered
		for(i=0;i<=MAX_TEXT_OUTPUT_DEVS-1;i++){
			if(text_output_devices[i]==tdev){
				printk("%s: Text output device already registered...\n",__func__);
				return -1;
			}
		}

		// If the text api was not registered yet, then add it into the list
		// of text output devices
		for(i=0;i<=MAX_TEXT_OUTPUT_DEVS-1;i++){
			if(text_output_devices[i]==NULL){
				text_output_devices[i]=tdev;
				printk("%s: Registered text output api [%s]\n",__func__,name);
				break;
			}
		}
	}
	return 0;
}

int txtdev_unregister(struct txt_api* tdev)
{
	uint8_t i=0;
	for(i=0;i<=MAX_TEXT_OUTPUT_DEVS-1;i++){
		if(text_output_devices[i]==tdev){
			text_output_devices[i] = NULL;
		}
	}
	return 0;
}

static void kputs(char *buf, uint16_t color){

	uint8_t i=0;

	// Call putstring -routine for each text output device.
	if(buf){
		i=0;
		while(text_output_devices[i]!=NULL && i<=MAX_TEXT_OUTPUT_DEVS-1){
			text_output_devices[i]->txtdev_putstring(buf,color);
			i++;
		}
	}
}

void console_reset(){
	int i;
	for(i=0;i<=MAX_TEXT_OUTPUT_DEVS-1;i++){
		if(text_output_devices[i]!=NULL){
			text_output_devices[i]->txtdev_reset();
		}
	}
}

static void kputchar(int c){
	char buf[2];
	buf[0] = (char)c;
	buf[1] = '\0';

	kputs(buf,PRINTK_COLOR_CHAR);
}

static void printhex(unsigned long hex){
	int i;
	uint32_t factor[8];
	uint32_t hexval = hex;
	char buf[11];

	// Character assignment from numbers 
	// 0...9,10,11,12,13,14,15 to characters 
	// '0'...'9','a','b','c','d','e','f'
	const char *chbuf = "0123456789abcdef";

	buf[0] = '0'; buf[1] = 'x';
	// convert only 32 bit values
	for(i=7;i>=0;i--) {
		factor[i]=divide(hexval,powof(16,i));
		hexval-=(factor[i]*powof(16,i));
		buf[9-i]=chbuf[factor[i]];
	}
	buf[10] = '\0';
	kputs(buf,PRINTK_COLOR_HEX);
}

static void printdec(int val){
	int i;
	unsigned long factor[10];
	char buf[12];
	char *ptrr = buf;
	int pos = 0;
	unsigned long pow_val;
	int sign=0;

	// pick the sign of the val
	if(val<0){sign=1; val=val*(-1);}

	// Character assignment from numbers 
	// 0...9 to characters '0'...'9'
	const char *chbuf = "0123456789";
	// convert only 32 bit values
	for(i=0;i<=9;i++){
		pow_val = powof(10,9-i);
		factor[i]=divide(val,pow_val);
		buf[pos]=chbuf[factor[i]];
		val-=(factor[i]*pow_val);
		pos++;
	}
	buf[pos] = '\0';
	while((*ptrr)=='0' && *(ptrr+1)!='\0') ptrr+=1;
	if(sign){ ptrr-=1; *ptrr='-';}
	kputs(ptrr,PRINTK_COLOR_DEC);
}

void printk(char *buf, ...){
	int argn = 0;
	va_list args;
	int arg_val;
	unsigned int counter=0;

	// since we do not know how long the char buffer is,
	// lets determine its size now
	unsigned long size = strlen(buf);

	if(size>PRINTK_MAX_LENGHT){
		// handle this
	}

	char *str = kmalloc(PRINTK_MAX_LENGHT);
	char *strptr = str;
	char *stringbuf = NULL;

	// parse the char buffer first 
	char *ptr = buf;
	while(*ptr!='\0'){
		if(*ptr=='%'){
			ptr++;
			switch(*ptr){
				case 'x': argn++; break;
				case 'd': argn++; break;
				case 'c': argn++; break;
				case 's': argn++; break;
			}
		}
		ptr++;
	}

	if(argn>PRINTK_MAX_ARGS){
		// Handle this case
	}
	else{
		// Now we know the number of arguments to be printed
		va_start(args,buf);

		ptr = buf;
		counter=0;

		// Initialize the print string with the end symbol
		strptr[0] = '\0';

		while(*ptr!='\0' && counter<PRINTK_MAX_LENGHT-1){
			if(*ptr=='%'){
				ptr++;
				kputs(strptr,PRINTK_COLOR_DEFAULT);
				switch(*ptr){
					case 'x':{
						arg_val = va_arg(args,unsigned long);
						printhex(arg_val);
						counter = 0;
						strptr[0] = '\0';
						break;
					}
					case 'd':{
						arg_val = va_arg(args,int);
						printdec(arg_val);
						counter = 0;
						strptr[0] = '\0';
						break;
					}
					case 'c':{
						arg_val = va_arg(args,unsigned long);
						kputchar(arg_val);
						counter = 0;
						strptr[0] = '\0';
						break;
					}
					case 's':{
						stringbuf = va_arg(args,char*);
						kputs(stringbuf,PRINTK_COLOR_STRING);
						counter = 0;
						strptr[0] = '\0';
						break;
					}
				}
			}
			else{
				strptr[counter]=*ptr;
				strptr[counter+1]='\0';
				counter++;
			}
			ptr++;
		}
		kputs(strptr,PRINTK_COLOR_DEFAULT);
	}
	va_end(args);
	kfree(str);
}


void nullerror_print(const char* func, void *ptr)
{
	if(ptr==NULL){
		printk("%s: ");
		kputs("NULL POINTER DETECTED!\n",PRINTK_COLOR_ERROR);

		// Handle this!
		while(1);
	}
}

static void console_irq_handler(uint32_t irq){

//	printk("READING FROM UART NOW... irq = %d\n",irq);
	char ch = bcm2708_uart_rx_byte();
//	printk("UART READ...\n");
	char buf[3];

	buf[0]=ch;
	if(ch=='\r'){ buf[1]='\n'; buf[2]='\0';}
	else buf[1]='\0';
	kputs(buf,PRINTK_COLOR_DEFAULT);
}

int console_init(){

	uint8_t i;
	for(i=0;i<=MAX_TEXT_OUTPUT_DEVS-1;i++) text_output_devices[i]=NULL;

	// First, ask for a framebuffer from the device manager
	nr_text_output_devices = request_devices(TXT_DEV,txtdev_register);
	if(nr_text_output_devices == 0){
		// No Text renderers available!
		return -1;
	}

	request_irq(console_irq_handler,UART_IRQ_RXIM);

	printk("%s: console initialized with %d text renderers!\n",__func__,nr_text_output_devices);

	return 0;
}

