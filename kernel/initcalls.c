#include "initcalls.h"
#include <stddef.h>

extern platform_initcall_t __platform_initcall_start;
extern platform_initcall_t __platform_initcall_end;

extern core_initcall_t __core_initcall_start;
extern core_initcall_t __core_initcall_end;

extern module_initcall_t __module_initcall_start;
extern module_initcall_t __module_initcall_end;

void do_initcalls()
{
	/* 	Initcall sequence:

		1) PLATFORM COMPONENTS
		2) CORE COMPONENTS
		3) MODULE COMPONENTS
	*/

	platform_initcall_t	*platform_initf;
	core_initcall_t		*core_initf;
	module_initcall_t	*module_initf;

	platform_initf = &__platform_initcall_start;
	while(platform_initf < &__platform_initcall_end){
		if(platform_initf!=NULL){
			(*platform_initf)();
			++platform_initf;
		}
	}

	core_initf = &__core_initcall_start;
	while(core_initf < &__core_initcall_end){
		if(core_initf!=NULL){
			(*core_initf)();
			++core_initf;
		}
	}

	module_initf = &__module_initcall_start;
	while(module_initf < &__module_initcall_end){
		if(module_initf!=NULL){
			(*module_initf)();
			++module_initf;
		}
	}
}

