#include <stdint.h>
#include <stddef.h>
#include "buddy.h"
// #include "console.h"
#include "string.h"
#include "algo.h"

unsigned long MEMTABLE_SIZE		= 0;			// Size of freelist and node table
unsigned long NODESIZE			= 0;			// Size of single node in the node table

static unsigned short int BUDDY_INIT_DONE = 0;

unsigned int max_order		= 0;
unsigned int max_nodes		= 0;

unsigned long **freelist;
unsigned long *node_base;
unsigned long *memorybase;
unsigned long *VcBASE;
unsigned long *freemem_base;


unsigned long getmaxorder(){return max_order;}

unsigned long freemem(void)
{
	struct mnode *nodeptr = NULL;
	unsigned long mem = 0;
	unsigned int order = 0;
	unsigned int counter = 1;
	int i;

	if(BUDDY_INIT_DONE){
		for(i=0;i<=max_order;i++){
			nodeptr = (struct mnode*)freelist[i];
			if(nodeptr)
			{
				counter = 1;
				order = (nodeptr->header>>2);
				while(nodeptr->next!=NULL){
					counter++;
					nodeptr=nodeptr->next;
				}
				mem+=counter*BLOCK0_SIZE*powof(2,order);
			}
		}
	}
	else{
		// debug_print("freemem: buddy memory manager not initialized!\n");
		mem = 0;
	}

	return mem;
}

uint8_t buddy_init(unsigned long *memory_base, unsigned long offset, uint32_t memsize)
{
	int i;
	max_order = 0;
	unsigned int bsize = BLOCK0_SIZE;
	uint8_t header;

	memorybase = memory_base;

	offset = divide(offset,sizeof(unsigned long*));

	freelist = (unsigned long**)memory_base + offset;

	// debug_print("buddy_init: Freelist address =  ");
	// printhex((unsigned long)freelist);


	// determine the max_order for the whole device memory
	for(;;){
		if(2*bsize>DEV_MEM)
			break;
		else{
			bsize*=2; max_order++;
		}
		// printf("order = %d and size = %ld kB (%ld MB)\n",max_order,TOKB(bsize),TOMB(bsize));
	}

	// printf("MAX ORDER\t = %ld\n",max_order);
	// debug_print("buddy_init: max_order =  ");
	// printhex(max_order);

	// determine max number of nodes
	max_nodes = divide(DEV_MEM,BLOCK0_SIZE);
	// debug_print("buddy_init: max_nodes =  ");
	// printhex(max_nodes);

	// total size of the freelist + memory management tables in bytes
	MEMTABLE_SIZE = (max_order+1)*sizeof(unsigned long*) + max_nodes*sizeof(struct mnode);
	// debug_print("buddy_init: MEMTABLE_SIZE =  ");
	// printhex(MEMTABLE_SIZE);

	// assign NULL pointer to each freelist element
	for(i=0;i<=max_order;i++){
		freelist[i] = NULL;
		// printf("freelist[%d] = %x\n",i,(unsigned long)freelist[i]);
	}

	// printf("sizeof freelist = %d\n", sizeof(freelist));
	unsigned short int mnodesize_b = sizeof(struct mnode);
	NODESIZE = mnodesize_b;

	// debug_print("buddy_init: NODESIZE =  ");
	// printhex(NODESIZE);

	// Set node_base address just after the freelist
	node_base  = (unsigned long*)freelist + (max_order+1);
	// printf("NODE_BASE\t\t = 0x%x\n",node_base);

	// debug_print("buddy_init: node_base address =  ");
	// printhex((unsigned long)node_base);

	// set freemem_base just after the node tables
	freemem_base = (unsigned long*)((char*)memory_base);

	// debug_print("buddy_init: freemem_base address =  ");
	// printhex((unsigned long)freemem_base);

	// Setup main node into node table
	struct mnode *main_node = (struct mnode *)(node_base);
	header = (max_order << 2) | M_FLAG_FREE;
	main_node->header = header;
	main_node->next = NULL;

	// Add main node into freelist
	freelist[max_order] = (unsigned long*)main_node;

	// ---------------------------------------TODO: use memsize to initialize -----------------------------------------------------
	// ----- MEMORY TABLE INITIALIZATION
	BUDDY_INIT_DONE = 0;
	// Allocate space for videocore memory
	char *ptrr1 = buddy_malloc(256*1024*1024);	// First 256MB
	char *ptrr2 = buddy_malloc(128*1024*1024);	// Second 128MB
	char *ptrVC = buddy_malloc(128*1024*1024);	// Final 128MB = VIDEOCORE MEM

	VcBASE = (unsigned long*)ptrVC;

	// ----- FREE THE HELPER POINTERS...AND 
	// WHAT REMAINS RESERVED IN THE NODE TABLE
	// IS EXACTLY THE VIDEOCORE MEMORY AREA
	// THE RELEASE ORDER OF THESE POINTERS SHOULD NOT BE ALTERED!
	buddy_free(ptrr1);

	// Protect the Interrupt vector table, ATAGS and stack + nodetable itself
	// 132kB (IVT+ATAGS+STACK+KERNEL) + 0x100048 bytes (max_order*sizeof(unsigned long) + 1MB) (FREELIST+NODETABLE)
	// 0...32 (132kB, 33 loops) + 0...256 (little bit over 1MB, 257 loops) = 290 loops of 4kB allocations
	for(i=0;i<=289;i++){
		ptrr1 = buddy_malloc(BLOCK0_SIZE);
	}

	// Finally release the middle section 128MB for system use
	buddy_free(ptrr2);
	BUDDY_INIT_DONE = 1;
	// --------------------------------------------------------------------------------------------

	// debug_print("Buddy memory handler initialized!\n");
	// debug_print("Free memory: ");
	return 0;
}


static void split_blocks(unsigned int start_order, unsigned int target_order){

//	unsigned int ord;
//	unsigned long factor;
	unsigned long ch1offset;
	
	unsigned char header;

	unsigned long *start_node = NULL;
	struct mnode *parent	= NULL;
	struct mnode *next	= NULL;
	struct mnode *child1	= NULL;
	struct mnode *child2	= NULL;

	// printf("split_blocks: start_order = %ld and target_order = %ld\n",start_order,target_order);

	if(start_order<target_order){
		// printf("split_blocks: TARGET BLOCK LARGER THAN STARTING POINT BLOCK!\n");
	}
	else if(start_order == target_order){
		// printf("split_blocks: NO NEED TO SPLIT EXISTING BLOCK!\n");
	}
	else{
		// First, pick a first node (parent) from the freelist to start
		// with (which should be divided)
		// At this stage, this node is guaranteed to exist at freelist[start_order].
		start_node = freelist[start_order];		
		if(start_node==NULL){
			// debug_print("split_blocks: NULL POINTER!\n");
		//	exit(0);
		}

		// printf("split_blocks: start_node address = 0x%x\n",(unsigned long)start_node);

		// Next, check if this parent has node->next pointer set.
		// If it is set, then store it into "next" before overwriting 
		// parent data with the child node information
		parent = (struct mnode*)start_node;
		if(parent->next!=NULL){
			// printf("split_blocks: parent has next-pointer! Storing it...\n");
			next = parent->next;
		}		

		// ------------------- INITIALIZE AND INSERT THE SIBLINGS INTO THE FREE LIST ------------------------

		// The first child position in the node table is determined 
		// by the order of the sibling node.
		// OPTIMIZE!!!
		ch1offset = divide(NODESIZE*powof(2,start_order-1),sizeof(unsigned long*));
		// printf("split_blocks: ch1offset = %ld unsigned longs, where sizeof(unsigned long) = %ld = \n",ch1offset,sizeof(unsigned long));
		child1 = (struct mnode*)(start_node+ch1offset);

		// Update the child1 header content
		header = ((start_order-1) << 2) | M_FLAG_FREE;
		child1->header = header;
		child1->next = NULL;

		// before pushing child1 into freelist, check if the freelist already contains a 
		// node of order start_order-1
		if(freelist[start_order-1]!=NULL){
			// printf("split_blocks: child1: lower order in freelist already has entry!\n");
			// printf("split_blocks: child1: entry address = 0x%x\n",(unsigned long)freelist[start_order-1]);
			// if yes, then set the existing node address to child1->next
			child1->next = (struct mnode*)freelist[start_order-1];
		}
		
		// then put child1 into freelist (one order lower than the parent)
		freelist[start_order-1] = (unsigned long*)child1;
		// printf("split_blocks: FREELIST UPDATED, added child1: freelist[%ld] = 0x%x\n",start_order-1,(unsigned long)freelist[start_order-1]);


		// the second sibling should be placed to the parent position in the node table
		child2 = (struct mnode*)start_node;
		// printf("split_blocks: child2 address = 0x%x\n",(unsigned long)(child2));
		header = ((start_order-1) << 2) | M_FLAG_FREE;
		child2->header = header;
		child2->next = NULL;

		// before pushing into freelist, check if the freelist already contains a 
		// node of order start_order-1
		if(freelist[start_order-1]!=NULL){
			// printf("split_blocks: child2: lower order in freelist already has entry!\n");
			// printf("split_blocks: child2: entry address = 0x%x\n",(unsigned long)freelist[start_order-1]);
			// if yes, then set the existing node address to child2->next
			child2->next = (struct mnode*)freelist[start_order-1];
		}

		// then put child2 into freelist (one order lower than the parent)
		freelist[start_order-1] = (unsigned long*)child2;
		// printf("split_blocks: FREELIST UPDATED, added child2: freelist[%ld] = 0x%x\n",start_order-1,(unsigned long)freelist[start_order-1]);

		// Check if the split parent had node->next
		if(next){
			// if yes, then put the parents "next" into freelist
			freelist[start_order] = (unsigned long*)next;
		}
		else{
			// Otherwise, remove the parent from the freelist
			freelist[start_order] = NULL;
		}
		// printf("\n");

		// NOW THE PARENT NODE IS SPLIT INTO TWO LOWER ORDER NODES
		// AND THE LOWER ORDER NODES ARE MARKED AS FREE AND ADDED TO 
		// THE FREELIST. ALSO, PARENT WAS REMOVED FROM THE FREELIST

		// If we were supposed to do multiple splittings, then start all over
		if(start_order-1>target_order)
			split_blocks(start_order-1,target_order);
		else{
			// printf("split_blocks: FINISHED SPLITTING!!!!!\n");
		}
	}
}

/*
 *	Find / Construct a free node of order 'order'
 */

static char* buddy_find_free_node(int order){
	int i;

	unsigned long *ptr	= NULL;
	char *memptr		= NULL;
	struct mnode *temp	= NULL;

	unsigned long offset;

	if(order>max_order){
		// printf("buddy_find_free_node: CANNOT ALLOCATE, NOT ENOUGH SPACE! returning NULL\n");
		return NULL;
	}

	// Check if there is already an entry in the freelist
	if(freelist[order]){

		// we found immediately a free block
		ptr = (unsigned long*)freelist[order];

		// TRANSLATION OF NODE ADDRESS INTO FREE MEMORY ADDRESS:
		// calculate the physical address corresponding to this block
		// by calculating how many bnodes fit between the node address
		// given by ptr and node_base

		offset = divide((unsigned long)(ptr-node_base)*sizeof(unsigned long),NODESIZE);

		// printf("buddy_find_free_node: FOUND NODE RIGHT AWAY: offset = %ld nodes from the node_base\n",offset);
		// printf("buddy_find_free_node: node address = 0x%x\n",(unsigned long)freelist[order]);
		// printf("buddy_find_free_node: node address difference = %ld\n",(unsigned long)(ptr-node_base)*sizeof(unsigned long));
		// printf("buddy_find_free_node: NODESIZE = %ld\n",NODESIZE);

		// this is the pointer to the physical free memory which
		// will be available for the memory requester
	//	memptr = (char*)(offset*BLOCK0_SIZE);
	//	memptr = (char*)memorybase+MEMTABLE_SIZE+offset*BLOCK0_SIZE;
		memptr = (char*)memorybase+offset*BLOCK0_SIZE;
		
		// set the node status to RESERVED
		// printf("NODE ORDER = %ld and MAX_ORDER = %ld\n",order,max_order);
		temp = (struct mnode *)ptr;
		temp->header = (order<<2|M_FLAG_RESERVED);

		// check also if this node is linked to another free node of same size
		struct mnode *nextnode = (struct mnode*)(temp->next);
		if(nextnode!=NULL)
		{
			// printf("buddy_find_free_node: NEXT NODE FOUND!\n");
			// printf("buddy_find_free_node: next node address = 0x%x\n",(unsigned long)nextnode);
			// if yes, then replace it to the freelist
			freelist[order] = (unsigned long*)nextnode;

			// then put the next pointer of requested node to NULL because it will be reserved
			temp->next = NULL;
		}
		else freelist[order] = NULL;
	}
	else if(order<max_order){
		// printf("buddy_find_free_node: Need to split...\n");
		// No suitable blocks available at the moment.
		// We need to split larger block to smaller ones so
		// loop until we find a free block which is larger than the requested one
		for(i=order+1;i<=max_order;i++){
			// printf("buddy_find_free_node: i = %ld\n",i);
			// Check for free blocks of order i
			if(freelist[i]){
				// printf("buddy_find_free_node: found a free node of order %ld! Will split it now..\n",i);
				// we found a block to split into target size given by variable order
				// so split a block (with size given by i) down to size given by order 
				split_blocks(i,order);

				// after splitting, lets just find it by calling our function recursively
				memptr = buddy_find_free_node(order);
				break;
			}
		}

		if(memptr==NULL){
			// printf("buddy_find_free_node: Could not find suitable memory block! returning NULL\n");
		}
	}
	else{
		// printf("buddy_find_free_node: NOT ENOUGH MEMORY! returning NULL\n");
		memptr = NULL;
	}
	return memptr;
}

char* buddy_malloc(size_t size)
{
//	int i;
	unsigned int order = 0;
	unsigned long bsize = BLOCK0_SIZE;
	char *ptr = NULL;

	// determine the block order and size to be allocated
	for(;;){
		if(bsize>=size) break;
		bsize*=2;
		order++;}
	// printf("buddy_malloc: requested %ld bytes (%ld kB, %ld MB), got %ld bytes (%ld kB, %ld MB), order = %d\n",size,TOKB(size),TOMB(size),bsize,TOKB(bsize),TOMB(bsize),order);

	ptr = buddy_find_free_node(order);

	return (char*)ptr;
}


static void buddy_remove_node_freelist(struct mnode* nodeptr)
{
//	int status = 0;
	unsigned char header		= nodeptr->header;
	unsigned int  order		= header >> 2;

	// start from the beginning of the freelist
	struct mnode 	*iterator 	= (struct mnode*)freelist[order];
	struct mnode 	*prev 		= NULL;

	// Check that freelist does not have null pointer
	if(freelist[order]==NULL)
	{
		// printf("buddy_remove_node_freelist: NULL POINTER DETECTED!!! Exiting..\n");
	}

	// check if it is immediately at the freelist
	if(iterator==nodeptr){
		// printf("buddy_remove_node_freelist: FOUND THE BUDDY FROM THE FREELIST IMMEDIATELY!\n");
		freelist[order] = (unsigned long*)(iterator->next);
	}
	else{
		// printf("buddy_remove_node_freelist: seeking through linkedlist...\n");
		// otherwise seek through the linked list for buddy
		prev = iterator;
		iterator = iterator->next;

		// loop until it is found
		while(iterator!=NULL){
			if(iterator==nodeptr){
				// found the node what we were looking for
				// printf("buddy_remove_node_freelist: FOUND THE BUDDY FROM THE LINKEDLIST!\n");	
				// now remove the iterator(nodeptr) from the linked list by linking 
				// the previous node to the iterators next node
				
				prev->next = iterator->next;
				break;
			}
			else{
				prev = iterator;
				iterator = iterator->next;
				// printf("buddy_remove_node_freelist: seeking through linkedlist...next\n");
			}
		}

		if(iterator==NULL){
			// debug_print("buddy_remove_node_freelist: NODE WAS MARKED AS FREE BUT NOT FOUND IN THE LINKED LIST!!! EXITING...\n");
			// exit(0);
		}
		else
		{
			// printf("buddy_remove_node_freelist: node found and removed from the linkedlist.\n");	
		}
	}
}


static unsigned int buddy_combine(unsigned long offset, struct mnode* nodeptr)
{
	unsigned char header		= nodeptr->header;
	unsigned int  order		= header >> 2;

	struct mnode* buddy		= NULL;
	unsigned char buddy_header	= 0;
	unsigned int  buddy_order	= 0;
	unsigned long buddy_offset	= 0;

	struct mnode* target_node	= NULL;
	unsigned int target_order	= 0;
	unsigned long target_offset	= 0;

	unsigned int k 		= 0;
	unsigned int status	= 0;

//	unsigned long pow_2_order = powof(2,order);
	
	// Check that the order is not too high
	if(order>max_order)
	{
		// printf("buddy_combine: CANNOT COMBINE NODES OF THIS SIZE! order = %ld and max_order = %ld\n",order,max_order);
		return 0;
	}

	// printf("buddy_combine: NODE ORDER = %ld\n",order);
	// ------------------ FINDING BUDDY -------------------
	// order N node positions (offset) in the node table are given by: 
	// offset = k*2^N, k=0,1,2,3...
	// calculate now the factor k = offset/(2^order)
	k = divide(offset,powof(2,order));

	// check if k is even or odd. This determines whether the 
	// buddy of this node is located to the 
	// left(odd,k=1,3,5,...) or to the right(even, k=0,2,4,...) 
	// of the current node
	if(k%2){
		// remainder nonzero, odd case, so the buddy is on the left, 
		// 2^order nodes away from the current node
		// printf("buddy_combine: ODD k(%ld): Buddy must be on the left!!\n",k);
		buddy_offset = offset - powof(2,order);
		buddy = (struct mnode*)((char*)nodeptr-NODESIZE*powof(2,order));
	}
	else{
		// remainder zero, even case, so the buddy is on the right, 
		// 2^order nodes away from the current node
		// printf("buddy_combine: EVEN k(%ld): Buddy must be on the right!!\n",k);
		buddy_offset = offset + powof(2,order);
		buddy = (struct mnode*)((char*)nodeptr+NODESIZE*powof(2,order));

	}
	// printf("buddy_combine: offset = %ld, buddy offset = %ld and buddy address = 0x%x\n",offset,buddy_offset,(unsigned long)buddy);

	// ------------ BUDDY IS NOW FOUND! -------------

	// Dig the status information of the buddy
	buddy_header	= buddy->header;
	buddy_order	= buddy_header >> 2;

	if(buddy_header&M_FLAG_RESERVED || buddy_order!=order || order==max_order){

		// the buddy is reserved, cannot combine with it.
		// printf("buddy_combine: BUDDY IS RESERVED OR MAX_ORDER (order=%ld) REACHED, CANNOT COMBINE\n",order);

		// So lets just add this node to freelist
		// Update the next pointer

		nodeptr->next = (struct mnode*)freelist[order];
		// and add the node to freelist of target_order
		freelist[order] = (unsigned long*)nodeptr;

		// Set status flag to 0 to indicate that we reached end in 
		// the combining process
		status = 0;
	}
	else{
		// Buddy was also free!
		// NOTE! Because buddy was free, it should be in the freelist chain of free nodes!
		// will check that later...
		// printf("buddy_combine: BUDDY IS ALSO FREE, LETS COMBINE!\n");

		// Check that the orders match...
		if(buddy_order != order){
			// debug_print("buddy_combine: BUDDY HAS DIFFERENT ORDER!!!!\n");
			// exit(0);
		}

		// LETS COMBINE THE NODES NOW.
		// First, set target order one larger than current order
		target_order = order+1;

		// Then, find which node will be the "main node" for the combined node
		if(k%2){
			// printf("buddy_combine: SETTING TARGET_NODE AS BUDDY ON THE LEFT...\n");		
			// buddy is on the left and it will now be the "target node".
			target_node = buddy;
			target_offset = buddy_offset;
		}
		else{
			// printf("buddy_combine: SETTING TARGET NODE AS THE CURRENT NODEPTR...\n");
			// buddy is on the right and nodeptr is the "main node".
			target_node = nodeptr;
			target_offset = offset;
		}

		// then, remove the buddy from the freelist or from the ->next chain
		// of free nodes of the same order
		buddy_remove_node_freelist(buddy);

		// remove also the node headers and next pointers
		buddy->header = 0;
		nodeptr->header = 0;
		buddy->next = NULL;
		nodeptr->next = NULL;

		// now the buddy is removed from the linked list of free nodes
		// and its identity (header & next-pointer) is cleared.
		// Next, install a new header for the main node.
		target_node->header = ((target_order<<2)|M_FLAG_FREE);

		// if the target order is the max_order, then terminate combining 
		// process by setting the target_node in the freelist[max_order]
		if(target_order==max_order){
			// printf("buddy_combine: TARGET IS MAX ORDER! Finishing...\n");
			target_node->next = NULL;
			freelist[target_order] = (unsigned long*)target_node;	
		}
		else{
			// Otherwise continue the process by trying to combine this recently 
			// created fresh node with its buddy, so lets call combining 
			// function recursively with these parameters:
			while((status=buddy_combine(target_offset,target_node))!=0);
		}
	}

	return status;
}

void buddy_free(void *memptr)
{
	struct mnode* nodeptr	= NULL;
//	struct mnode* next	= NULL;

	unsigned char header 	= 0;
	unsigned int  order 	= 0;

	unsigned long offset	= 0;
	unsigned long *freeptr	= (unsigned long*)memptr;

	if(memptr==NULL && BUDDY_INIT_DONE){
		// debug_print("buddy_free: ERROR: NULL POINTER!\n");
		// exit(0);
	}

	// TRANSLATION OF MEMORY ADDRESS INTO NODE POSITION IN NODE TABLE:
	offset	= divide((unsigned long)(freeptr-freemem_base)*sizeof(unsigned long),BLOCK0_SIZE);
	// printf("buddy_free: memptr = 0x%x\n",(unsigned long)freeptr);
	// printf("buddy_free: freemem_base = 0x%x\n",(unsigned long)freemem_base);
	// printf("buddy_free: offset = %ld nodes from the node_base!\n",offset);
	
	// this is the node address in the node table
	nodeptr	= (struct mnode*)((char*)node_base+offset*NODESIZE);
	header	= nodeptr->header;
	order	= header >> 2;

	// Check the header of this node and check that the 
	// block we are trying to free is RESERVED
	if(header&M_FLAG_RESERVED){
		// node is reserved, so we can free it!
		// printf("buddy_free: NODE IS RESERVED SO WE CAN FREE IT!\n");
		// set the node flag first to M_FLAG_FREE 
		nodeptr->header = ((order<<2)|M_FLAG_FREE);

		// also make sure that the next pointer is NULL
		nodeptr->next = NULL;
		
		// ...and try to combine with its buddy if the order is not the max_order
		if(order<max_order){
			buddy_combine(offset,nodeptr);
		}
		else{
			nodeptr->next = NULL;
			freelist[max_order] = (unsigned long*)nodeptr;
		}
	}
	else{
		// debug_print("buddy_free: NODE IS ALREADY FREE!\n");
		// exit(0);
	}
}
