#include <stdarg.h>
#include "io.h"
#include "fbcon.h"
#include "console.h"
#include "string.h"
#include "algo.h"
#include "buddy.h"
#include "device.h"
#include "txtdev.h"
#include "initcalls.h"
#include "uart.h"

struct device uartcon_dev;
struct device_driver uartcon_driver;

struct uart_api *uart_dev = NULL;

int uartcon_init();
int uartcon_exit();

void uartcon_puts(char *buf, uint16_t unused){

	// push the characters to uart dev
	while(*buf!='\0'){
		if(*buf=='\n') uart_dev->uart_tx_byte('\r');

		uart_dev->uart_tx_byte(*buf);
		buf++;
	}
}

static void uartcon_reset(){

	// TODO:
	// How to reset uart console?
	printk("%s: UART console resetted!\n",__func__);
}

/* uartcon driver implements txt_api functionality */
static struct txt_api uartcon_api = {
	.txtdev_putstring	= uartcon_puts,
	.txtdev_reset		= uartcon_reset,
};

int uartcon_init(){

	// First, ask for a uart device api from the device manager
	uart_dev = (struct uart_api*)request_device(UART_DEV);

	if(uart_dev==NULL){
		// No UART devices available!
		printk("%s: NO UART DEVICES AVAILABLE!\n",__func__);
		return -1;
	}

	// Fill in the device info
	uartcon_dev.name = "uartcon";
	uartcon_dev.type = TXT_DEV,
	uartcon_dev.driver = &uartcon_driver,
	uartcon_dev.irq_info.start = -1,
	uartcon_dev.irq_info.end = -1,
	uartcon_dev.data = NULL,

	// Fill in the device driver info
	uartcon_driver.name = "uartcon";
	uartcon_driver.dev = &uartcon_dev;
	uartcon_driver.driver_init = uartcon_init;
	uartcon_driver.driver_exit = uartcon_exit;
	uartcon_driver.api = &uartcon_api;

	device_register(&uartcon_dev);
	device_driver_register(&uartcon_driver);

	printk("%s: UARTCon driver initialized!\n",__func__);
	return 0;
}

int uartcon_exit(){

	// release reserved resources
	return 0;
}

//module_init(uartcon_init);
//module_exit(uartcon_exit);
