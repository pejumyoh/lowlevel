.global wait				@ This function takes one argument: r0: amount of time to wait in microseconds
wait:

	push	{r4}
	ldr	r1, =0x20003000		@ Load system timer base address into r1
	ldr	r3, =0xffffffff		@ Load the max value for 32 bit register into r3
	sub	r4, r3, r0		@ Subtract the delay from the max value and insert the result into r4

	ldr	r2, [r1,#4]		@ Store the lower 32 bits from the timer counter. This is the time-stamp!
	cmp	r4, r2			@ compare the time-stamps
	subls	r4, r2, r4		@ If the time-interval is too long and register max value would be reached
	bls 	waitloopls		@ and go to waitloopls
	addhi	r4, r2, r0 		@ In case the delay fits to the remaining bits, then set the new upper limit for time
	bhi	waitloophi		@ If r4 was larger than r2, then go to waitloophi

waitloopls:				@ loop until we reach the max. value of the register

	ldr	r2, [r1,#4]		@ Store the lower 32 bits from the timer counter. This is the time-stamp!
	cmp	r4, r2			@ compare the time-intervals
	ble	waitloopls		@ Loop as long as the counter has resetter from zero
	b	endd

waitloophi:

	ldr	r2, [r1,#4]		@ Store the lower 32 bits from the timer counter. This is the time-stamp!
	cmp	r4, r2			@ compare the time-intervals
	bhi	waitloophi		@ Loop until the timestamp exceeds r4

endd:
	pop	{r4}

	mov pc, lr			@ return to main program
