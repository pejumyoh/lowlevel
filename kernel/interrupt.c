#include "string.h"
#include "console.h"
#include "interrupt.h"
#include "device.h"
#include "io.h"

#define IRQ_MAX_CHIPS	10
#define MAX_IRQS	200
#define MAX_HANDLERS	10

extern void wait(uint32_t time);

static void (*platform_irq_pending)(uint32_t*, uint16_t*) = NULL;

static uint32_t pend_irq_table[MAX_IRQS];
static void (*irq_handler_table[MAX_IRQS])(uint32_t) = {NULL};
static void (*irq_mask_table[MAX_IRQS])(uint32_t) = {NULL};
static void (*irq_unmask_table[MAX_IRQS])(uint32_t) = {NULL};

static struct irq_chip* irq_chip_vector[IRQ_MAX_CHIPS]={NULL};

static int counter = 0;

void swi_handler(){
	printk("%s: SWI Interrupt handler...\n",__func__);
}

void irq_handler(){
	uint16_t pend = 0;
	int i;
	counter++;
	platform_irq_pending(pend_irq_table,&pend);

//	printk("PENDING INTERRUPTS = %d\n",pend);

	// call the irq handlers for each detected interrupt
	for(i=0;i<=pend-1;i++){
		if(irq_handler_table[pend_irq_table[i]]!=NULL){
		//	printk("%s: Calling interrupt handler for IRQ%d\n",__func__,pend_irq_table[i]);
		//	printk("%s: HANDLER ADDRESS = %x\n",__func__,irq_handler_table[pend_irq_table[i]]);
			irq_handler_table[pend_irq_table[i]](pend_irq_table[i]);
		}
	}

	// mask the pending interrupts
	for(i=0;i<=pend-1;i++){
		if(irq_mask_table[pend_irq_table[i]]!=NULL){
		//	printk("%s: Calling interrupt masking function for IRQ%d\n",__func__,pend_irq_table[i]);
			irq_mask_table[pend_irq_table[i]](pend_irq_table[i]);
		}
	}
}

void fiq_handler(){
	printk("%s: FIQ Interrupt handler...\n",__func__);
}


void manage_interrupt(uint32_t irq){

	// Second-stage interrupt handler.
	// Launch kernel thread to run pending interrupts

	// For now, just handle them here.
	if(irq_handler_table[irq]!=NULL){
	//	printk("%s: Managing interrupt %d\n",__func__,irq);
	//	printk("%s: HANDLER ADD = %x\n",__func__,irq_handler_table[irq]);
		irq_handler_table[irq](irq);
	}
	else{
	//	printk("%s: No Handler for irq %d\n",__func__,irq);
	}

/*
	printk("HANDLER TABLE:\n");
	for(int i=0;i<=MAX_IRQS-1;i++){
		printk("%d --> %x\n",i,irq_handler_table[i]);
		wait(250000);
	}
*/
}

void irq_register_probe_pending(void (*f)(uint32_t* irqs, uint16_t* count)){
	platform_irq_pending=f;
//	printk("%s: Platform Irq handler registered\n",__func__);
}

/* We need quick access for clearing the pending interrupts
   So those low-level (platform) drivers which are bound to devices 
   capable of generating/receiving interrupts should register their 
   irq_chip (equipped with masking and unmasking functions) to the 
   irq subsystem */

int irq_register_chip(struct irq_chip* chip)
{
	int i;
	
	// Check that the chip is not already registered
	for(i=0;i<=IRQ_MAX_CHIPS-1;i++){
		if(irq_chip_vector[i]!=NULL){
			if(strcmp(chip->name,irq_chip_vector[i]->name)){
				printk("%s: IRQ chip already registered!\n",__func__);
				return -1;
			}		
		}
	}

	for(i=0;i<=IRQ_MAX_CHIPS-1;i++){
		if(irq_chip_vector[i]==NULL){
			irq_chip_vector[i]=chip;
			break;
		}
	}
/*
	for(i=0;i<=chip->nr_irqs-1;i++){
		irq_mask_table[chip->irqvec[i]] = chip->mask_irq;
		irq_unmask_table[chip->irqvec[i]] = chip->unmask_irq;		
	//	printk("%s: Registered interrupt line irq=%d\n",__func__,chip->irqvec[i]);
	//	printk("%s: irq_mask_table[chip->irqvec[i]] = %x\n",__func__,irq_mask_table[chip->irqvec[i]]);
	}
*/	

//	printk("%s: Registered IRQ-chip %s\n",__func__,chip->name);
	return 0;
}

int irq_unregister_chip(struct irq_chip* chip)
{
	int i;
	
	char *devname = chip->name;
	struct irq_resource *res = request_irqresource(devname);

	// Check that the chip is registered
	for(i=0;i<=IRQ_MAX_CHIPS-1;i++){
		if(irq_chip_vector[i]!=NULL){
			if(strcmp(chip->name,irq_chip_vector[i]->name)){

				// Clear the chip from the irq_chip_vector
				irq_chip_vector[i] = NULL;

				// Clear the related making and unmasking function entries
				for(i=res->start;i<=res->end;i++){
					irq_mask_table[i] = NULL;
					irq_unmask_table[i] = NULL;
					printk("%s: Mask & unmask functions cleared for interrupt %d\n",__func__,i);
				}

				printk("%s: IRQ Chip %s unregistered!\n",__func__,chip->name);
				return 0;
			}
		}
	}

	printk("%s: Could not find IRQ-chip %s\n",__func__,chip->name);

	return -1;
}

int request_irq(void (*hdlr)(uint32_t), uint32_t irq)
{
	int i,j;
	irq_handler_table[irq] = hdlr;

	int exitloop = 0;
	struct irq_chip* chip;

	// Go through the registered chips and find suitable masking functions
	for(i=0;i<=IRQ_MAX_CHIPS-1 && exitloop==0;i++){
		if(irq_chip_vector[i]!=NULL){

			chip = irq_chip_vector[i];

			for(j=0;j<=chip->nr_irqs-1 && exitloop==0;j++){
		
				if(irq==chip->irqvec[j]){
				//	printk("FOUND SUITABLE MASK FUNCTION!\n");
					irq_mask_table[irq] = chip->mask_irq;
					exitloop = 1;
					break;
				}
			}
		}
	}
	
	if(irq_mask_table[irq]==NULL){
		printk("%s: No IRQ chip available for interrupt %d\n",__func__,irq);
	}

//	printk("%s: Registered interrupt handler for irq = %d\n",__func__,irq);

	return 0;
}

int release_irq(char* devname, uint32_t irq)
{
	struct irq_resource *res = request_irqresource(devname);
	uint8_t irqindex = 0;

	if(res->start >=0 && res->end >= res->start){
		// Evelything ok
	}
	else{
		printk("%s: IRQ resources not initialized properly for device %s!\n",__func__,devname);	
		return -1;
	}

	if(irq < res->start || irq > res->end){
		printk("%s: Invalid IRQ RELEASE request for device %s! Released: %d and range is [%d -> %d]\n",__func__,devname,irq,res->start,res->end);
		return -1;
	}
	
	irqindex = irq - res->start;

	res->handlers[irqindex] = NULL;
	irq_handler_table[irq] = NULL;

	printk("%s: Released interrupt for irq = %d\n",__func__,irq);

	return 0;
}
