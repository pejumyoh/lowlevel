#include "initcalls.h"
#include "console.h"
#include "device.h"
#include "gpio.h"
#include "interrupt.h"

static uint16_t NR_GPIO_PINS = 0;

// Initially all the gpio pins are free (0)
static uint16_t GPIO_PINS[100] = {0};

static struct gpio_api *gpiodev = NULL;


uint32_t gpiocore_irq_to_gpio(uint32_t irq){
	// Translate a given irq to gpio pin
	return gpiodev->irq_to_gpio(irq);
}

uint32_t gpiocore_gpio_to_irq(uint32_t pin){
	// Translate a given gpio pin to irq
	return gpiodev->gpio_to_irq(pin);
}

int gpiocore_request_gpio(uint32_t pin){

	// Check if the requested gpio pin is free
	if(GPIO_PINS[pin]!=0){
		// The pin is already taken
		printk("%s: Requested gpio pin (%d) is not free!\n",__func__,pin);
		return -1;
	}
	else{
		// The pin is free.
		// Mark the requested gpio pin as reserved (1)
		GPIO_PINS[pin]=1;
	}

	printk("%s: Reserved gpio pin %d\n",__func__,pin);

	return 0;
}

int gpiocore_setdrvdata(struct gpio_data* data){
	printk("%s: Setting gpiocore drvdata...\n",__func__);
	NR_GPIO_PINS = data->NR_GPIO_PINS;
	return 0;
}

int gpio_handle_irq(uint32_t mapped_irq){
	printk("%s: Handling GPIO-Specific interrupt %d!\n",__func__,mapped_irq);

	return 0;
}

int gpiocore_init(void)
{
	// Initializing GPIO-CORE.

	// First, ask for a gpio-api from the device manager
	gpiodev = (struct gpio_api*)request_device(GPIO_DEV);

	if(gpiodev==NULL){
                // Handle this!
		printk("%s: Could not find any GPIO-devices!\n",__func__);
		return -1;
        }
	
	printk("%s: Gpio Core Initialized\n",__func__);
	return 0;
}

int gpiocore_exit(void){
	// release allocated resources
	return 0;
}

// Core-level component
//core_init(gpiocore_init);
//core_exit(gpiocore_exit);
