#include <stdarg.h>
#include "io.h"
#include "framebuffer.h"
#include "fbcon.h"
#include "console.h"
#include "string.h"
#include "algo.h"
#include "buddy.h"
#include "font_8x16.h"
#include "device.h"
#include "txtdev.h"
#include "colordefs.h"
#include "initcalls.h"

static struct fbcon	*fbterm = NULL;

int fbcon_init();
int fbcon_exit();

struct device fbcon_dev;
struct device_driver fbcon_driver;


static void scroll(){
	fbterm->fbdev->scrollscreen_y(16);
}

static void drawcharacter(uint8_t ch, uint32_t row, uint32_t col, uint16_t color){
	uint32_t xpix = (col-1)*8;
	uint32_t ypix = (row-1)*16;
	uint8_t r,c;
	uint8_t row_ch;
	uint32_t x=0,y=0;

	const uint8_t *font = fontdata + (ch<<4);
	uint16_t mask = 0b100000000;

	if(row<1 || col<1){
	    // Handle this!
	}
	// go through each pixel row in the font
	for(r=0;r<16;r++) {
		x=0;
		row_ch = read8((uint32_t)(font++));
		for(c=1;c<=8;c++){
			if(row_ch&(mask>>c)) // the bit is on.
				fbterm->fbdev->drawpixel(xpix+x,ypix+y,color);
			else // the bit is off.
				fbterm->fbdev->drawpixel(xpix+x,ypix+y,fbterm->bgcolor);
			x++;
		}
		y++;
	}
}

void drawstring(char *buf, uint16_t color){

	uint32_t i;
	uint32_t row = fbterm->rowpos;
	uint32_t col = fbterm->colpos;
	uint32_t lr = row;
	uint32_t lc = col;
	size_t size = strlen(buf);

	if(row < 1 || col < 1 || row > fbterm->max_rows+1 || col > fbterm->max_cols+1){
		drawcharacter('s',1,50,PRINTK_COLOR_DEFAULT);
	}
	else{
		if(lr>fbterm->max_rows){
			scroll(); lc=1; lr--;
		}
		for(i=0;i<size;i++){
			if(buf[i] == '\n'){
				// add line change
				lc=1; lr++;
			}
			else if(buf[i] == '\r'){
				// rewind to beginning of the line	
				lc=1;
			}
			else{
				drawcharacter(buf[i],lr,lc,color);
				lc++;
			}
			if(lc > fbterm->max_cols){
				lc=1; lr++;
				if(lr>fbterm->max_rows){
					scroll();
					lc=1; lr--;
				}
			}
		}
	}
	fbterm->rowpos = lr;
	fbterm->colpos = lc;
}

static void fbcon_reset(){
	fbterm->bgcolor = Black;
	fbterm->fontcolor = PRINTK_COLOR_DEFAULT;
	fbterm->rowpos = 1;
	fbterm->colpos = 1;

	fbterm->fbdev->clear(fbterm->bgcolor);	
	printk("%s: Framebuffer console resetted!\n",__func__);
}


static struct txt_api fbcon_api = {
	.txtdev_putstring	= drawstring,
	.txtdev_reset		= fbcon_reset,
};

int fbcon_init(){

	// Initializing Framebuffer console.

	uint16_t i;

	// First, ask for a framebuffer from the device manager
	struct framebuffer_api *fbdev = (struct framebuffer_api*)request_device(FB_DEV);

	if(fbdev==NULL){
		// No Framebuffers available!
		return -1;
	}

	fbterm = (struct fbcon*) kmalloc(sizeof(struct fbcon));

	fbterm->bgcolor = Black;
	fbterm->fontcolor = PRINTK_COLOR_DEFAULT;
	fbterm->rowpos = 1;
	fbterm->colpos = 1;

	if(fbdev!=NULL)
	  fbterm->fbdev=fbdev;
	else{
		// Handle this!!!
	}
	// Determine max number of 8x16 characters that fit into the allocated framebuffer
	// For 1920 x 1200 framebuffer, the maximum numner of characters are:
	// max_cols = 240 ; max_rows = 75

	fbterm->max_cols=0;
	fbterm->max_rows=0;
	for(i=0;i<=fbterm->fbdev->data->width;i+=8)
		fbterm->max_cols++;

	for(i=0;i<=fbterm->fbdev->data->height;i+=16)
		fbterm->max_rows++;

	fbterm->max_cols--; fbterm->max_rows--;


	// Fill in the device info
	fbcon_dev.name = "fbcon";
	fbcon_dev.type = TXT_DEV,
	fbcon_dev.driver = &fbcon_driver,
	fbcon_dev.irq_info.start = -1,
	fbcon_dev.irq_info.end = -1,
	fbcon_dev.data = NULL,

	// Fill in the device driver info
	fbcon_driver.name = "fbcon";
	fbcon_driver.dev = &fbcon_dev;
	fbcon_driver.driver_init = fbcon_init;
	fbcon_driver.driver_exit = fbcon_exit;
	fbcon_driver.api = &fbcon_api;

	device_register(&fbcon_dev);
	device_driver_register(&fbcon_driver);

	return 0;
}

int fbcon_exit(){
	return 0;
}


//core_init(fbcon_init);
//core_exit(fbcon_exit);


uint32_t fbcon_max_rows()
{
	return fbterm->max_rows;
}

uint32_t fbcon_max_cols()
{
	return fbterm->max_cols;
}
