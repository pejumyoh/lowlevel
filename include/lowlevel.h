#ifndef INCLUDE_LOWLEVEL_H
#define INCLUDE_LOWLEVEL_H

extern void enable_interrupts();
extern void disable_interrupts();
extern void dmb();
extern void wmb();
extern void flush_cache();
extern void clinv_dcache();

#endif
