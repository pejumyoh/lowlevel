#ifndef INCLUDE_IO_H
#define INCLUDE_IO_H

#include <stdint.h>
#include <stddef.h>

void write32(uint32_t reg, uint32_t val);
void write16(uint32_t reg, uint16_t val);
void write8(uint32_t reg, uint8_t val);

uint32_t read32(uint32_t reg);
uint16_t read16(uint32_t reg);
uint8_t read8(uint32_t reg);

void memcpy32(uint32_t *from, uint32_t words, uint32_t *to);
void memcpy16(uint16_t *from, uint32_t hwords, uint16_t *to);
void memcpy8(uint8_t *from, uint32_t bytes, uint8_t *to);

void memset32(uint32_t *ptr, uint32_t words, uint32_t val);
void memset16(uint16_t *ptr, uint32_t hwords, uint16_t val);
void memset8(uint8_t *ptr, uint32_t bytes, uint8_t val);

#endif
