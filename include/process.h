#ifndef INCLUDE_PROCESS_H
#define INCLUDE_PROCESS_H

#include <stdint.h>

/* definitions for the various process states */
typedef enum process_state
{
	PROC_RUNNING,
	PROC_SLEEPING

} pstate;

/* main structure for a process */
struct process
{
	char		name[32];
	uint32_t	pid;
	pstate 		state;
	uint32_t	stacksize;
	uint32_t	*sp;
};

#endif
