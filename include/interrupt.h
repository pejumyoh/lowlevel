#ifndef INCLUDE_INTERRUPT_H
#define INCLUDE_INTERRUPT_H

#include <stddef.h>

#define MAX_HANDLERS_PER_CHIP	62

struct irq_resource
{
	uint32_t start;
	uint32_t end;
	void (* handlers[MAX_HANDLERS_PER_CHIP])(uint32_t);
};

struct irq_chip{	
	char* name;
	void (*unmask_irq)(uint32_t irq);
	void (*mask_irq)(uint32_t irq);
	void (*enable_irq)(uint32_t irq, uint32_t mode);
	void (*disable_irq)(uint32_t irq, uint32_t mode);
	uint32_t nr_irqs;
	uint32_t *irqvec;
};

void manage_interrupt(uint32_t irq);
void irq_register_probe_pending(void (*)(uint32_t* irqs,uint16_t* count));
int irq_register_chip(struct irq_chip* chip);
int irq_unregister_chip(struct irq_chip* chip);
int request_irq(void (*hdlr)(uint32_t), uint32_t irq);
int release_irq(char* devname, uint32_t irq);

#endif
