#ifndef INCLUDE_GPIO_H
#define INCLUDE_GPIO_H

#include "device.h"

#define GPIO_RISING_DETECT		(1<<1)
#define GPIO_FALLING_DETECT		(1<<2)
#define GPIO_ASYNCH_RISING_DETECT	(1<<3)
#define GPIO_ASYNCH_FALLING_DETECT	(1<<4)
#define GPIO_HIGH_DETECT		(1<<5)
#define GPIO_LOW_DETECT			(1<<6)

#define DETECT_ALL      (GPIO_RISING_DETECT|GPIO_FALLING_DETECT|GPIO_ASYNCH_RISING_DETECT|GPIO_ASYNCH_FALLING_DETECT|GPIO_HIGH_DETECT|GPIO_LOW_DETECT)

typedef enum{
	GPIO_INPUT,	/* gpio is input */
	GPIO_OUTPUT,	/* gpio in output */
	GPIO_ALT0,	/* gpio is alt0 */
	GPIO_ALT1,	/* gpio is alt1 */
	GPIO_ALT2,	/* gpio is alt2 */
	GPIO_ALT3,	/* gpio is alt3 */
	GPIO_ALT4,	/* gpio is alt4 */
	GPIO_ALT5,	/* gpio is alt5 */
} gpio_function;

typedef enum{
	GPIO_PUD_UP_DOWN_DISABLE,
	GPIO_PUD_DOWN_ENABLE,
	GPIO_PUD_UP_ENABLE,
} gpio_pudctrl;

struct gpio_data{
	uint32_t	NR_GPIO_PINS;
	uint32_t	flags;
};

struct gpio_api{
	uint32_t	(*irq_to_gpio)(uint32_t irq);
	uint32_t	(*gpio_to_irq)(uint32_t pin);
	void		(*gpio_function_set)(uint32_t pin, gpio_function func);
	void		(*gpio_set)(uint32_t pin);
	void		(*gpio_clear)(uint32_t pin);
	uint32_t	(*gpio_pinlevel)(uint32_t pin);
	void 		(*gpio_enable_irq)(uint32_t irq, uint32_t mode);
	void 		(*gpio_disable_irq)(uint32_t irq, uint32_t mode);
	int		(*gpio_pullupdown_set)(uint32_t pin, gpio_pudctrl mode);
	struct gpio_data  *data;
};

int gpiocore_setdrvdata(struct gpio_data* data);
uint32_t gpiocore_irq_to_gpio(uint32_t irq);
uint32_t gpiocore_gpio_to_irq(uint32_t pin);
int gpiocore_request_gpio(uint32_t pin);
int gpio_handle_irq(uint32_t mapped_irq);
int gpiocore_init();

#endif
