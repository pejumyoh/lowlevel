#ifndef INCLUDE_TXTDEV_H
#define INCLUDE_TXTDEV_H

#include <stdint.h>

#define MAX_TEXT_OUTPUT_DEVS	8

struct txt_api
{
	void	(*txtdev_putstring)(char *buf, uint16_t color);
	void	(*txtdev_reset)(void);
};

//int txtdev_register(struct txt_api*);
//int txtdev_unregister(struct txt_api*);

#endif
