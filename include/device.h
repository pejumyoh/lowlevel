#ifndef INCLUDE_DEVICE_H
#define INCLUDE_DEVICE_H

/*
 *	Device API of the Kernel
 *
 *	Any subsystem (framebuffer, gpio, etc..) may use these
 *	structs as a base for more elaborated device and driver 
 *	description.
 */

#include <stdint.h>
#include <stddef.h>
#include "interrupt.h"
#include "txtdev.h"

typedef enum{
	FB_DEV,
	UART_DEV,
	GPIO_DEV,
	TIMER_DEV,
	TXT_DEV,
	MODULE_DEV,
} device_type;

struct device;

struct device_driver{
	char*			name;			/* Device driver name */
	struct device*		dev;			/* The device struct where this driver belongs */ 
	int			(*driver_init)(void);	/* device driver init function	*/
	int			(*driver_exit)(void);	/* device driver exit function	*/
	void*			api;			/* pointer to device driver API struct */
};

struct device{
	char*			name;			/* device name */
	device_type		type;			/* device type identifier */
	struct device_driver*	driver;			/* pointer to the device driver info struct */
	struct irq_resource	irq_info;		/* irq information */
	void*			data;			/* Additional device data */
};

int device_register(struct device* dev);
void device_driver_register(struct device_driver* driver);
void* request_device(device_type type);
struct irq_resource* request_irqresource(char* devname);
int request_devices(device_type type, int (*request_callback)(void* dev, char* name));

#endif
