#ifndef INCLUDE_ATAGS_H
#define INCLUDE_ATAGS_H

#include <stdint.h>

#define ATAG_NONE	0
#define ATAG_CORE	1
#define ATAG_MEM	2
#define ATAG_VIDEOTEXT	3
#define ATAG_RAMDISK	4
#define ATAG_INITRD2	5
#define ATAG_SERIALNR	6
#define ATAG_REVISION	7
#define ATAG_VIDEOLFB	8
#define ATAG_CMDLINE	9

struct atag_header{
	uint32_t size;
	uint32_t tag;
};

struct atag_core{
	uint32_t flags;		/* bit 0 = read-only */
	uint32_t pagesize;	/* systems page size (usually 4k) */
	uint32_t rootdev;	/* root device number */
};

struct atag_mem{
	uint32_t size;
	uint32_t start;
};

struct atag_videotext {
	uint8_t	x;
	uint8_t	y;
	uint16_t video_page;
	uint8_t	video_mode;
	uint8_t	video_cols;
	uint16_t video_ega_bx;
	uint8_t	video_lines;
	uint8_t	video_isvga;
	uint16_t video_points;
};

struct atag_ramdisk{
	uint32_t flags;
	uint32_t size;
	uint32_t start;
};

struct atag_initrd2{
	uint32_t start;
	uint32_t size;
};

struct atag_serial{
	uint32_t low;
	uint32_t high;
};

struct atag_revision{
	uint32_t rev;
};

struct atag_videolfb{
	uint16_t lfb_width;
	uint16_t lfb_height;
	uint16_t lfb_depth;
	uint16_t lfb_linelength;
	uint32_t lfb_base;
	uint32_t lfb_size;
	uint8_t	red_size;
	uint8_t	red_pos;
	uint8_t	green_size;
	uint8_t	green_pos;
	uint8_t	blue_size;
	uint8_t	blue_pos;
	uint8_t	rsvd_size;
	uint8_t	rsvd_pos;
};

struct atag_cmdline{
	char	*cmdline;
};


struct atags
{
	/* Pointers to different tags */
	struct atag_core*	core;
	struct atag_mem*	mem;
	struct atag_videotext*	videotext;
	struct atag_ramdisk*	ramdisk;
	struct atag_initrd2*	initrd2;
	struct atag_serialnr* 	serialnr;
	struct atag_revision*	revision;
	struct atag_videolfb*	videolfb;
	struct atag_cmdline*	cmdline;
};


void inittags(unsigned long *atag_addr);

unsigned long atags_get_avail_mem();

char* atags_get_cmdline();

#endif
