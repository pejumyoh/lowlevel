#ifndef INCLUDE_MAIL_H
#define INCLUDE_MAIL_H

uint32_t writemailbox(uint32_t data, uint8_t channel);
uint32_t readmailbox(uint8_t channel);
#endif
