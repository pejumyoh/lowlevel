#ifndef INCLUDE_KERNEL_H
#define INCLUDE_KERNEL_H

#include "bcm2708_platform.h"
#include "framebuffer.h"
#include "atags.h"

struct kernel_components
{
	struct fb_info *fb;	/* Pointer to the framebuffer structure */
	struct console *term;	/* Pointer to the framebuffer console */

	struct atags tags;	/* Structure containing pointers to different tags */
};

#endif
