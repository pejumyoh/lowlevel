#ifndef INCLUDE_FRAMEBUFFER_H
#define INCLUDE_FRAMEBUFFER_H

/* 
 *	Programming API for Framebuffer subsystem
 *
 */

#include <stdint.h>
#include "device.h"

struct fb_data{

	uint32_t width;		/* Physical Width							*/
	uint32_t height;	/* Physical Height							*/
	uint32_t vwidth;	/* Virtual Width							*/
	uint32_t vheight;	/* Virtual Height							*/
	uint32_t pitch;		/* Pitch (Number of bytes between each row of the frame buffer)		*/
	uint32_t bitdepth;	/* Bit Depth (bits per pixel)						*/
	uint32_t xoff;		/* X-offset (Offset in the x direction)					*/
	uint32_t yoff;		/* Y-offset (Offset in the y direction)					*/
	uint32_t size;		/* Framebuffer Size.							*/
	uint8_t  fb_id;		/* Id of current active framebuffer 					*/
	uint8_t  fb_devs;	/* Total number of available framebuffers 				*/
};

struct framebuffer_api{

	void 		(*drawpixel)(uint32_t x, uint32_t y, uint16_t color);
	void 		(*drawline)(uint32_t x1, uint32_t y1, uint32_t x2, uint32_t y2, uint16_t color);
	void 		(*scrollscreen_y)(uint32_t ypixels);
	void 		(*clear)(uint32_t color);
	void 		(*changebuffer)(uint8_t buf_if);

	struct fb_data	*data;
};

/*
 *
 */
struct framebuffer_device{

	struct device*	dev;
};


#endif
