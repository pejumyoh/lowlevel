#ifndef INCLUDE_BUDDY_H
#define INCLUDE_BUDDY_H

#define M_FLAG_FREE		0
#define M_FLAG_RESERVED		1

#define BLOCK0_SIZE		4096			// block size = 4kB = 2^12 bytes
#define DEV_MEM			512*1024*1024		// Maximum memory in the device		(0x00000000->0x20000000)
#define MEM_VC			128*1024*1024		// Reserved memory for videocore	(0x18000000->0x20000000)
#define MEM_STACK		32*1024			// Memory for for atags and stack	(0x00000000->0x00008000)
#define MEM_KERNEL		100*1024		// Memory for kernel (100kB now)	(0x00008000->0x00021000)
#define ALLOCATOR_BASE		132*1024		// Set memory allocator to 132kB

struct mnode{
	unsigned char header;	// two lowest bits = flags, secont 6 bits = order
	struct mnode *next;	// pointer to next free block
};

uint8_t	buddy_init(unsigned long *memory_base, unsigned long offset, uint32_t memsize);
char* buddy_malloc(size_t size);
void buddy_free(void *memptr);
unsigned long getmaxorder(void);

#define kmalloc(x)	buddy_malloc(x)
#define kfree(x)	buddy_free(x)

#endif
