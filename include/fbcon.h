#ifndef INCLUDE_FBCON_H
#define INCLUDE_FBCON_H

#include <stdint.h>
#include <stddef.h>
#include "colordefs.h"
#include "framebuffer.h"

struct fbcon{
	uint32_t bgcolor;		/* background color 					*/
	uint32_t fontcolor;		/* font color 						*/
	uint32_t rowpos;		/* row position 					*/
	uint32_t colpos;		/* column position 					*/
	uint32_t max_rows;		/* max number of characters in y-direction		*/
	uint32_t max_cols;		/* max number of characters in x-direction 		*/
	struct framebuffer_api *fbdev;	/* pointer to the framebuffer api & data structure 	*/
};

int fbcon_init();

uint32_t fbcon_max_rows();
uint32_t fbcon_max_cols();

#endif
