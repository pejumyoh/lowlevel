#ifndef INCLUDE_INITCALLS_H
#define INCLUDE_INITCALLS_H


// PLATFORM INIT FUNCTIONS
typedef int (*platform_initcall_t)(void);
typedef int (*platform_exitcall_t)(void);
#define platform_init(fn) static platform_initcall_t __platform_initcall_##fn __attribute__((used)) __attribute__((section("platform_init_fptrs"))) = fn
#define platform_exit(fn) static platform_exitcall_t __platform_exitcall_##fn __attribute__((used)) __attribute__((section("platform_exit_fptrs"))) = fn

// CORE COMPONENT INIT FUNCTIONS
typedef int (*core_initcall_t)(void);
typedef int (*core_exitcall_t)(void);
#define core_init(fn) static core_initcall_t __core_initcall_##fn __attribute__((used)) __attribute__((section("core_init_fptrs"))) = fn
#define core_exit(fn) static core_exitcall_t __core_exitcall_##fn __attribute__((used)) __attribute__((section("core_exit_fptrs"))) = fn

// MODULE INIT FUNCTIONS
typedef int (*module_initcall_t)(void);
typedef int (*module_exitcall_t)(void);
#define module_init(fn) static module_initcall_t __module_initcall_##fn __attribute__((used)) __attribute__((section("module_init_fptrs"))) = fn
#define module_exit(fn) static module_exitcall_t __module_exitcall_##fn __attribute__((used)) __attribute__((section("module_exit_fptrs"))) = fn

void do_initcalls();

#endif
