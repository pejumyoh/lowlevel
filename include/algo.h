#ifndef INCLUDE_ALGO_H
#define INCLUDE_ALGO_H

int powof(int val, int p);
uint32_t divide(uint32_t divident, uint32_t divisor);

#define TOKB(x)                 (unsigned long int)(divide(x,1024))
#define TOMB(x)                 (unsigned long int)(divide(TOKB(x),1024))

#endif
