#ifndef INCLUDE_STRING_H
#define INCLUDE_STRING_H

#include <stddef.h>
#include <stdint.h>

size_t strlen(char* buf);
int strcmp(char *buf1, char* buf2);
void hextostring(char *buf, uint32_t hex);

#endif
