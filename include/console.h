#ifndef INCLUDE_CONSOLE_H
#define INCLUDE_CONSOLE_H

#include <stdint.h>
#include <stddef.h>
#include "colordefs.h"

int console_init(void);
void console_reset();
void printk(char *buf, ...);
void nullerror_print();
void console_rewind(void);

#define PRINTK_MAX_LENGHT	4096
#define PRINTK_MAX_ARGS		4096

#define PRINTK_COLOR_DEFAULT	White
#define PRINTK_COLOR_HEX	Purple
#define PRINTK_COLOR_DEC	Blue
#define PRINTK_COLOR_CHAR	Yellow
#define PRINTK_COLOR_STRING	Green
#define PRINTK_COLOR_ERROR	Red


#endif
