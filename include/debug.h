#ifndef INCLUDE_DEBUG_H
#define INCLUDE_DEBUG_H

#include "console.h"

#define NULLTST(x,...)\
	if(x==NULL){nullerror_print();}\
	else{\
		do{\
		void *_p[] = { __VA_ARGS__ };\
		int _i;\
		for(_i = 0; _i < sizeof(_p)/sizeof(*_p); _i++){\
			if(_p[_i] == NULL){\
				nullerror_print();\
			}\
		}\
		}while(0);\
	}
#endif
