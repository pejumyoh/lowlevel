#ifndef INCLUDE_UART_H
#define INCLUDE_UART_H

#include <stdint.h>

#define UART_STD_BRATE_300		300
#define UART_STD_BRATE_1200		1200
#define UART_STD_BRATE_2400		2400
#define UART_STD_BRATE_4800		4800
#define UART_STD_BRATE_9600		9600
#define UART_STD_BRATE_19200		19200
#define UART_STD_BRATE_38400		38400
#define UART_STD_BRATE_57600		57600
#define UART_STD_BRATE_115200		115200
#define UART_STD_BRATE_230400		230400

#define UART_PARITY_DISABLE		0
#define UART_PARITY_EVEN		1
#define UART_PARITY_ODD			2


struct uart_api
{
	int (*uart_tx_byte)(unsigned char c);
	unsigned char (*uart_rx_byte)();
	int (*uart_set_baudrate)(unsigned int brate);
	int (*uart_set_parity)(uint8_t parity);	
};

#endif
