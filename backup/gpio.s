.global GetGpioAddress
GetGpioAddress:
	ldr 	r0,=0x20200000
	mov 	pc,lr

.global SetGpioFunction			@ This function takes two inputs: r0 = GPIO pin number, r1 = GPIO pin function
SetGpioFunction:
	cmp 	r0, #53			@ Check r0 param limit (GPIO pin number)
	cmpls 	r1, #7			@ Check r1 param limit (GPIO pin function = number from 0 to 7 (3 bits) )
	movhi	pc, lr			@ If parameters not in limits, then return
	push	{lr}			@ Otherwise, push link register into stack
	mov	r2, r0			@ put r0 into r2. r2 contains now the GPIO pin number
	bl	GetGpioAddress		@ call getGpioAddress. r0 contains then the GPIO base address

functionLoop:
	cmp 	r2, #9			@ Check if r2 input is 9
	subhi	r2, #10			@ If r2 was larger than 9, then subtract 10 from r2
	addhi	r0, #4			@ Also in that case add 4 (bytes) to GPIO address provided by GetGpioAddress
	bhi	functionLoop		@ Also, continue the loop until we found a value which is between [0...9]

					@ Now we know the pin number in correct GPIO GPFSELn register (given by r0).
					@ Because each GPIO pin reserves 3 bits, we multiply the pin number by 3:

	add	r2, r2,lsl #1		@ This is the same as r2 = r2 + (r2<<1), where (r2<<1) = 2*r2
	lsl	r1, r2			@ Shift the function bitmask (3 bits) to correct pin position
	str	r1, [r0]		@ Store the r1 value into address contained in r0
	pop	{pc}			@ pop a value from the stack (lr) and place it into pc

.global SetGpio				@ This function takes two inputs: r0 = GPIO pin number, r1 = GPIO pin value
SetGpio:
	pinNum	.req	r0		@ Assign alias to register r0
	pinVal	.req	r1		@ Assign alias to register r1

	cmp 	pinNum, #53		@ Check if pin number is equal to 53
	movhi	pc, lr			@ If it was larger, then return
	push	{lr}			@ otherwise push the link register to stack
	mov	r2, pinNum		@ insert pinNum into r2

	.unreq	pinNum			@ Unalias the pinNum
	pinNum	.req	r2		@ make a new alias for pinNum ot point to r2

	bl GetGpioAddress		@ call GetGpioAddress

	gpioAddr .req	r0		@ make alias for gpioAddress (r0)
	pinBank	 .req	r3		@ make alias for pinBank (r3)

	lsr	pinBank, pinNum, #5	@ divide pin number by 32 to determine which pin bank the pin belongs to.
	lsl	pinBank, #2		@ multiply the resulting pinBank with 4 to get the number of bytes for address offset
	add	gpioAddr, pinBank	@ add the offset to gpioAddr
	.unreq	pinBank			@ unregister the alias pinBank

	and	pinNum, #31		@ Pick the remaining 5 bits from the pinNum to find the correct pin the pinBank
	setBit	.req	r3		@ alias for the setBit
	mov	setBit, #1		@ initialize the setBit with 1
	lsl	setBit, pinNum		@ Shift the setBit to correct position in the bitBank

	teq	pinVal, #0		@ Check if the pinVal is zero
	.unreq	pinVal			@ unregister the alias pinVal

	streq	setBit, [gpioAddr,#40]	@ in case the pinVal was 0, put it into the GPIO pin Clear Register
	strne	setBit, [gpioAddr,#28]	@ otherwise put it into GPIO pin Set Register

	.unreq	setBit			@ unregister setBit
	.unreq	gpioAddr		@ unregister gpioAddr

	pop	{pc}			@ pop the lr from the stack and insert it into pc
