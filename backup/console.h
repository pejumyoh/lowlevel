#ifndef INCLUDE_CONSOLE_H
#define INCLUDE_CONSOLE_H

#include <stdint.h>
#include <stddef.h>
#include "colordefs.h"

struct console
{
	uint32_t bgcolor;	/* background color 				*/
	uint32_t fontcolor;	/* font color 					*/
	uint32_t rowpos;	/* row position 				*/
	uint32_t colpos;	/* column position 				*/
	uint32_t max_rows;	/* max number of characters in y-direction	*/
	uint32_t max_cols;	/* max number of characters in x-direction 	*/
	struct fb_info *fb;	/* pointer to the framebuffer info structure 	*/
};

struct console* initconsole(struct fb_info *fb);
void drawcharacter8x16(uint8_t ch, uint32_t row, uint32_t col, uint16_t color);
void drawstring8x16(char *buf, size_t size, uint32_t row, uint32_t col, uint16_t color);
void scroll(void);
void debug_print(char* buf);
void kputs(char *buf, uint16_t color);
void reset_console();
void printhex(unsigned long hex);
void printdec(unsigned long dec);
void printk(char *buf, ...);
void nullerror_print();

#define PRINTK_MAX_LENGHT	4096
#define PRINTK_MAX_ARGS		4096

#define PRINTK_COLOR_DEFAULT	White
#define PRINTK_COLOR_HEX	Purple
#define PRINTK_COLOR_DEC	Blue
#define PRINTK_COLOR_CHAR	Yellow
#define PRINTK_COLOR_STRING	Green
#define PRINTK_COLOR_ERROR	Red


#endif
