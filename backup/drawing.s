.section .data
.align 1
ForeColour:
	.hword	0xffff

.align 2
GraphicsAddress:
	.int	0

.include "font_8x16.s"

.section .text
.global SetForeColour
SetForeColour:

	cmp	r0, #0x10000	@ Check if the input variable is larger or equal to max. 16 bit value
	movhs	pc, lr
	ldr	r1, =ForeColour
	strh	r0, [r1]
	mov	pc, lr

.global SetGraphicsAddress
SetGraphicsAddress:

	ldr	r1, =GraphicsAddress
	str	r0, [r1]
	mov	pc, lr


.global DrawPixel
DrawPixel:

	px	.req	r0
	py	.req	r1
	addr	.req	r2

	height	.req	r3
	ldr	height, [addr, #4]
	sub	height, #1
	cmp	py, height
	movhi	pc, lr
	.unreq	height

	width	.req	r3
	ldr	width, [addr, #0]
	sub	width, #1
	cmp	px, width
	movhi	pc, lr

	add	width, #1
	ldr	addr, [addr, #32]
	mla	px, py, width, px	@ multiply and add: px = py*width + px
	add	addr, px, lsl #1
	.unreq	width

	fore	.req	r3

	ldr	fore,=ForeColour
	ldrh	fore, [fore]

	@ Draw the pixel
	strh	fore, [addr]

	.unreq	fore
	.unreq	addr
	.unreq	px
	.unreq	py

	mov	pc, lr


.global DrawCharacter
DrawCharacter:

	@; inputs:	r0 = character, r1 = x-coord, r2 = y-coord, r3 = framebuffer address

	cmp	r0, #127
	movhi	pc, lr

	push	{r4,r5,r6,r7,r8,r9,r10,r11,lr}

	CharAddr	.req	r4
	x		.req	r5
	y		.req	r6
	addre		.req	r11

	mov	x, r1
	mov	y, r2
	mov 	addre,r3

	ldr	CharAddr, =fontdef	@ load font base address
	add	CharAddr, r0, lsl #4	@ each font takes 16 bytes so multiply font number with 16

	line	.req	r7
	rowpix	.req	r8
	colpix	.req	r9
	bit	.req	r10

	mov	rowpix, #0		@ start from row pixel offset 0

rowloop:

	ldrb	line, [CharAddr]	@ load 8 bits (one row) from the font
	mov	colpix, #0		@ initialize the column to zero
	mov	bit, #0b100000000	@ bitmask to compare with the font bit representation

colloop:
	add	colpix, #1		@ column counter add 1
	cmp	colpix, #8		@ check if already processed the whole row
	bgt	newline			@ if yes, then change the line

	lsl	line, #1		@ otherwise shift the bit representation of the current pixel row of the font to left
	tst	line, bit		@ Check if the bit is 1 or 0
	beq	colloop			@ if it was not set, continue loop
setpix:
	add	r0, x, colpix		@ if the pixel was set, then add the current colpix to x and insert result to r0
	add	r1, y, rowpix		@ same thing to y
	mov	r2, addre
	bl	DrawPixel		@ draw the pixel
	b	colloop			@ continue the row drawing

newline:

	add	CharAddr, #1		@ add one byte (8 bit) to character address. This means that we change the line in the font to be drawn
	add	rowpix, #1		@ add also number of row pixels
	cmp	rowpix, #16		@ check if we are already done
	bls	rowloop			@ if not, then loop again

	mov	r0, colpix
	mov	r1, rowpix

	.unreq	CharAddr
	.unreq	x
	.unreq	y
	.unreq	rowpix
	.unreq	colpix
	.unreq	line
	.unreq	bit

	pop	{r4,r5,r6,r7,r8,r9,r10,r11,pc}




.globl DrawString
DrawString:
	x	.req r4
	y	.req r5
	x0	.req r6
	string	.req r7
	length	.req r8
	char	.req r9

	push	{r4,r5,r6,r7,r8,r9,lr}

	mov	string, r0
	mov	x, r2
	mov	x0, x
	mov	y, r3
	mov	length, r1

stringLoop:
	subs	length, #1
	blt	stringLoopEnd

	ldrb	char, [string]
	add	string, #1

	mov	r0, char
	mov	r1, x
	mov	r2, y
	bl	DrawCharacter

	cwidth	.req r0
	cheight .req r1

	teq	char, #'\n'
	moveq	x, x0
	addeq	y, cheight
	beq	stringLoop

	teq	char, #'\t'
	addne	x, cwidth
	bne	stringLoop

stringLoopEnd:

	.unreq cwidth
	.unreq cheight

	.unreq x
	.unreq y
	.unreq x0
	.unreq string
	.unreq length

	pop {r4,r5,r6,r7,r8,r9,pc}



.globl DrawStringNoSpaces
DrawStringNoSpaces:

	x	.req r4
	y	.req r5
	x0	.req r6
	string	.req r7
	length	.req r8
	char	.req r9

	push	{r4,r5,r6,r7,r8,r9,lr}

	mov	string, r0
	mov	x, r2
	mov	x0, x
	mov	y, r3
	mov	length, r1

stringLoop2:
	subs	length, #1
	blt	stringLoopEnd2

	ldrb	char, [string]
	add	string, #1

	mov	r0, char
	mov	r1, x
	mov	r2, y
	bl	DrawCharacter

	cwidth	.req r0
	cheight .req r1

	teq	char, #'\n'
	moveq	x, x0
	addeq	y, cheight
	beq	stringLoop2

	teq	char, #' '
	moveq	x, x0
	addeq	y, cheight
	beq	stringLoop2

	teq	char, #'\t'
	addne	x, cwidth
	bne	stringLoop2

stringLoopEnd2:

	.unreq cwidth
	.unreq cheight

	.unreq x
	.unreq y
	.unreq x0
	.unreq string
	.unreq length

	pop {r4,r5,r6,r7,r8,r9,pc}
