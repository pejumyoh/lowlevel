.section .init
.global _start
_start:

	mov sp,#0x8000
	b main

	.section .text
main:

	mov	r0, #47
	mov	r1, #1
	bl 	SetGpioFunction

	ptrn 	.req r5
	ldr	ptrn, =pattern
	ldr	ptrn, [ptrn]
	seq	.req r6
	mov	seq, #0

	@ ----------- begin infinite loop
loop:

	mov	r0, #47		@ Set the pin number
	mov	r1, #1		@ Set initial pin value to 1
	lsl	r1, seq		@ Shift the pin value to current position
	and	r1, ptrn	@ Set the pin value to 1 if pattern has 1 at the current position
	bl 	SetGpio		@ set the pin according to the result of and-operation

	ldr	r0,=250000
	bl	wait

	add	seq, #1
	and	seq, #0b11111

	b loop



.section .data
.align 2

pattern:
	.int 0b00000000111111110000000011111111
