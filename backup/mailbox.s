.global GetMailboxBase
GetMailboxBase:
		ldr 	r0, =0x2000B880
		mov	pc, lr


.global MailboxWrite				@ This function takes two inputs: r0 = what to write, r1 = where to write (mailbox address)
MailboxWrite:
		@ Chech that the inputs are ok

		tst	r0, #0b1111		@ four lowest bits must be zero. That data area is reserved to pass the correct mailbox channel (1 = Framebuffer)
		movne	pc, lr
		cmp	r1, #15			@ must be smaller or equal to 4 bit number. This must fit to the remaining lowest 4 bit section of r0
		movhi	pc, lr

		channel	.req	r1
		value	.req	r2
		mov	value, r0
		push	{lr}
		bl	GetMailboxBase
		mailbox	.req	r0

wait$:
		status	.req r3
		ldr	status,[mailbox,#0x18]	@ Read 4 bytes (=word) from the mailbox status register (offset 0x18)
		tst	status,#0x80000000	@ Check that the highest bit in the read word is 0
		.unreq	status
		bne	wait$

		add	value, channel
		.unreq	channel

		str	value, [mailbox,#0x20]
		.unreq	value
		.unreq	mailbox

		pop	{pc}


.global MailboxRead				@ This function takes one input: r0 = mailbox to read (0-7)
MailboxRead:

		@ Chech that the input (channel value) is ok

		cmp	r0, #15
		movhi	pc, lr

		channel	.req	r1
		mov 	channel, r0
		push	{lr}
		bl	GetMailboxBase
		mailbox	.req	r0

readloop$:

wait2$:
		status	.req	r2
		ldr	status, [mailbox,#0x18]
		tst	status, #0x40000000
		.unreq	status
		bne	wait2$

		mail	.req	r2
		ldr	mail, [mailbox, #0]

		inchan	.req	r3
		and	inchan,	mail, #0b1111	@ pick the four lowest bits to check the channel id
		teq	inchan,	channel
		.unreq	inchan
		bne	readloop$

		.unreq	mailbox
		.unreq	channel

		and 	r0, mail,#0xfffffff0	@ pick the highest 28 bits from the mail and insert the result into r0
		.unreq	mail

		pop	{pc}
