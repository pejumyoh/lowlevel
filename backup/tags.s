.section .data
tag_core:	.int 0
tag_mem:	.int 0
tag_videotext:	.int 0
tag_ramdisk:	.int 0
tag_initrd2:	.int 0
tag_serial:	.int 0
tag_revision:	.int 0
tag_videolfb:	.int 0
tag_cmdline:	.int 0

.section .text
.global Inittags
Inittags:
	push	{r4,r5}

	tagAddr		.req	r0			@ address of the tag
	tagSizeW	.req	r1			@ size of the tag in words
	tagBase 	.req	r2			@ Index of the tag
	tagNumber 	.req	r3			@ number of the tag
	xcoord		.req	r4
	offset		.req	r5

	ldr	tagBase, =tag_core			@ Start initialization from the tag_core. TagIndex contains now the address of the tag_core variable
	mov	tagAddr, #0x100				@ start from beginning address 0x100 where atags are present
	mov	xcoord, #40
	mov	tagNumber, #0
TagLoop:

	ldr	tagSizeW, [tagAddr]			@ read the size of the tag in words
	ldrh	tagNumber, [tagAddr,#4]			@ read the tag index (half word)
	sub	tagNumber, #1				@ Subtract 1 from the read tag number
	mov	offset, tagNumber, lsl #2		@ offset is tagNumber x 4 bytes
	str	tagAddr, [tagBase, offset]		@ Store the address given by tagAddr into memory offset given by tagBase + tagNumber (first = tag_core)
	lsl	tagSizeW, #2				@ convert content of tagsizeW from words into bytes by multiplying by 4
	add	tagAddr, tagSizeW			@ add tag address the tagSize (in bytes!) to get the starting address of the next tag
	cmp	tagNumber, #8				@ Compare the read tag number to 9 (maximum)
	blt	TagLoop					@ If we are not yet at maximum tagNumber, continue the loop

	pop	{r4,r5}
	mov	pc, lr					@ otherwise we are done

.global	FindTag
FindTag:

	@ r0 contains the number of tag to find [1...9]
	@ when returned, r0 contains the start address of the desired tag

	@ TODO: check if the input is acceptable

	ldr	r1, =tag_core		@ read the tag core address
	sub	r0, #1			@ subtract 1 from the given input index value
	add	r2, r1, r0, lsl #2	@ calculate the correct tag position in the table tag_core... tag_cmdline
	ldr	r0, [r2]		@ load the address contained in r2 to r2 register
	mov	pc, lr

