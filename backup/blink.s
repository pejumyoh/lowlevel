.global	blink5times
blink5times:

	push	{r5,lr}
	mov	r0,#47
	mov	r1,#1
	bl	SetGpioFunction

	mov	r5, #0  @ counter

blinkloop:

	mov	r0,#47
	mov	r1,#1
	bl	SetGpio

	ldr	r0, =100000
	bl	wait

	mov	r0,#47
	mov	r1,#0
	bl	SetGpio

	ldr	r0, =100000
	bl	wait

	cmp	r5,#5
	add	r5,#1
	ble	blinkloop

	pop	{r5,pc}
