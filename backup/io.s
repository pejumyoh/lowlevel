.section .text

.global read32
read32:
	ldr	r0,[r0]
	mov	pc, lr

.global write32
write32:
	str	r1,[r0]
	mov	pc, lr

.global read16
read16:
	ldrh	r0,[r0]
	mov	pc, lr

.global write16
write16:
	strh	r1,[r0]
	mov	pc, lr

