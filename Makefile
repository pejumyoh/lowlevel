ARCH		= arm
PLATFORM	= bcm2708
#PLATFORM	= versatilepb

ifeq ($(PLATFORM),versatilepb)
MCPU		= arm926ej-s
else
MCPU    	= arm1176jzf-s
endif

ARMGNU		= arm-linux
BUILD		= build/
KSOURCE		= kernel/
DRIVERS 	= drivers/
INCLUDE 	= include/
TARGET 		= kernel.img
CFLAGS 		= -ffreestanding -std=gnu99 -Wall -O2 -fstack-protector-all -nostdlib -I./$(INCLUDE) -mcpu=$(MCPU)
LDFLAGS 	= -static -L/home/pejumyoh/BUILDROOT/buildroot/output/host/usr/lib/gcc/arm-buildroot-linux-gnueabihf/4.9.2 -lgcc
LIST 		= kernel.list
MAP		= kernel.map
LINKER		= kernel.ld 
OBJECTS 	:= $(patsubst 	$(KSOURCE)%.s,$(BUILD)%.o,$(wildcard $(KSOURCE)*.s))\
			$(patsubst $(KSOURCE)%.c,$(BUILD)%.o,$(wildcard $(KSOURCE)*.c))\
			$(patsubst arch/$(ARCH)/%.s,$(BUILD)%.o,$(wildcard arch/$(ARCH)/*.s))\
			$(patsubst arch/$(ARCH)/%.c,$(BUILD)%.o,$(wildcard arch/$(ARCH)/*.c))\
			$(patsubst arch/$(ARCH)/$(PLATFORM)/%.s,$(BUILD)%.o,$(wildcard arch/$(ARCH)/$(PLATFORM)/*.s))\
			$(patsubst arch/$(ARCH)/$(PLATFORM)/%.c,$(BUILD)%.o,$(wildcard arch/$(ARCH)/$(PLATFORM)/*.c))\
			$(patsubst $(DRIVERS)/%.s,$(BUILD)%.o,$(wildcard $(DRIVERS)/*.s))\
			$(patsubst $(DRIVERS)/%.c,$(BUILD)%.o,$(wildcard $(DRIVERS)/*.c))
	
all: $(TARGET) $(LIST)

rebuild: all

$(LIST) : $(BUILD)output.elf
	$(ARMGNU)-objdump -d $(BUILD)output.elf > $(LIST)

$(TARGET) : $(BUILD)output.elf
	$(ARMGNU)-objcopy $(BUILD)output.elf -O binary $(TARGET) 

$(BUILD)output.elf : $(OBJECTS) $(LINKER)
	$(ARMGNU)-ld -T$(LINKER) --no-undefined $(OBJECTS) $(LDFLAGS) -Map $(MAP) -o $(BUILD)output.elf


# KERNEL SOURCES
$(BUILD)%.o: $(KSOURCE)%.s $(BUILD)
	$(ARMGNU)-as -I $(KSOURCE) $< -o $@

$(BUILD)%.o: $(KSOURCE)%.c $(BUILD)
	$(ARMGNU)-gcc -I $(KSOURCE) $(CFLAGS) -c $< -o $@


# ARCH-SPECIFIC SOURCES
$(BUILD)%.o: arch/$(ARCH)/%.s $(BUILD)
	$(ARMGNU)-as -I arch/$(ARCH)/ $< -o $@

$(BUILD)%.o: arch/$(ARCH)/%.c $(BUILD)
	$(ARMGNU)-gcc -I arch/$(ARCH)/ $(CFLAGS) -c $< -o $@


# PLATFORM-SPECIFIC SOURCES
$(BUILD)%.o: arch/$(ARCH)/$(PLATFORM)/%.s $(BUILD)
	$(ARMGNU)-as -I arch/$(ARCH)/$(PLATFORM)/ $< -o $@

$(BUILD)%.o: arch/$(ARCH)/$(PLATFORM)/%.c $(BUILD)
	$(ARMGNU)-gcc -I arch/$(ARCH)/$(PLATFORM)/ $(CFLAGS) -c $< -o $@


# ARCH-INDEPENDENT KERNEL DRIVERS
$(BUILD)%.o: drivers/%.c $(BUILD)
	$(ARMGNU)-gcc -I drivers $(CFLAGS) -c $< -o $@

$(BUILD):
	mkdir $@

clean : 
	rm -rf $(BUILD)
	rm -f $(TARGET)
	rm -f $(LIST)
	rm -f $(MAP)
